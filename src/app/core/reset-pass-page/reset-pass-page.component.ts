import { Component, OnInit } from '@angular/core';
import { TopbarService } from "app/shared/topbar.service";

@Component({
  selector: 'app-reset-pass-page',
  templateUrl: './reset-pass-page.component.html',
  styleUrls: ['./reset-pass-page.component.scss']
})
export class ResetPassPageComponent implements OnInit {

        constructor(private topbarservice: TopbarService) { }

    ngOnInit() {
        this.topbarservice.setState({
            title: 'เปลี่ยนรหัสผ่าน',
            hasButton: false
        })
    }
}
