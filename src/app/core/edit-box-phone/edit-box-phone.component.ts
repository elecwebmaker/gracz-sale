import { Component, OnInit, forwardRef, OnChanges } from '@angular/core';
import { EditBoxComponent } from "app/core/edit-box/edit-box.component";
import { FormGroup, FormControl, Validators } from "@angular/forms/";

@Component({
  selector: 'app-edit-box-phone',
  templateUrl: './edit-box-phone.component.html',
  styleUrls: ['./edit-box-phone.component.scss'],
    providers:[
        {
            provide: EditBoxComponent,
            useExisting:forwardRef(() => EditBoxPhoneComponent)
        }
    ]
})
export class EditBoxPhoneComponent extends EditBoxComponent implements OnChanges {
    public formg: FormGroup = new FormGroup({
        code: new FormControl('', Validators.required),
        number: new FormControl('', Validators.required)
    });

    ngOnChanges() {
        if (this.value) {
            this.formg.get('code').setValue(this.value.code);
            this.formg.get('number').setValue(this.value.number);
        }
    }

    onSave() {
        this.save.emit({
            code: this.formg.get('code').value,
            number: this.formg.get('number').value
        });
    }
}
