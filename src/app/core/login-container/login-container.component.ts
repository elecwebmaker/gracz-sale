import { Component, OnInit, ViewChild } from "@angular/core";
import { LoginUser, LoginComponent } from "app/core/login/login.component";
import { UserAuthenService } from "app/core/user-authen.service";
import { Router } from "@angular/router";
import { CfUserHandlerJWTService } from "app/cfusersecure/cf-user-handler/cf-user-handler-jwt.service";

@Component({
    selector: 'app-login-container',
    templateUrl: './login-container.component.html',
    styleUrls: ['./login-container.component.scss']
})
export class LoginContainerComponent implements OnInit {
    loading: boolean;
    @ViewChild(LoginComponent) logincomp: LoginComponent;
    constructor(
        private cfuserAuthenJWT: CfUserHandlerJWTService,
        private userAuthenService: UserAuthenService,
        private router: Router
    ) {}

    ngOnInit() {
        if (this.cfuserAuthenJWT.isAuthen()) {
        this.router.navigate(['/profile']);
        }
    }
    login(user: LoginUser) {
        console.log('login!', user);
        this.loading = true;
        this.userAuthenService.login(user).subscribe(() => {
            this.loading = false;
            this.router.navigate(['/customer/list']);
            this.logincomp.setPassWrong(false);
        }, () => {
            this.loading = false;
            this.logincomp.setPassWrong(true);
        });
    }
}
