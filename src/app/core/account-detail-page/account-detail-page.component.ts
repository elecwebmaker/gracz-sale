import { Component, OnInit } from '@angular/core';
import { TopbarService } from "app/shared/topbar.service";

@Component({
  selector: 'app-account-detail-page',
  templateUrl: './account-detail-page.component.html',
  styleUrls: ['./account-detail-page.component.scss']
})
export class AccountDetailPageComponent implements OnInit {

    constructor(private topbarservice: TopbarService) { }

    ngOnInit() {
        this.topbarservice.setState({
            title: 'บัญชีของฉัน',
            hasButton: false
        })
    }
}
