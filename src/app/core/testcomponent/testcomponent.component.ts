import { Component, OnInit } from '@angular/core';
import { UserService } from "app/core/user.service";

@Component({
  selector: 'app-testcomponent',
  templateUrl: './testcomponent.component.html',
  styleUrls: ['./testcomponent.component.scss']
})
export class TestcomponentComponent implements OnInit {
  data: any;
  datajwt: any;
  isAuthen : boolean;
  isAuthenJWT : boolean;
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.isAuthen = this.userService.isAuthen();
    // this.isAuthenJWT = this.userService.isAuthenJWT();
    this.data = this.userService.getData();
    this.datajwt = this.userService.getDataJWT();
  }

}
