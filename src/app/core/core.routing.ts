import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginContainerComponent } from 'app/core/login-container/login-container.component';
import { TestcomponentComponent } from "app/core/testcomponent/testcomponent.component";
import { TestloginComponent } from "app/core/testlogin/testlogin.component";
import { PageNotFoundComponent } from "app/shared/page-not-found/page-not-found.component";
import { AccountDetailPageComponent } from "app/core/account-detail-page/account-detail-page.component";
import { ResetPassPageComponent } from "app/core/reset-pass-page/reset-pass-page.component";
import { CfAuthenJWTGuard } from "app/cfusersecure/cf-authen-guard/cf-authen-jwt-guard";
import { OutletComponent } from "app/shared/outlet/outlet.component";

export const coreRoutes:Routes = [
    {
        path: 'login',
        component: LoginContainerComponent
    },
    {
        path: 'test',
        component: TestcomponentComponent
    },
    {
        path: 'testlogin',
        component: TestloginComponent
    }, {
        path: 'profile',
        component: OutletComponent,
        canActivate:[CfAuthenJWTGuard],
        children: [{
            path: '',
            component: AccountDetailPageComponent
        }, {
            path: '**',
            redirectTo: '/404'
        }]
    }, {
        path: 'resetpass',
        component: OutletComponent,
        canActivate:[CfAuthenJWTGuard],
        children: [
            {
                path: '',
                component: ResetPassPageComponent,
            },
            {
                path: '**',
                redirectTo: '/404'
            }
        ]
    }, {
        path: '404',
        component: PageNotFoundComponent
    },
    {
        path: '',
        component: LoginContainerComponent
    },
    
];

@NgModule({
  imports: [
    RouterModule.forChild(coreRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers:[]
})


export class CoreRouting { }