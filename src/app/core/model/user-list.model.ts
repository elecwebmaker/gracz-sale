

export interface IUserListModel {
    id: number;
    username: string;
    job_type: number;
    name: string;
    phone: {code: string, number: string};
    email: string;
    line_id: string;
    pic: {
        url: string,
        tmp: string
    };
    isArchived: boolean;
    lastname: string;
    password: string;
}

export class UserListModel {
    id: number;
    username: string;
    job_type: number;
    name: string;
    lastname: string;
    phone: {code: string, number: string};
    email: string;
    line_id: string;
    pic: {
        url: string,
        tmp: string
    };
    isArchived: boolean;
    password: string;
    constructor(data: IUserListModel) {
        this.id = data.id;
        this.username = data.username;
        this.job_type = data.job_type;
        this.name = data.name;
        if(data.phone){
            this.phone = {
                code:data.phone.code,
                number:data.phone.number
            }
        }else{
            this.phone = <{code: string, number: string}>{};
        }
        this.email = data.email;
        this.line_id = data.line_id;
        if(data.pic){
            this.pic = {
                tmp:data.pic.tmp,
                url:data.pic.url,
            }
        }else{
            this.pic = <{url: string,tmp: string}> {};
        }
        this.isArchived = data.isArchived;
        this.lastname = data.lastname;
        this.password = data.password;
    }

    gen(): IUserListModel {
        return {
            id: this.id,
            username: this.username,
            job_type: this.job_type,
            name: this.name,
            phone: {
                code: this.phone.code,
                number: this.phone.number
            },
            email: this.email,
            line_id: this.line_id,
            pic: this.pic,
            isArchived: this.isArchived,
            lastname: this.lastname,
            password: this.password
        };
    }
}