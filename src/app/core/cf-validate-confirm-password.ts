import { FormControl } from "@angular/forms";

export function validateConfirmPassword(oldC: FormControl, reverse: boolean = false) {
    return  (c: FormControl) =>{
        if (c.value == oldC.value && reverse) {
            if(oldC.errors){
                console.log('del',oldC);
                delete oldC.errors['validateEqual'];
            }
            
            if (oldC.errors && !Object.keys(oldC.errors).length){
                oldC.setErrors(null);
            }
            return null;
        }

     
        if (c.value !== oldC.value && reverse) {
            oldC.setErrors({ validateEqual: false });
            return null;
        }
        

        if(!reverse){
            return (c.value == oldC.value)? null : {
                validateEqual: {
                    valid: false
                }
            }
        };
    }
}