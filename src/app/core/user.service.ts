import { Injectable } from '@angular/core';

import { Http } from "@angular/http";
import { CfUserHandlerNormalService } from "app/cfusersecure/cf-user-handler/cf-user-handler-normal.service";
import { CfUserHandlerJWTService } from "app/cfusersecure/cf-user-handler/cf-user-handler-jwt.service";

@Injectable()
export class UserService {

  constructor(private cfuserhandlernormalservice:CfUserHandlerNormalService,private cfuserjwtservice: CfUserHandlerJWTService,private http: Http) { 
    console.log('new userservice');
  }

  login(isSave:boolean){
    //username:string, password: string
    const url = 'http://demo0905840.mockable.io/user/login';
    this.http.post(url,{username:'admin1',password:'admi'}).subscribe((res:any)=>{
      this.cfuserhandlernormalservice.login(res.access_token,{
        user_id:res.user_id,
        nameer:"Napat"
      });
      if(isSave){
        this.cfuserhandlernormalservice.saveToLocalStorage();
      }
    });
 
  }

  loginJwt(isSave){
    const testacc = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiVCBQIiwiZW1haWwiOiJ0b25nQGcuY29tIiwiZ3JvdXAiOiJ4NkE5eFZHaHJcLzRBSE1EVnJxTXpKQT09IiwibG9naW4iOjE0OTgxODAwNjI1NjgsImV4cGlyZSI6MTQ5ODE4MTg2MjU2OH0.1cag0FZBCbFcWdhYvVDNgcX0tFJpRax5VkwxoJ6ukK8';
    this.cfuserjwtservice.login(testacc);
    if(isSave){
      this.cfuserjwtservice.saveToLocalStorage();
    }
  }

  getData(){
    return {
      user_id:this.cfuserhandlernormalservice.getUserData('user_id'),
      name: this.cfuserhandlernormalservice.getUserData('nameer')
    }
  }

  getDataJWT(){
    return {
      name: this.cfuserjwtservice.getUserData('name'),
      email: this.cfuserjwtservice.getUserData('email')
    }
  }

  isAuthen(){
    return this.cfuserhandlernormalservice.isAuthen();
  }
  isAuthenJWT(){
    return this.cfuserjwtservice.isAuthen();
  }
  logout(){
    this.cfuserhandlernormalservice.logout();
    this.cfuserjwtservice.logout();
  }
}
