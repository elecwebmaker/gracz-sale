import { Injectable } from '@angular/core';
import { TokenProvider } from 'craftutility';
import { UserAuthenService } from "app/core/user-authen.service";
import { CfUserHandlerJWTService } from "app/cfusersecure/cf-user-handler/cf-user-handler-jwt.service";
@Injectable()
export class AccestokenProviderService implements TokenProvider{
  getToken(): string {
      // console.log(this.cfuserjwtservice.getAccesstoken(),'token from service')
   return this.cfuserjwtservice.getAccesstoken();
  }

  constructor(private cfuserjwtservice: CfUserHandlerJWTService) { }

}
