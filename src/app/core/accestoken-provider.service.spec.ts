import { TestBed, inject } from '@angular/core/testing';

import { AccestokenProviderService } from './accestoken-provider.service';

describe('AccestokenProviderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccestokenProviderService]
    });
  });

  it('should ...', inject([AccestokenProviderService], (service: AccestokenProviderService) => {
    expect(service).toBeTruthy();
  }));
});
