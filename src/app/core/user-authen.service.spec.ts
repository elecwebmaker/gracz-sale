import { TestBed, inject } from '@angular/core/testing';

import { UserAuthenService } from './user-authen.service';

describe('UserAuthenService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserAuthenService]
    });
  });

  it('should ...', inject([UserAuthenService], (service: UserAuthenService) => {
    expect(service).toBeTruthy();
  }));
});
