import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { FormControl } from "@angular/forms/";

@Component({
    selector: 'app-edit-box',
    templateUrl: './edit-box.component.html',
    styleUrls: ['./edit-box.component.scss']
})
export class EditBoxComponent implements OnChanges {
    public input: FormControl = new FormControl();
    @Input() value;
    @Input() editable: boolean = false;
    @Output() save: EventEmitter<any> = new EventEmitter();
    @Output() edit: EventEmitter<any> = new EventEmitter();
    activated: boolean = false;
    constructor() { }

    ngOnChanges() {
        if (this.value) {
            this.input.setValue(this.value);
        }
    }

    onSave() {
        this.save.emit(this.input.value);
    }

    onClick() {
        if (this.editable) {
            this.edit.emit();
        }
    }

    active() {
        this.activated = true;
    }

    deactive() {
        this.activated = false;
    }

}
