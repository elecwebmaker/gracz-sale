import { TestBed, async, inject } from '@angular/core/testing';

import { AuthorizeInventoryGuard } from './authorize-inventory.guard';

describe('AuthorizeInventoryGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthorizeInventoryGuard]
    });
  });

  it('should ...', inject([AuthorizeInventoryGuard], (guard: AuthorizeInventoryGuard) => {
    expect(guard).toBeTruthy();
  }));
});
