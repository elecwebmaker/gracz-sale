import { TestBed, async, inject } from '@angular/core/testing';

import { AuthorizeOrderGuard } from './authorize-order.guard';

describe('AuthorizeOrderGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthorizeOrderGuard]
    });
  });

  it('should ...', inject([AuthorizeOrderGuard], (guard: AuthorizeOrderGuard) => {
    expect(guard).toBeTruthy();
  }));
});
