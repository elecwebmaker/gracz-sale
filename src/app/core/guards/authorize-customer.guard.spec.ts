import { TestBed, async, inject } from '@angular/core/testing';

import { AuthorizeCustomerGuard } from './authorize-customer.guard';

describe('AuthorizeCustomerGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthorizeCustomerGuard]
    });
  });

  it('should ...', inject([AuthorizeCustomerGuard], (guard: AuthorizeCustomerGuard) => {
    expect(guard).toBeTruthy();
  }));
});
