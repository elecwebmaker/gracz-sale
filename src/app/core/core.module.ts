import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { CustomerModule } from 'app/customer/customer.module';
import { LoginComponent } from './login/login.component';
import { LoginContainerComponent } from './login-container/login-container.component';
import { CoreRouting } from './core.routing';
import { OrderModule } from 'app/order/order.module';
import { InventoryModule } from 'app/inventory/inventory.module';

import { UserService } from './user.service';
import { TestcomponentComponent } from './testcomponent/testcomponent.component';
import { TestloginComponent } from './testlogin/testlogin.component';

import { UserAuthenService } from './user-authen.service';
import { AccestokenProviderService } from './accestoken-provider.service';
import { AccountDetailComponent } from 'app/core/account-detail/account-detail.component';
import { AccountDetailContainerComponent } from 'app/core/account-detail-container/account-detail-container.component';
import { AccountDetailPageComponent } from 'app/core/account-detail-page/account-detail-page.component';
import { EditBoxComponent } from 'app/core/edit-box/edit-box.component';
import { EditBoxPhoneComponent } from 'app/core/edit-box-phone/edit-box-phone.component';
import { ResetPassComponent } from 'app/core/reset-pass/reset-pass.component';
import { ResetPassContainerComponent } from 'app/core/reset-pass-container/reset-pass-container.component';
import { ResetPassPageComponent } from 'app/core/reset-pass-page/reset-pass-page.component';
import { AccountGetGetterService } from 'app/core/account-get-getter.service';
import { AccountSetterService } from 'app/core/account-setter.service';
import { AuthorizeCustomerGuard } from './guards/authorize-customer.guard';
import { AuthorizeOrderGuard } from './guards/authorize-order.guard';
import { AuthorizeInventoryGuard } from './guards/authorize-inventory.guard';
import { CfAuthorizeJWTGuard } from "app/cfusersecure/cf-authorize-guard/cf-authorize-jwt-guard";



@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    CustomerModule,
    CoreRouting,
    InventoryModule,
  ],
  providers:[UserService, UserAuthenService, AccountGetGetterService, AccountSetterService, AuthorizeCustomerGuard, AuthorizeOrderGuard, AuthorizeInventoryGuard, CfAuthorizeJWTGuard],
  declarations: [LoginComponent, LoginContainerComponent, TestcomponentComponent, TestloginComponent, AccountDetailComponent, AccountDetailContainerComponent, AccountDetailPageComponent, EditBoxComponent, EditBoxPhoneComponent, ResetPassComponent, ResetPassContainerComponent, ResetPassPageComponent],
  exports: [
    CustomerModule, 
    LoginComponent,
    OrderModule,
  ]
})
export class CoreModule { }
