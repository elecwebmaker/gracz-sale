import { Injectable } from '@angular/core';
import { LoginUser } from "app/core/login/login.component";
import { CfUserHandlerJWTService } from "app/cfusersecure/cf-user-handler/cf-user-handler-jwt.service";
import { UrlproviderService, LOGIN, LOGOUT } from "app/shared/urlprovider.service";
import { Http } from "@angular/http";
import { Router } from "@angular/router";

export interface EmployeeLogin{
  name: string;
  picture_src: string;
  position: string;
}

@Injectable()
export class UserAuthenService {

  constructor(private cfuserjwtservice: CfUserHandlerJWTService,private urlprovider: UrlproviderService, private http: Http, private router: Router) {

  }

  login(user:LoginUser){
    const url = this.urlprovider.getUrl(LOGIN);
    return this.http.post(url,{
      username: user.username,
      password: user.password
    }).map((res:any)=>{
      this.cfuserjwtservice.login(res.token, res.permission);
      if(user.savePass){
        this.cfuserjwtservice.saveToLocalStorage();
      }
    })
  }


  getUserData(): EmployeeLogin{
    if(this.cfuserjwtservice.isAuthen()){
      return {
        name: this.cfuserjwtservice.getUserData('name'),
        picture_src: this.cfuserjwtservice.getUserData('picture_src'),
        position: this.cfuserjwtservice.getUserData('position'),
      }
    }else{
      console.error('Please log in');
      return {
        name:undefined,
        picture_src:undefined,
        position:undefined
      };
    }
  }

  logout(){
    const url = this.urlprovider.getUrl(LOGOUT);
   
    return this.http.post(url,{}).map(()=>{
       this.cfuserjwtservice.logout();
    });
  }

  getToken(){
    return this.cfuserjwtservice.getAccesstoken();
  }
}
