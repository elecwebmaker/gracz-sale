import { Injectable } from '@angular/core';
import { Http } from "@angular/http/";
import { Observable } from "rxjs/Rx";
import { UrlproviderService, ACCOUNT_EDIT, PASSWORD_RESET } from "app/shared/urlprovider.service";

@Injectable()
export class AccountSetterService {

    constructor(
        private http: Http,
        private urlProvider: UrlproviderService
    ) { }

    edit(body): Observable<any> {
        const url = this.urlProvider.getUrl(ACCOUNT_EDIT);
        return this.http.post(url, body);
    }

    resetPassword(body): Observable<any> {
        const url = this.urlProvider.getUrl(PASSWORD_RESET);
        return this.http.post(url, body);
    }
}
