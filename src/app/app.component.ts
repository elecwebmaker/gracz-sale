import { Component } from '@angular/core';
import { CFAuthenJWTManageService } from "app/cfusersecure/cf-authen-manage/cf-authen-jwt-manage.service";
import { AccestokenProviderService } from "app/core/accestoken-provider.service";
import { CraftHttpNotifyService } from 'craftutility';
import { Router } from '@angular/router';
import { UserAuthenService } from 'app/core/user-authen.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private userAuthenservice: UserAuthenService,private router: Router, private crafthttpnotify: CraftHttpNotifyService,private cfauthenmanage: CFAuthenJWTManageService, private test:AccestokenProviderService){
    this.cfauthenmanage.setRouteWhenExpired('/login');
    this.cfauthenmanage.setRouteWhenInvalid('/login');
    this.cfauthenmanage.setRouteWhenUnAuthorize('/profile');
    this.cfauthenmanage.setRouteWhenUnAuthen('/login');
    this.crafthttpnotify.getError().subscribe((error:any)=>{
      if(error){
        if(error.status == "401" || error.status == "403"){
          this.userAuthenservice.logout().subscribe(()=>{
            this.router.navigate(['/login']);
          })
        }
      }
    });
  }

    ngOnInit() {
        console.log(this.test);
    }
}
