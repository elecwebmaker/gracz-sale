import { Component, OnInit } from '@angular/core';
import { BaseItemComponent } from 'app/shared/base-item/base-item.component';

@Component({
    selector: 'app-order-item',
    templateUrl: './order-item.component.html',
    styleUrls: ['./order-item.component.scss']
})
export class OrderItemComponent  extends BaseItemComponent {
    ngClass;
    ngOnInit() {
        if (this.model) {
            this.ngClass = {
                approve: (this.model.status == 1 || this.model.status == 2 || this.model.status == 4),
                reject: (this.model.status == 5 || this.model.status == 7),
                pending: (this.model.status == 3),
                edit: (this.model.status == 6)
            };
        }
    }
}
