import { TestBed, inject } from '@angular/core/testing';

import { OrderGetterService } from './order-getter.service';

describe('OrderGetterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrderGetterService]
    });
  });

  it('should ...', inject([OrderGetterService], (service: OrderGetterService) => {
    expect(service).toBeTruthy();
  }));
});
