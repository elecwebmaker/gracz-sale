import { Component, OnInit } from '@angular/core';
import { TopbarService } from "app/shared/topbar.service";

@Component({
    selector: 'app-order-form-page',
    templateUrl: './order-form-page.component.html',
    styleUrls: ['./order-form-page.component.scss']
})
export class OrderFormPageComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }

}
