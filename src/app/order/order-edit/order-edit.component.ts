import { Component, OnInit } from '@angular/core';
import { OrderGetterService } from "app/order/order-getter.service";
import { OrderFormService } from "app/order/service/order-form.service";
import { Router,ActivatedRoute } from "@angular/router";
import { OrderPrefill } from "app/order/orderprefill";
import { TopbarService } from "app/shared/topbar.service";
import { OrderSenderService } from "app/order/service/order-sender.service";
import { NotifyService } from "app/shared/notify.service";

@Component({
    selector: 'app-order-edit',
    templateUrl: './order-edit.component.html',
    styleUrls: ['./order-edit.component.scss']
})
export class OrderEditComponent implements OnInit {
    loading = true;
    orderprefill:OrderPrefill;
    constructor(
        private ordergetterservice: OrderGetterService,
        private ordersetterservice: OrderSenderService,
        private orderformservice : OrderFormService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private topbarservice: TopbarService,
        private notify: NotifyService
    ) { 
        this.orderprefill = new OrderPrefill(this.orderformservice,this.ordergetterservice);
    }

    ngOnInit() {
        this.orderprefill.setOrderFormService(this.activatedRoute.snapshot.params['order_id']).subscribe(()=>{
            this.loading = false;
            this.topbarservice.setState({
                title: 'แก้ไขรายการสั่งซื้อ #' + this.orderprefill.getPrefillData().preoder_id,
                hasButton: false
            });
        });
    }

    add_product() {
        this.router.navigate(['/product/oid/' + this.orderprefill.getPrefillData().order_id + '/push']);
    }

    close_product() {
        this.ordersetterservice.closeProduct(this.orderprefill.getPrefillData().order_id).subscribe(() => {
            this.notify.showSuccess();
            this.router.navigate(['order/list']);
        });
    }

}
