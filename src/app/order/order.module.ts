import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderListComponent } from './order-list/order-list.component';
import { OrderItemComponent } from './order-item/order-item.component';
import { SharedModule } from 'app/shared/shared.module';
import { OrderFormComponent } from './form/order-form/order-form.component';
import { OrderListPageComponent } from './order-list-page/order-list-page.component';
import { OrderGetterService } from './order-getter.service';
import { OrderRouting } from "app/order/order.routing.module";
import { OrderProductFormComponent } from './form/order-product-form/order-product-form.component';
import { OrderFormAddContainerComponent } from './orderformflow/orderhead/order-form-add-container/order-form-add-container.component';
import { OrderFormService } from './service/order-form.service';
import { OrderAddComponent } from './order-add/order-add.component';
import { OrderProductListComponent } from './orderformflow/product-table/order-product-list/order-product-list.component';
import { OrderEditComponent } from './order-edit/order-edit.component';
import { OrderFormEditContainerComponent } from './orderformflow/orderhead/order-form-edit-container/order-form-edit-container.component';
import { OrderFormViewContainerComponent } from './orderformflow/orderhead/order-form-view-container/order-form-view-container.component';
import { OrderViewComponent } from './order-view/order-view.component';
import { OrderProductlistAddContainerComponent } from './orderformflow/product-table/order-productlist-add-container/order-productlist-add-container.component';
import { OrderProductlistEditContainerComponent } from './orderformflow/product-table/order-productlist-edit-container/order-productlist-edit-container.component';
import { OrderProductaddContainerComponent } from './orderformflow/product/order-productadd-container/order-productadd-container.component';
import { OrderProducteditContainerComponent } from './orderformflow/product/order-productedit-container/order-productedit-container.component';
import { OrderSenderService } from './service/order-sender.service';
import { OrderProductpushContainerComponent } from './orderformflow/product/order-productpush-container/order-productpush-container.component';
import { OrderProductupdateContainerComponent } from './orderformflow/product/order-productupdate-container/order-productupdate-container.component';
import { OrderFormPageComponent } from './order-form-page/order-form-page.component';
import { CanActivateOrderheaderService } from './can-activate-orderheader.service';
import { OrderProductlistViewContainerComponent } from './orderformflow/product-table/order-productlist-view-container/order-productlist-view-container.component';
import { OrderProductviewContainerComponent } from "app/order/orderformflow/product/order-productview-container/order-productview-container.component";
import { OrderActivateService } from './order-add-activate.service';
import { ProductActivateService } from './product-activate.service';
import { CustomerActivateService } from './customer-activate.service';
import { OrderSearchbarComponent } from './order-searchbar/order-searchbar.component';
import { ProductFormService } from './service/product-form.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    OrderRouting
  ],
  declarations: [
    OrderListComponent,
    OrderItemComponent,
    OrderFormComponent,
    OrderListPageComponent,
    OrderProductFormComponent,
    OrderFormAddContainerComponent,
    OrderAddComponent,
    OrderProductListComponent,
    OrderEditComponent,
    OrderFormEditContainerComponent,
    OrderFormViewContainerComponent,
    OrderViewComponent,
    OrderProductlistAddContainerComponent,
    OrderProductlistEditContainerComponent,
    OrderProductaddContainerComponent,
    OrderProducteditContainerComponent,
    OrderProductpushContainerComponent,
    OrderProductupdateContainerComponent,
    OrderFormPageComponent,
    OrderProductlistViewContainerComponent,
    OrderProductviewContainerComponent,
    OrderSearchbarComponent
  ],
  exports: [OrderListComponent, OrderItemComponent, OrderFormComponent,OrderProductFormComponent],
  providers: [OrderGetterService, OrderFormService, OrderSenderService, CanActivateOrderheaderService, OrderActivateService, ProductActivateService, CustomerActivateService, ProductFormService]

})
export class OrderModule { }
