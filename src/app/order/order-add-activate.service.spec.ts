import { TestBed, inject } from '@angular/core/testing';

import { OrderAddActivateService } from './order-add-activate.service';

describe('OrderAddActivateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrderAddActivateService]
    });
  });

  it('should ...', inject([OrderAddActivateService], (service: OrderAddActivateService) => {
    expect(service).toBeTruthy();
  }));
});
