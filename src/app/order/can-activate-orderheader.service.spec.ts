import { TestBed, inject } from '@angular/core/testing';

import { CanActivateOrderheaderService } from './can-activate-orderheader.service';

describe('CanActivateOrderheaderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanActivateOrderheaderService]
    });
  });

  it('should ...', inject([CanActivateOrderheaderService], (service: CanActivateOrderheaderService) => {
    expect(service).toBeTruthy();
  }));
});
