import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from "@angular/router";
import { NotifyService } from "app/shared/notify.service";
import { OrderFormAddContainerComponent } from "app/order/orderformflow/orderhead/order-form-add-container/order-form-add-container.component";
import { OrderFormService } from "app/order/service/order-form.service";
import { TopbarService } from "app/shared/topbar.service";


@Component({
    selector: 'app-order-add',
    templateUrl: './order-add.component.html',
    styleUrls: ['./order-add.component.scss']
})
export class OrderAddComponent implements OnInit {
    isLoading = false;
    @ViewChild(OrderFormAddContainerComponent) orderformcomp: OrderFormAddContainerComponent;
    constructor(
        private router: Router,
        private notifyservice: NotifyService,
        private topbarservice: TopbarService,
        private orderformservice: OrderFormService
    ) { }

    ngOnInit() {
        this.topbarservice.setState({
            title: 'เพิ่มรายการสินค้า',
            hasButton: false
        });
        
        console.log('clear');
    }

    add_product() {
        this.router.navigate(['/product/add']);
    }

    loading(load: boolean) {
        if (load) {
            this.isLoading = true;
        } else {
            this.isLoading = false;
            this.notifyservice.showSuccess();
        }
    }
}
