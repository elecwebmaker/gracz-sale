import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderSearchbarComponent } from './order-searchbar.component';

describe('OrderSearchbarComponent', () => {
  let component: OrderSearchbarComponent;
  let fixture: ComponentFixture<OrderSearchbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderSearchbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderSearchbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
