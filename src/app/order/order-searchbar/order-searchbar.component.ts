import { Component, OnInit } from '@angular/core';
import { OrderGetterService } from 'app/order/order-getter.service';
import { ListitemdataService } from 'app/shared/listitemdata.service';
import { FilterBar,IFilterKey } from 'app/shared/filterbar/filterbar';

@Component({
    selector: 'app-order-searchbar',
    templateUrl: './order-searchbar.component.html',
    styleUrls: ['./order-searchbar.component.scss']
})
export class OrderSearchbarComponent implements OnInit {
    private filter = new FilterBar<IFilterKey>(['status', 'key']);
    constructor(private ordergetter: OrderGetterService, private listdataservice: ListitemdataService) { }

    ngOnInit() {
        this.filter.setStatusList(this.listdataservice.getOrderStatus());
        this.filter.filterChanges().subscribe((res) => {
            this.ordergetter.setFilter(res);
        });
    }

    searchChange(val) {
        this.filter.setFilter('key', val);
    }

    statusChange(val) {
        this.filter.setFilter('status', val);
    }
}
