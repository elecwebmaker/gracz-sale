import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { OrderAddComponent } from "app/order/order-add/order-add.component";

@Injectable()
export class CanActivateOrderheaderService implements CanDeactivate<OrderAddComponent>{

  constructor() {
    
  }

  canDeactivate(comp: OrderAddComponent){
    if(comp.orderformcomp){
       comp.orderformcomp.saveFormtoStorage();
    }
   
    return true;
  }
}
