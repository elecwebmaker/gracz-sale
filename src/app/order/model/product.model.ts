export interface IProduct{
    id?: string;
    gracz_type: string;
    type: string;
    model: string;
    unit: string;
    unit_text: string;
    price: number;
    discount_percent: number;
    discount: string;
    total_price: number;
    quantity: number;
    isScrean: boolean;
    default_price: number;
    fgcode:string;
    product_name:string;
    quantity_text?: string;
    status_text: string;
}

export class ProductModel{
    id: string;
    gracz_type: string;
    type: string;
    model: string;
    unit: string;
    unit_text: string;
    price: number;
    discount_percent: number;
    discount: string;
    total_price: number;
    quantity: number;
    isScrean: boolean;
    fgcode:string;
    product_name:string;
    status_text: string;
    default_price: number;
    quantity_text: string;
    constructor(data:IProduct){
        this.id = data.id;
        this.gracz_type = data.gracz_type;
        this.type = data.type;
        this.model = data.model;
        this.unit = data.unit;
        this.unit_text = data.unit_text;
        this.price = data.price;
        this.discount_percent = data.discount_percent;
        this.discount = data.discount;
        this.total_price = data.total_price;
        this.quantity = data.quantity;
        this.quantity_text = data.quantity + " รายการ";
        this.isScrean = data.isScrean;
        this.product_name = data.product_name;
        this.fgcode = data.fgcode;
        this.default_price = data.default_price;
        this.status_text = data.status_text? data.status_text : 'รอดำเนินการ';
    }

    gen(): IProduct{
        return {
            id: this.id,
            gracz_type: this.gracz_type,
            type: this.type,
            model:this.model,
            unit: this.unit,
            unit_text: this.unit_text,
            price: this.price,
            discount_percent: this.discount_percent,
            discount: this.discount,
            total_price: this.total_price,
            quantity: this.quantity,
            isScrean: this.isScrean,
            fgcode:this.fgcode,
            product_name:this.product_name,
            default_price:this.default_price,
            status_text: this.status_text,
            quantity_text: this.quantity_text
        }
    }
}