export interface IOrderItemModel {
    id: number;
    preorder_id: string;
    list_quantity: string;
    total_price: string;
    status: number;
    status_text: string;
    is_edit: boolean;
    customer_name: string;
}

export class OrderItemModel {
    id: number;
    preorder_id: string;
    list_quantity: string;
    total_price: string;
    status: number;
    status_text: string;
    is_edit: boolean;
    customer_name: string;

    constructor(data: IOrderItemModel) {
        this.id = data.id;
        this.preorder_id = data.preorder_id;
        this.list_quantity = data.list_quantity;
        this.total_price = data.total_price;
        this.status = data.status;
        this.status_text = data.status_text;
        this.is_edit = data.is_edit;
        this.customer_name = data.customer_name;
    }

    gen(): IOrderItemModel {
        return {
            id: this.id,
            preorder_id: this.preorder_id,
            list_quantity: this.list_quantity,
            total_price: this.total_price,
            status: this.status,
            status_text: this.status_text,
            is_edit: this.is_edit,
            customer_name: this.customer_name
        }
    }
}