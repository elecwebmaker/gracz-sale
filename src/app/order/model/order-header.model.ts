
export interface IOrderHeader{
    oid?: string;
    is_draft?: number;
    shipment_id: string;
    shipment_date: Date;
    remark: string;
}

export class OrderHeaderModel{
    oid?: string;
    is_draft: number;
    shipment_id: string;
    shipment_date: Date;
    remark: string;
    constructor(data: IOrderHeader){
        this.oid = data.oid;
        this.is_draft = data.is_draft;
        this.shipment_id = data.shipment_id;
        this.shipment_date = data.shipment_date;
        this.remark = data.remark;
    }

    gen():IOrderHeader{
        return {
            oid:this.oid,
            is_draft: this.is_draft,
            shipment_id: this.shipment_id,
            remark: this.remark,
            shipment_date: this.shipment_date
        }
    }
}