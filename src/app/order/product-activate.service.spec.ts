import { TestBed, inject } from '@angular/core/testing';

import { ProductActivateService } from './product-activate.service';

describe('ProductActivateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProductActivateService]
    });
  });

  it('should ...', inject([ProductActivateService], (service: ProductActivateService) => {
    expect(service).toBeTruthy();
  }));
});
