import { TestBed, inject } from '@angular/core/testing';

import { OrderFormService } from './order-form.service';

describe('OrderFormService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrderFormService]
    });
  });

  it('should ...', inject([OrderFormService], (service: OrderFormService) => {
    expect(service).toBeTruthy();
  }));
});
