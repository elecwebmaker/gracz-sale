import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ProductModel,IProduct } from 'app/order/model/product.model';
import { UrlproviderService, ADD_ORDER, PUSH_PRODUCT, UPDATE_PRODUCT, UPDATE_ORDER, DEL_PRODUCT, CLOSE_ORDER } from 'app/shared/urlprovider.service';
import { Observable } from 'rxjs/Rx';

interface IParseProduct {
    category: string;
    model: string;
    unit: string;
    quantity: number;
    default_price: number;
    price: number;
    discount_percent: number;
    discount_value: string;
    total_price: number;
    product_code: string;
    screen: number;
}

function parseProduct(product: IProduct): IParseProduct {
    return {
        category: product.gracz_type,
        model: product.model,
        unit: product.unit,
        quantity: product.quantity,
        default_price: product.default_price,
        price: product.price,
        discount_percent: product.discount_percent,
        discount_value: product.discount,
        total_price: product.total_price,
        product_code: product.fgcode,
        screen: product.isScrean ? 1 : 0
    }
}

function parseDate(date_data){
    return date_data.getFullYear() + '-' + (date_data.getMonth() + 1) + '-' + date_data.getDate();
}

@Injectable()
export class OrderSenderService {

    constructor(private http: Http,private urlprovider:UrlproviderService) { }

    editOrder(order_id: string, orderheader:{
        shipment_id: string,
        shipment_date: Date,
        remark: string,
        is_draft: number;
    }){
        const url = this.urlprovider.getUrl(UPDATE_ORDER) + `/${order_id}`;
        const body = {
            shipment_id: orderheader.shipment_id,
            shipment_date: parseDate(orderheader.shipment_date),
            remark: orderheader.remark,
            is_draft: orderheader.is_draft
        };
        return this.http.post(url,body).map((res: any) => {
            return res.model;
        });
    }

    pushProduct(order_id: string,product: IProduct): Observable<undefined> {
        const url = this.urlprovider.getUrl(PUSH_PRODUCT);
        const tmp = parseProduct(product);
        return this.http.post(url, {order_id: order_id, product_list: JSON.stringify(tmp)}).map((res: any) => {
            return res.model;
        });
    }

    deleteProduct(id: string){
        const url = this.urlprovider.getUrl(DEL_PRODUCT) + '/' + id;
        return this.http.post(url,{}).map((res)=>{
            return res;
        })
    }

    updateProduct(id: string, product: IProduct): Observable<undefined>{
        const url = this.urlprovider.getUrl(UPDATE_PRODUCT);
        const tmp = parseProduct(product);;
        return this.http.post(url + product.id, {product_list: JSON.stringify(tmp)}).map((res: any) => {
            return res.model;
        });
    }


    addProduct(header:{
        cus_id: string,
        ship_id: string,
        ship_date: Date,
        remark: string,
        is_draft: number
    },product_list: Array<IProduct>){
        const parse_shipdate  =  parseDate(header.ship_date);
        const body = {
        customer_id: header.cus_id,
        shipment_id: header.ship_id,
        shipment_date :parse_shipdate,
        remark: header.remark,
        is_draft: header.is_draft,
        product_list: JSON.stringify(product_list.map((product)=>{
            return parseProduct(product);
        }))
        };
        console.log(body);
        return this.http.post(this.urlprovider.getUrl(ADD_ORDER),body);
    }

    closeProduct(id: string): Observable<any> {
        const url = this.urlprovider.getUrl(CLOSE_ORDER);
        return this.http.post(url, {order_id: id}).map((res) => {
            return res;
        });
    }

}
