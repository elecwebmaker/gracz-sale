import { Injectable } from '@angular/core';
import { ProductModel, IProduct } from "app/order/model/product.model";
import { ProductData } from "app/order/order-view/productdata";
import { OrderHeaderModel } from "app/order/model/order-header.model";
import { Iorderform } from "app/order/form/order-form/order-form.component";

@Injectable()
export class OrderFormService {
  cus_id: string;
  order_id: string;
  cus_name: string;
  productdata: ProductData;
  preorder_id:string;
  remark: string;
  ship_date: Date;
  shipment_id: string;
  status_text: string;
  cus_code : string;
  so_left: number;
  invoice_left: number;
  constructor() {
    this.productdata = new ProductData();
  }
  setCustomer(cus_id: string, cus_name: string, cus_code: string){
    this.cus_id = cus_id;
    this.cus_name = cus_name;
    this.cus_code = cus_code;
    
  }


  setDetail(data:{
    order_id: string;
    remark: string;
    ship_date: Date;
    shipment_id: string;
    status_text: string;
    preorder_id: string;
    so_left: number;
    invoice_left: number;
  }){
    this.order_id = data.order_id;
    this.remark = data.remark;
    this.ship_date = data.ship_date;
    this.shipment_id = data.shipment_id;
    this.status_text = data.status_text;
    this.preorder_id = data.preorder_id;
    this.so_left = data.so_left;
    this.invoice_left = data.invoice_left;
  }

  clear(){
    this.cus_id = undefined;
    this.order_id = undefined;
    this.cus_name = undefined;
    this.remark = undefined;
    this.ship_date = undefined;
    this.shipment_id = undefined;
    this.status_text = undefined;
    this.productdata.clear();
    this.cus_code = undefined;
  }

  clearOnlyHeadForm(){
      this.ship_date = undefined;
      this.shipment_id = undefined;
      this.status_text = undefined;
      this.remark = undefined;
      this.productdata.clear();
  }

  getPrefill(): Iorderform{
    return {
      cus_name: this.cus_name,
      cus_id: this.cus_id,
      remark: this.remark,
      shipment_id: this.shipment_id,
      shipment_date: this.ship_date,
      total_price: this.productdata.total_price,
      preoder_id: this.preorder_id
    };
  }

  setFromHeaderModel(orderheader: OrderHeaderModel){
    this.remark = orderheader.remark;
    this.shipment_id = orderheader.shipment_id;
    this.ship_date = orderheader.shipment_date;
  }
}
