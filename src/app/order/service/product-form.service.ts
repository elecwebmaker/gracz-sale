import { Injectable } from '@angular/core';
import { Http } from '@angular/http/';
import {
  UrlproviderService,
  CHK_SCREEN,
  GET_PRICE
} from 'app/shared/urlprovider.service';
import { Observable } from 'rxjs/Rx';
import { OrderFormService } from 'app/order/service/order-form.service';

@Injectable()
export class ProductFormService {

  constructor(
    private http: Http,
    private urlProvider: UrlproviderService,
    private orderService: OrderFormService
  ) { }

  chkScreen(model: number): Observable<boolean> {
    const url = this.urlProvider.getUrl(CHK_SCREEN);
    const body = {
      customer_code: this.orderService.cus_code,
      model_id: model
    };
    return this.http.post(url, body).map((res: any) => {
      return res.is_screen;
    });
  }

  getPrice(model: number, unit: number, screen: boolean): Observable<any> {
    const url = this.urlProvider.getUrl(GET_PRICE);
    const body = {
      model_id: model,
      unit_id: unit,
      customer_code: this.orderService.cus_code,
      screen: (screen) ? 1 : 0
    }
    return this.http.post(url, body).map((res: any) => {
      return res.model;
    });
  }

}
