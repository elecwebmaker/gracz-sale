import { TestBed, inject } from '@angular/core/testing';

import { OrderSenderService } from './order-sender.service';

describe('OrderSenderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrderSenderService]
    });
  });

  it('should ...', inject([OrderSenderService], (service: OrderSenderService) => {
    expect(service).toBeTruthy();
  }));
});
