import { Component, OnInit, ViewChild, Input, EventEmitter, Output } from '@angular/core';
import { ListitemdataService } from "app/shared/listitemdata.service";
import { Observable } from "rxjs/Rx";
import { BaseformComponent } from "app/shared/baseform/baseform.component";
import { SemanticSelectComponent } from "ng-semantic/ng-semantic";
import { CanSubmit } from "app/shared/model/can-submit";
import { OrderHeaderModel } from "app/order/model/order-header.model";
import { Validators } from "@angular/forms";
import { IMyDpOptions, IMyDate } from "mydatepicker";
declare var $: any;

interface IAddress{
    id: string;
    address: string;
    province: string;
    district: string;
    sub_district: string;
    zipcode: string;
};

export interface Iorderform{
    order_id?: string;
    shipment_date?: Date;
    shipment_id?: string;
    cus_name: string;
    cus_id?: string;
    status_text?: string;
    remark?: string;
    total_price: number;
    preoder_id: string;
};

export const ORDERADD = 'ORDERADD';
export const ORDEREDIT = 'ORDEREDIT';
export const ORDERVIEW = 'ORDERVIEW';

@Component({
    selector: 'app-order-form',
    templateUrl: './order-form.component.html',
    styleUrls: ['./order-form.component.scss']
})
export class OrderFormComponent extends BaseformComponent implements CanSubmit<OrderHeaderModel>{
    public date: Date = new Date();
    public today = this.date.getDate() + '/' + (this.date.getMonth() + 1) + '/' + this.date.getFullYear();
    public dateSubmit: Date = new Date();
    private myDatePickerOptions: IMyDpOptions = {
        disableUntil: {
            year: this.date.getFullYear(),
            month: this.date.getMonth() + 1,
            day: this.date.getDate()
        },
        // componentDisabled: true,
        dateFormat: 'dd/mm/yyyy'
    }
    @Output() onSubmit: EventEmitter<OrderHeaderModel> = new EventEmitter<OrderHeaderModel>();
    @ViewChild('shipmentselect') shipmentselect: SemanticSelectComponent;
    @Input('type') type:string = ORDERADD;
    public shipmentlist:Array<{value:string,text:string}>;
    public shipment_full_list:Array<IAddress> = [];
    public currentShipment:IAddress;
    prefilldata:Iorderform;
    order_id: string;
    total_price: number;
    customer_name: string;
    status_text: string;
    date_disable = false;
    manageBaseOnType(){
        switch(this.type){
            case ORDERADD:

                break;
            
            case ORDEREDIT:
                
                break;

            case ORDERVIEW:
                this.formg.disable();
                this.date_disable = true;
                this.myDatePickerOptions = {...this.myDatePickerOptions,...{componentDisabled:true}};
                break;
        }
    }

    parsePrefill(){
        this.customer_name = this.prefilldata.cus_name;
        // this.date = this.prefilldata.shipment_date;
        // this.formg.get('myDate').setValue(this.prefilldata.shipment_date);
        this.setDate(this.prefilldata.shipment_date);
        this.order_id = this.prefilldata.preoder_id;
        this.total_price = this.prefilldata.total_price;
        if(this.type == ORDERADD){
            this.status_text = "เปิด";
        }else{
            this.status_text = this.prefilldata.status_text;
        }
        return {
            shipment:this.prefilldata.shipment_id ? this.prefilldata.shipment_id + '':undefined,
            remark: this.prefilldata.remark
        };
    }

    initialForm(){
        this.formg =  this._fb.group({
            address: [{value:'',disabled:true}],
            district: [{value:'',disabled:true}],
            sub_district: [{value:'',disabled:true}],
            province: [{value:'',disabled:true}],
            zipcode: [{value:'',disabled:true}],
            shipment:['',Validators.required],
            myDate: [null, Validators.required],
            remark:['']
        });
    }

    setDate(date: Date): void {
        if (!date) {
            date = new Date();
        }
        this.formg.get('myDate').setValue({ date: {
            year: date.getFullYear(),
            month: date.getMonth() + 1,
            day: date.getDate()
        }});
    }

    clearDate(): void {
        this.formg.get('myDate').setValue(null);
    }

    onShipmentSelect(id: string){
        const selectShipment = this.shipment_full_list.filter((item)=>{return item.id == id;})[0];
        if(selectShipment){
            this.formg.patchValue(selectShipment);
        }
    }

    OnInit() {
         this.listitemdata.getShipmentList(this.prefilldata.cus_id).map((res)=>{
            this.shipment_full_list = res.map((item)=>{
                return {
                    address:item.address,
                    province:item.province_text,
                    district:item.district_text,
                    sub_district:item.subdistrict_text,
                    zipcode:item.zipcode,
                    id:item.id
                }
            });
            this.shipmentlist = res.map((item)=>{
                return {
                    value:item.id,
                    text:item.text
                };
            });
            setTimeout(()=>{
                this.shipmentselect.reselect();
                this.onShipmentSelect(this.formg.get('shipment').value);
            },0);
        }).subscribe();
        this.manageBaseOnType();
    }

    submit(){
        if (this.formg.valid) {
            this.onSubmit.emit(this.getData(0));
        }
    }

    draft() {
      if (this.formg.valid) {
        this.onSubmit.emit(this.getData(1));
      }
    }

    getData(is_draft: number){
        const formvalue = this.formg.value;
        this.dateSubmit.setDate(this.formg.get('myDate').value.date.day);
        this.dateSubmit.setMonth(this.formg.get('myDate').value.date.month);
        this.dateSubmit.setFullYear(this.formg.get('myDate').value.date.year);
        return new OrderHeaderModel({
            oid:this.prefilldata ? this.prefilldata.order_id : undefined,
            is_draft: is_draft,
            remark :  formvalue.remark,
            shipment_date: this.dateSubmit,
            shipment_id: formvalue.shipment
        })
    }
}
