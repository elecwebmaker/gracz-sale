import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  ViewChild,
  Input,
  ViewChildren
} from '@angular/core';
import { BaseformComponent } from 'app/shared/baseform/baseform.component';
import { CanSubmit } from 'app/shared/model/can-submit';
import { ProductModel, IProduct } from 'app/order/model/product.model';
import { SemanticSelectComponent } from 'ng-semantic/ng-semantic';
import {
  validateDiscountPercent,
  validateDiscountValue
} from 'app/shared/validate-percent';
import { Validators, FormControl, FormBuilder } from '@angular/forms';
import { ListitemdataService } from 'app/shared/listitemdata.service';
import { OrderFormService } from 'app/order/service/order-form.service';
import { CfChoiceComponent } from 'app/shared/cf-choice/cf-choice.component';
import { ProductFormService } from 'app/order/service/product-form.service';
declare var $: any;

@Component({
  selector: 'app-order-product-form',
  templateUrl: './order-product-form.component.html',
  styleUrls: ['./order-product-form.component.scss']
})
export class OrderProductFormComponent extends BaseformComponent
  implements CanSubmit<ProductModel> {
  prefilldata: IProduct;
  @Input('mode') mode: string = "add";
  @Input('loading') loading = false;
  @Input('disabled') isDisabled = false;
  @Output() onSubmit: EventEmitter<ProductModel> = new EventEmitter<ProductModel>();
  @Output() onBack: EventEmitter<undefined> = new EventEmitter<undefined>();
  @Input() hasSubmit: boolean = true;
  @ViewChild('selectmodel') model_select: SemanticSelectComponent;
  @ViewChild('selectunits') units_select: SemanticSelectComponent;
  public productChoice: Array<any> = [];
  public modelChoice: Array<any> = [];
  public unitsChoice: Array<any> = [];
  public canScreen: boolean;
  private defaultPrice = 0;
  constructor(
    _fb: FormBuilder,
    listitemdata: ListitemdataService,
    private orderformservice: OrderFormService,
    private productService: ProductFormService
  ) {
    super(_fb, listitemdata);
  }
  OnInit() {
    $('.ui.checkbox').checkbox();
    this.get_productList();
    this.formg.get('product').valueChanges.subscribe(() => {
      this.get_modelList(this.formg.get('product').value);
      this.reset_stepInput(2);
    });
    this.formg.get('model').valueChanges.subscribe(() => {
      this.get_unitsList(this.formg.get('model').value);
      this.productService.chkScreen(this.formg.get('model').value).subscribe((res: boolean) => {
        this.canScreen = res;
        if (!this.canScreen) {
          this.formg.get('isScreen').setValue(false);
        }
      });
    });
    this.formg.get('units').valueChanges.subscribe(() => {
      this.get_defaultPrice();
      this.get_discount();
    });
    this.formg.get('isScreen').valueChanges.subscribe(() => {
      this.get_defaultPrice();
      this.get_discount();
    });
    if (this.isDisabled) {
      this.formg.disable();
    }
    this.formg.get('product').setValue(2);
  }

  initialForm(): void {
    this.formg = this._fb.group({
      product: ['', Validators.required],
      model: ['', Validators.required],
      units: ['', Validators.required],
      quantity: [1, Validators.required],
      price: ['', Validators.required],
      sp_discount: [''],
      discount: [''],
      total_price: ['', Validators.required],
      isScreen: [false]
    });
    this.formg.get('sp_discount').setValidators([<any>Validators.required, validateDiscountPercent]);
    this.formg.get('discount').setValidators([
      <any>Validators.required,
      validateDiscountValue(<FormControl>this.formg.get('sp_discount'))
    ]);
  }

  submit(): void {
    if (this.formg.valid && confirm('ข้อมูลจะถูกบันทึกลงฐานข้อมูลโดยทันที')) {
      const data = this.formg.value;
      const product = new ProductModel({
        discount: data.discount,
        discount_percent: data.sp_discount,
        gracz_type: data.product,
        id: this.prefilldata ? this.prefilldata.id : undefined,
        model: data.model,
        price: data.price,
        quantity: data.quantity,
        total_price: data.total_price,
        type: data.type,
        unit: data.units,
        unit_text: data.unit_text,
        isScrean: data.isScreen,
        default_price: this.defaultPrice,
        fgcode: undefined,
        product_name: undefined,
        status_text: undefined
      });
      this.onSubmit.emit(product);
    }
  }

  parsePrefill(): Object {
    return {
      product: this.prefilldata.gracz_type,
      type: this.prefilldata.type,
      model: this.prefilldata.model,
      units: this.prefilldata.unit,
      quantity: this.prefilldata.quantity,
      price: this.prefilldata.price,
      sp_discount: this.prefilldata.discount_percent,
      discount: this.prefilldata.discount,
      total_price: this.prefilldata.total_price,
      isScreen: this.prefilldata.isScrean
    };
  }

  cal_totalprice() {
    if(this.mode !== "view"){
      let total: number;
      total = this.formg.get('quantity').value * this.formg.get('price').value - this.formg.get('discount').value;
      total = total * 1.07;
      total = Number(parseFloat(total.toString()).toFixed(2));
      this.formg.get('total_price').setValue(total);
    }
  }

  onDiscount_pct() {
    let discount_bth: number;
    const total = this.formg.get('quantity').value * this.formg.get('price').value;
    discount_bth = total * this.formg.get('sp_discount').value / 100;
    this.formg.get('discount').setValue(discount_bth);
    this.cal_totalprice();
  }

  onDiscount_bth() {
    this.formg.get('sp_discount').markAsDirty();
    let discount_pct: number;
    if (this.formg.get('discount').value === 0) {
      discount_pct = 0;
    } else {
      const total = this.formg.get('quantity').value * this.formg.get('price').value;
      discount_pct = this.formg.get('discount').value / total * 100;
    }
    this.formg.get('sp_discount').setValue(discount_pct);
    this.cal_totalprice();
  }

  reset_stepInput(step: number) {
    if (step > 0) {
      this.unitsChoice = [];
      this.formg.get('units').setValue('');
    }
    if (step > 1) {
      this.modelChoice = [];
      this.formg.get('model').setValue('');
    }
  }

  get_productList() {
    this.listitemdata.getProductList().subscribe(res => {
      this.productChoice = res;
    });
  }

  get_modelList(p_id) {
    this.listitemdata.getModelList(p_id).subscribe(res => {
      this.modelChoice = res;
      window.setTimeout(() => {
        this.model_select.reselect();
      }, 0);
    });
  }

  get_unitsList(m_id) {
    this.listitemdata.getUnitList(m_id, this.orderformservice.cus_code).subscribe(res => {
        this.unitsChoice = res;
        window.setTimeout(() => {
          this.units_select.reselect();
        }, 0);
      });
  }

  get_defaultPrice() {
    if(this.mode != "view"){
      this.productService.getPrice(
        this.formg.get('model').value,
        this.formg.get('units').value,
        this.formg.get('isScreen').value
      ).subscribe((res: any) => {
        this.defaultPrice = res.price;
        this.formg.get('price').setValue(this.defaultPrice);
      });
    }
   
  }

  get_discount() {
    if(this.mode != "view"){
      this.productService.getPrice(
        this.formg.get('model').value,
        this.formg.get('units').value,
        this.formg.get('isScreen').value
      ).subscribe((res: any) => {
        this.formg.get('sp_discount').setValue(res.discount_percent);
        this.onDiscount_pct();
      });
    }
  }

  back() {
    this.onBack.emit();
  }
}
