import { OrderFormService } from "app/order/service/order-form.service";
import { OrderGetterService } from "app/order/order-getter.service";
import { Observable } from "rxjs/Rx";
import { IProduct } from "app/order/model/product.model";
import { Iorderform } from "app/order/form/order-form/order-form.component";

export class OrderPrefill {
    constructor(private orderformservice: OrderFormService,private ordergetterservice?: OrderGetterService){

    }

    getPrefillData():Iorderform{
        return {
            cus_name: this.orderformservice.cus_name,
            cus_id: this.orderformservice.cus_id,
            order_id: this.orderformservice.order_id,
            preoder_id: this.orderformservice.preorder_id,
            remark: this.orderformservice.remark,
            shipment_date: this.orderformservice.ship_date,
            shipment_id: this.orderformservice.shipment_id,
            status_text: this.orderformservice.status_text,
            total_price: this.orderformservice.productdata.total_price
        };
    }

    setOrderFormService(order_id: string){
        return Observable.combineLatest(this.ordergetterservice.get(order_id).map((res)=>{
            this.orderformservice.setCustomer(res.cus_id,res.cus_name,res.cus_code);
            this.orderformservice.setDetail({
                order_id:res.order_id,
                preorder_id:res.preorder_id,
                remark:res.remark,
                ship_date:res.date,
                shipment_id:res.shipment_id,
                status_text:res.status_text,
                invoice_left:res.invoice_left,
                so_left: res.so_left
            });
        }),this.ordergetterservice.getProducts(order_id).map((res)=>{
            let products: Array<IProduct> = [];
            res.map((item)=>{
                products.push({
                    discount:item.discount,
                    discount_percent:item.special_discount,
                    gracz_type: item.gracz_type,
                    id: item.id,
                    model: item.model,
                    price: item.price,
                    total_price: item.total_price,
                    type: item.type,
                    unit:item.unit,
                    unit_text: item.unit_text,
                    quantity: item.quantity,
                    isScrean:item.isScrean,
                    default_price: item.default_price,
                    fgcode: item.fgcode,
                    product_name: item.product_name,
                    status_text: item.status_text
                });
            });
            this.orderformservice.productdata.setProduct(products);
        }));
    }

}