import { TestBed, inject } from '@angular/core/testing';

import { CustomerActivateService } from './customer-activate.service';

describe('CustomerActivateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CustomerActivateService]
    });
  });

  it('should ...', inject([CustomerActivateService], (service: CustomerActivateService) => {
    expect(service).toBeTruthy();
  }));
});
