import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { UrlproviderService, LIST_LESS_ORDER, GET_ORDER, LIST_PRODUCT, GEN_CODE_PRODUCT, HAS_ORDER } from 'app/shared/urlprovider.service';
import { CustomerFormStorageService } from 'app/customer/service/customer-form-storage.service';
import { BehaviorSubject, Observable } from "rxjs/Rx";
import { ISearchList } from "app/customer/service/customer-getter.service";
import * as moment from 'moment';
import { Subject } from 'rxjs/Subject';

export interface OrderListItem{
  id: number;
  preorder_id: string;
  remark: string;
  status_text: string;
  status_id: number;
  list_quantity_text: string;
  total_price_text: string;
  customer_name:string;
  is_edit: boolean;
}

@Injectable()
export class OrderGetterService {
  resetPage = new Subject();
  searchObservable: BehaviorSubject<ISearchList> = new BehaviorSubject<ISearchList>(undefined); 
  cus_id: string;
  filter;
  constructor(private http: Http,private urlprovider: UrlproviderService,private custh_storage: CustomerFormStorageService) {

  }
  setCusId(cus_id: string){
    this.cus_id = cus_id;
  }

  list(cus_id?: boolean):Observable<Array<OrderListItem>> {
    return this.searchObservable.flatMap((filter)=>{
      let cus_id_query: string;
      if(cus_id){
        cus_id_query = '/cid/' + this.cus_id;
      }else{
        cus_id_query = '';
      }
      
      return this.http.post(this.urlprovider.getUrl(LIST_LESS_ORDER) + cus_id_query, this.filter).map((res:any)=>{
        let result;
        if(!res.model){
           return [];
        }
         result = res.model.map((item)=>{
            return {
                id: item.id,
                preorder_id: item.preorder_id,
                remark: item.remark,
                status_text: item.status_text,
                status_id: item.status,
                list_quantity_text: item.list_quantity_text,
                total_price_text: item.total_price_text,
                customer_name:item.customer_name,
                is_edit:item.is_edit
              }
        });
        return {
          model:result,
          total:res.total
        }
      });
    });
  }

  get(order_id: string){
    const parseDate = function(date_data){
      
      return moment(date_data,'YYYY-MM-DD').toDate();
    }

    return this.http.post(this.urlprovider.getUrl(GET_ORDER) + `/${order_id}`,{}).map((res: any)=>{
      const response = res.model;
      return {
        order_id:response.id,
        preorder_id:response.preorder_id,
        cus_name:response.customer_name,
        cus_id:response.customer_id,
        total_price:response.total_price,
        status_id:response.status,
        status_text:response.status_text,
        date: parseDate(response.shipment_date),
        remark:response.remark,
        shipment_id:response.shipment_address.shipment_id,
        cus_code:response.customer_code,
        so_left:response.SO_qty,
        invoice_left: response.invoice_qty,
        is_edit: response.is_edit
      }
    });

  }

  setFilter(filter:any){
    this.filter = {
      ...this.filter,
      ...filter
    }
    this.searchObservable.next(filter);
  }

  getProducts(order_id: string): Observable<Array<{
          id: string;
          type: string;
          category: string;
          model: string;
          unit: string;
          unit_text: string;
          quantity: number;
          discount: string;
          special_discount: number;
          price: number;
          gracz_type: string;
          total_price: number;
          isScrean: boolean;
          default_price: number;
          fgcode: string;
          product_name: string;
          status_text: string;
    }>> {
    return this.http.post(this.urlprovider.getUrl(LIST_PRODUCT) + `/${order_id}`,{}).map((res:any)=>{
      const response = res.model;
      return response.map((model)=>{
        return {
          id:model.id,
          type:model.type,
          gracz_type:model.category,
          model:model.model,
          unit:model.unit,
          unit_text: model.unit_text,
          quantity:model.quantity,
          discount:model.discount_value,
          special_discount:model.discount_percent,
          price:model.price,
          isScrean: (model.screen == 1),
          default_price: model.default_price,
          fgcode: model.product_code,
          product_name: model.name,
          total_price: model.total_price,
          status_text: model.status_text? model.status_text : 'ไม่ทราบสถานะ'
        };
      });
    })
  }

  getCode(cus_id:string, model: string, unit: string){
    return this.http.post(this.urlprovider.getUrl(GEN_CODE_PRODUCT),{
      model_id:model,
      unit_id:unit,
    //   customer_id:cus_id
    }).map((res:any)=>{
      const response = res.model[0];
      return {
        product_code:response.product_code,
        name:response.name
      }
    });
  }


  isThereOrder(oid: string): Observable<boolean>{
    return this.http.post(this.urlprovider.getUrl(HAS_ORDER) + "/" + oid,{}).map((res:any)=>{
      return (<boolean>res.status);
    });
  }
  
}
