import { Component, OnInit } from '@angular/core';
import { OrderFormService } from "app/order/service/order-form.service";
import { Router,ActivatedRoute } from "@angular/router";
import { ProductModel, IProduct } from "app/order/model/product.model";
import { NotifyService } from 'app/shared/notify.service';
import { TopbarService } from "app/shared/topbar.service";
@Component({
  selector: 'app-order-productedit-container',
  templateUrl: './order-productedit-container.component.html',
  styleUrls: ['./order-productedit-container.component.scss']
})
export class OrderProducteditContainerComponent implements OnInit {
  prefilldata: IProduct;
  loading = false;
  constructor(private notifyservice: NotifyService ,private orderFormService: OrderFormService,private router: Router,
  private activated: ActivatedRoute, private topbarservice: TopbarService) {

  }

  ngOnInit() {
    this.prefilldata = this.orderFormService.productdata.get(this.activated.snapshot.params['product_id']);
    this.topbarservice.setState({
        title: 'แก้ไขสินค้า #' + this.prefilldata.fgcode,
        hasButton: false
    });
  }

  onSubmit(product: ProductModel){
    this.notifyservice.showSuccess();
    this.orderFormService.productdata.edit(product.id,product);
    this.router.navigate(['/order/add']);
  }

  onBack(){
    this.router.navigate(['/order/add']);
  }
}
