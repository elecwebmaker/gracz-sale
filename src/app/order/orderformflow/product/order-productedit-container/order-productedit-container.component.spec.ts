import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderProducteditContainerComponent } from './order-productedit-container.component';

describe('OrderProducteditContainerComponent', () => {
  let component: OrderProducteditContainerComponent;
  let fixture: ComponentFixture<OrderProducteditContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderProducteditContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderProducteditContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
