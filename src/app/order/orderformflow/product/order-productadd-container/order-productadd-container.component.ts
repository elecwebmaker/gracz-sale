import { Component, OnInit } from '@angular/core';
import { ProductModel } from "app/order/model/product.model";
import { OrderFormService } from "app/order/service/order-form.service";
import { Router } from "@angular/router";
import { OrderGetterService } from "app/order/order-getter.service";
import { NotifyService } from "app/shared/notify.service";
import { TopbarService } from "app/shared/topbar.service";
let id = 0;
@Component({
    selector: 'app-order-productadd-container',
    templateUrl: './order-productadd-container.component.html',
    styleUrls: ['./order-productadd-container.component.scss']
})
export class OrderProductaddContainerComponent implements OnInit {
    loading = false;
    constructor(private notifyservice: NotifyService, private orderFormService: OrderFormService,
        private router: Router, private ordergetter: OrderGetterService,
        private topbarservice: TopbarService) {

    }

    ngOnInit() {
        this.topbarservice.setState({
            title: 'เพิ่มสินค้า',
            hasButton: false
        });
    }

    onSubmit(product: ProductModel) {
        product.id = "pn" + id;
        id += 1;
        this.loading = true;
        this.ordergetter.getCode(this.orderFormService.cus_id, product.model, product.unit).map((res) => {
            product.fgcode = res.product_code;
            product.product_name = res.name;
            this.orderFormService.productdata.addProduct(product.gen());
        }).subscribe((res) => {
            this.notifyservice.showSuccess();
            this.loading = false;
            this.router.navigate(['/order/add']);

        });


    }

    onBack() {
        this.router.navigate(['/order/add']);
    }
}
