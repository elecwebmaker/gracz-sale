import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderProductaddContainerComponent } from './order-productadd-container.component';

describe('OrderProductaddContainerComponent', () => {
  let component: OrderProductaddContainerComponent;
  let fixture: ComponentFixture<OrderProductaddContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderProductaddContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderProductaddContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
