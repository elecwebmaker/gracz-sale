import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderProductpushContainerComponent } from './order-productpush-container.component';

describe('OrderProductpushContainerComponent', () => {
  let component: OrderProductpushContainerComponent;
  let fixture: ComponentFixture<OrderProductpushContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderProductpushContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderProductpushContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
