import { Component, OnInit } from '@angular/core';
import { ProductModel } from 'app/order/model/product.model';
import { OrderFormService } from 'app/order/service/order-form.service';
import { Router, ActivatedRoute } from '@angular/router';
import { OrderSenderService } from 'app/order/service/order-sender.service';
import { OrderGetterService } from 'app/order/order-getter.service';
import { NotifyService } from "app/shared/notify.service";
import { TopbarService } from "app/shared/topbar.service";

@Component({
    selector: 'app-order-productpush-container',
    templateUrl: './order-productpush-container.component.html',
    styleUrls: ['./order-productpush-container.component.scss']
})
export class OrderProductpushContainerComponent implements OnInit {
    loading = false;
    constructor(private notifyservice: NotifyService,private orderFormService: OrderFormService,private router: Router,
    private actiavatedrouter: ActivatedRoute, private orderSender: OrderSenderService ,private ordergetter:OrderGetterService,
    private topbarservice: TopbarService) { }

    ngOnInit() {
        this.topbarservice.setState({
            title: 'เพิ่มสินค้า',
            hasButton: false
        });
    }

    onSubmit(product: ProductModel){
        this.loading = true;
        this.ordergetter.getCode(this.orderFormService.cus_id,product.model,product.unit)
        .flatMap((res) => {
            product.fgcode = res.product_code;
            product.product_name = res.name;
            return this.orderSender.pushProduct(this.actiavatedrouter.snapshot.params['order_id'], product.gen());
        })
        .subscribe((res) => {
            this.loading = false;
            this.notifyservice.showSuccess();
            this.router.navigate(['/order/edit/' + this.actiavatedrouter.snapshot.params['order_id']]);
        });
    }

    onBack(){
        this.router.navigate(['/order/edit/' + this.actiavatedrouter.snapshot.params['order_id']]);
    }
}
