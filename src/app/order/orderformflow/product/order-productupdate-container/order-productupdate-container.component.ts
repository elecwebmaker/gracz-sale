import { Component, OnInit } from '@angular/core';
import { IProduct, ProductModel } from 'app/order/model/product.model';
import { OrderFormService } from 'app/order/service/order-form.service';
import { Router, ActivatedRoute } from '@angular/router';
import { OrderSenderService } from 'app/order/service/order-sender.service';
import { OrderGetterService } from "app/order/order-getter.service";
import { NotifyService } from "app/shared/notify.service";
import { TopbarService } from "app/shared/topbar.service";

@Component({
    selector: 'app-order-productupdate-container',
    templateUrl: './order-productupdate-container.component.html',
    styleUrls: ['./order-productupdate-container.component.scss']
})
export class OrderProductupdateContainerComponent implements OnInit {
    prefilldata: IProduct;
    loading = false;
    constructor(private notiservice: NotifyService ,private orderFormService: OrderFormService, private router: Router,
    private activated: ActivatedRoute, private orderSender: OrderSenderService,private ordergetter: OrderGetterService,
    private topbarservice: TopbarService) { }

    ngOnInit() {
        this.prefilldata = this.orderFormService.productdata.get(this.activated.snapshot.params['product_id']);
        console.log("this.prefilldata",this.prefilldata);
        this.topbarservice.setState({
            title: 'แก้ไขสินค้า #' + this.prefilldata.fgcode,
            hasButton: false
        });
    }

    onSubmit(product: ProductModel){
        this.loading = true;
        this.ordergetter.getCode(this.orderFormService.cus_id,product.model,product.unit)
        .flatMap((res) => {
            product.fgcode = res.product_code;
            product.product_name = res.name;
            return this.orderSender.updateProduct(this.activated.snapshot.params['product_id'], product.gen());
        })
        .subscribe((res) => {
            this.loading = false;
            this.notiservice.showSuccess();
            this.router.navigate(['/order/edit/' + this.activated.snapshot.params['order_id']]);
        });
    }

    onBack(){
        this.router.navigate(['/order/edit/' + this.activated.snapshot.params['order_id']]);
    }
}