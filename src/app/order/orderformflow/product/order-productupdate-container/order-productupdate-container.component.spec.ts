import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderProductupdateContainerComponent } from './order-productupdate-container.component';

describe('OrderProductupdateContainerComponent', () => {
  let component: OrderProductupdateContainerComponent;
  let fixture: ComponentFixture<OrderProductupdateContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderProductupdateContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderProductupdateContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
