import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { OrderFormService } from "app/order/service/order-form.service";
import { Router, ActivatedRoute } from "@angular/router";
import { ProductModel, IProduct } from "app/order/model/product.model";
import { TopbarService } from "app/shared/topbar.service";



@Component({
    selector: 'app-order-productview-container',
    templateUrl: './order-productview-container.component.html',
    styleUrls: ['./order-productview-container.component.scss']
})
export class OrderProductviewContainerComponent implements OnInit {

    prefilldata: IProduct;
    constructor(private orderFormService: OrderFormService, private router: Router, private activated: ActivatedRoute,
        private location: Location, private topbarservice: TopbarService) { }

    ngOnInit() {
        this.prefilldata = this.orderFormService.productdata.get(this.activated.snapshot.params['product_id']);
        console.log(this.prefilldata,'product');
        this.topbarservice.setState({
            title: 'สินค้า #' + this.prefilldata.fgcode,
            hasButton: false
        });
    }

    onBack(product: ProductModel) {
        this.location.back();
    }
}
