import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderProductviewContainerComponent } from './order-productview-container.component';

describe('OrderProductviewContainerComponent', () => {
  let component: OrderProductviewContainerComponent;
  let fixture: ComponentFixture<OrderProductviewContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderProductviewContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderProductviewContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
