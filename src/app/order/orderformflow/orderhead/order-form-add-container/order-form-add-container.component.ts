import { Component, OnInit, EventEmitter, Output, ViewChild } from '@angular/core';
import { Iorderform, ORDERADD, OrderFormComponent } from "app/order/form/order-form/order-form.component";
import { OrderFormService } from "app/order/service/order-form.service";
import { OrderSenderService } from "app/order/service/order-sender.service";
import { OrderHeaderModel } from "app/order/model/order-header.model";
import { Router } from "@angular/router";
import { NotifyService } from "app/shared/notify.service";

@Component({
  selector: 'app-order-form-add-container',
  templateUrl: './order-form-add-container.component.html',
  styleUrls: ['./order-form-add-container.component.scss']
})
export class OrderFormAddContainerComponent implements OnInit {
  prefilldata:Iorderform;
  type = ORDERADD;
  draft: number;
  @Output('loading') loading : EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild(OrderFormComponent) orderformcomp:OrderFormComponent;
  constructor(private notifyservice:NotifyService ,private orderformservice: OrderFormService,private ordersender: OrderSenderService, private router: Router) { 

  }

  ngOnInit() {
    this.prefilldata = this.orderformservice.getPrefill();
  }

  onSubmit(orderdata: OrderHeaderModel){
    this.draft = orderdata.is_draft;
    this.loading.emit(true);
     this.ordersender.addProduct({
      cus_id: this.orderformservice.cus_id,
      ship_id: orderdata.shipment_id,
      ship_date: orderdata.shipment_date,
      remark: orderdata.remark,
      is_draft: orderdata.is_draft
    },this.orderformservice.productdata.list().map((product)=>{
        return product.gen();
    })).subscribe((res)=>{
      this.loading.emit(false);
      this.notifyservice.showSuccess();
      this.orderformservice.clear();
      this.router.navigate(['/order/list']);

    });
    
  }

  saveFormtoStorage(){
    const orderheader = this.orderformcomp.getData(this.draft);
    this.orderformservice.setFromHeaderModel(orderheader);

  }
}