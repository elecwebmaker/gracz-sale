import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderFormAddContainerComponent } from './order-form-add-container.component';

describe('OrderFormAddContainerComponent', () => {
  let component: OrderFormAddContainerComponent;
  let fixture: ComponentFixture<OrderFormAddContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderFormAddContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderFormAddContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
