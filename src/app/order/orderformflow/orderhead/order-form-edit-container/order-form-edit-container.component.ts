import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { OrderFormService } from "app/order/service/order-form.service";
import { Iorderform, ORDEREDIT, OrderFormComponent } from "app/order/form/order-form/order-form.component";
import { OrderPrefill } from "app/order/orderprefill";
import { OrderSenderService } from "app/order/service/order-sender.service";
import { Router } from "@angular/router";
import { OrderHeaderModel } from "app/order/model/order-header.model";
import { NotifyService } from "app/shared/notify.service";

@Component({
  selector: 'app-order-form-edit-container',
  templateUrl: './order-form-edit-container.component.html',
  styleUrls: ['./order-form-edit-container.component.scss']
})
export class OrderFormEditContainerComponent implements OnInit {
  prefillData:Iorderform;
  type = ORDEREDIT;
  orderprefill:OrderPrefill;
  draft: number;
  @Output('loading') loading : EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild(OrderFormComponent) orderformcomp:OrderFormComponent;
  
  constructor(private notifyservice:NotifyService, private orderformservice: OrderFormService,private ordersender: OrderSenderService, private router: Router) {
    this.orderprefill = new OrderPrefill(this.orderformservice);
  }

  ngOnInit() {
    this.prefillData = this.orderprefill.getPrefillData();
  }


  onSubmit(orderdata: OrderHeaderModel){
    this.draft = orderdata.is_draft;
    this.loading.emit(true);
    this.ordersender.editOrder(this.orderformservice.order_id,{
      remark: orderdata.remark,
      shipment_date: orderdata.shipment_date,
      shipment_id: orderdata.shipment_id,
      is_draft: orderdata.is_draft
    }).subscribe((res)=>{
      this.loading.emit(false);
      this.notifyservice.showSuccess();
      this.orderformservice.clear();
      this.router.navigate(['/order/list']);
    });
  }

  saveFormtoStorage(){
    const orderheader = this.orderformcomp.getData(this.draft);
    this.orderformservice.setFromHeaderModel(orderheader);

  }

}
