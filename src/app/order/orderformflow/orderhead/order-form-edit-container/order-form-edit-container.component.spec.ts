import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderFormEditContainerComponent } from './order-form-edit-container.component';

describe('OrderFormEditContainerComponent', () => {
  let component: OrderFormEditContainerComponent;
  let fixture: ComponentFixture<OrderFormEditContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderFormEditContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderFormEditContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
