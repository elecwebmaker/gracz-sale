import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderFormViewContainerComponent } from './order-form-view-container.component';

describe('OrderFormViewContainerComponent', () => {
  let component: OrderFormViewContainerComponent;
  let fixture: ComponentFixture<OrderFormViewContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderFormViewContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderFormViewContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
