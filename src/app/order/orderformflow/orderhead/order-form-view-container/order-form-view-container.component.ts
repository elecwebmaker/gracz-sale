import { Component, OnInit } from '@angular/core';
import { OrderFormService } from "app/order/service/order-form.service";
import { Iorderform, ORDERVIEW } from "app/order/form/order-form/order-form.component";
import { OrderPrefill } from "app/order/orderprefill";

@Component({
  selector: 'app-order-form-view-container',
  templateUrl: './order-form-view-container.component.html',
  styleUrls: ['./order-form-view-container.component.scss']
})
export class OrderFormViewContainerComponent implements OnInit {

  prefillData:Iorderform;
  type = ORDERVIEW;
  orderprefill:OrderPrefill;
  constructor(private orderformservice: OrderFormService) {
    this.orderprefill = new OrderPrefill(this.orderformservice);
  }

  ngOnInit() {
    console.log(this.orderprefill.getPrefillData(),'this.orderprefill.getPrefillData()');
    this.prefillData = this.orderprefill.getPrefillData();
  }

}
