import { Component, OnInit, ViewChild } from '@angular/core';
import { CFRow } from "craft-table";
import { Router } from "@angular/router";
import { OrderFormService } from "app/order/service/order-form.service";
import { OrderProductListComponent } from "app/order/orderformflow/product-table/order-product-list/order-product-list.component";
import { OrderSenderService } from "app/order/service/order-sender.service";

@Component({
  selector: 'app-order-productlist-view-container',
  templateUrl: './order-productlist-view-container.component.html',
  styleUrls: ['./order-productlist-view-container.component.scss']
})
export class OrderProductlistViewContainerComponent implements OnInit {

  constructor(private router: Router, private orderform: OrderFormService, private ordersender: OrderSenderService) { }

  ngOnInit() {
  }


  viewProduct(cfrow: {row: CFRow}){
      const pid = cfrow.row.get('id').getValue();
      this.router.navigate(['/product/view/' + pid]);
  }
}
