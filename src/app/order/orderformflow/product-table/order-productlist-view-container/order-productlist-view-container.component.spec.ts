import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderProductlistViewContainerComponent } from './order-productlist-view-container.component';

describe('OrderProductlistViewContainerComponent', () => {
  let component: OrderProductlistViewContainerComponent;
  let fixture: ComponentFixture<OrderProductlistViewContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderProductlistViewContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderProductlistViewContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
