import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderProductlistAddContainerComponent } from './order-productlist-add-container.component';

describe('OrderProductlistAddContainerComponent', () => {
  let component: OrderProductlistAddContainerComponent;
  let fixture: ComponentFixture<OrderProductlistAddContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderProductlistAddContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderProductlistAddContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
