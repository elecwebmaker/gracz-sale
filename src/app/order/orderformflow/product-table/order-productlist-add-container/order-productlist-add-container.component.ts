import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from "@angular/router";
import { IProduct } from "app/order/model/product.model";
import { CFRow } from "craft-table";
import { OrderSenderService } from "app/order/service/order-sender.service";
import { OrderFormService } from "app/order/service/order-form.service";
import { OrderProductListComponent } from "app/order/orderformflow/product-table/order-product-list/order-product-list.component";

@Component({
  selector: 'app-order-productlist-add-container',
  templateUrl: './order-productlist-add-container.component.html',
  styleUrls: ['./order-productlist-add-container.component.scss']
})
export class OrderProductlistAddContainerComponent implements OnInit {
  @ViewChild(OrderProductListComponent) productlistComp:OrderProductListComponent;
  constructor(private router: Router, private orderform: OrderFormService) { }

  ngOnInit() {

  }
  editProduct(cfrow: {row:CFRow}){
    console.log(cfrow);
    const pid = cfrow.row.get('id').getValue();
    this.router.navigate(['/product/edit/' + pid]);
  }

  deleteProduct(cfrow: {row:CFRow}){
    const pid = cfrow.row.get('id').getValue();
    this.orderform.productdata.remove(pid);
    this.productlistComp.refresh();
  }

  viewProduct(cfrow: {row:CFRow}){
    const pid = cfrow.row.get('id').getValue();
    this.router.navigate(['/product/view/' + pid]);
  }
}
