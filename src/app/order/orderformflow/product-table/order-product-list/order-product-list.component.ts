import { Component, OnInit, Input, TemplateRef, ElementRef } from '@angular/core';
import { CFTableControl } from "craft-table";
import { OrderFormService } from "app/order/service/order-form.service";
import { IProduct } from "app/order/model/product.model";
import { money_format } from 'craftutility';

@Component({
  selector: 'app-order-product-list',
  templateUrl: './order-product-list.component.html',
  styleUrls: ['./order-product-list.component.scss']
})
export class OrderProductListComponent implements OnInit {
  @Input('extendcolumn') extendcolumn: TemplateRef<ElementRef>;
  @Input('cusid') cus_id: string;
  control: CFTableControl = new CFTableControl({
    header:[
      {
        id: 'product_name',
        label: 'สินค้า'
      },
      {
        id: 'fgcode',
        label: 'รหัส'
      },
      {
        id: 'quantity_text',
        label: 'จำนวน'
      },
      {
        id: 'unit_text',
        label: 'หน่วย'
      },
      {
        id: 'total_price_text',
        label: 'ราคา'
      },
      {
        id: 'status_text',
        label: 'สถานะสั่งซื้อ'
      }
    ],
    navigating: false
  });
  constructor(private orderformservice: OrderFormService) {

  }

  ngOnInit() {
    const products = this.orderformservice.productdata.list();
    console.log(products,"products");
    this.control.setData((<Array<IProduct>>products.map((item)=>{
        return {...item.gen(),...{total_price_text: money_format(item.gen().total_price) } };
    }
    )));

  }

  refresh(){
    console.log("refres!");
    const products = this.orderformservice.productdata.list();
      this.control.setData((<Array<IProduct>>products.map((item)=>{
        return item.gen();
    }
    )));
  }
}
