import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderProductlistEditContainerComponent } from './order-productlist-edit-container.component';

describe('OrderProductlistEditContainerComponent', () => {
  let component: OrderProductlistEditContainerComponent;
  let fixture: ComponentFixture<OrderProductlistEditContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderProductlistEditContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderProductlistEditContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
