import { Component, OnInit, ViewChild } from '@angular/core';
import { CFRow } from "craft-table";
import { Router } from "@angular/router";
import { OrderFormService } from "app/order/service/order-form.service";
import { OrderProductListComponent } from "app/order/orderformflow/product-table/order-product-list/order-product-list.component";
import { OrderSenderService } from "app/order/service/order-sender.service";

@Component({
    selector: 'app-order-productlist-edit-container',
    templateUrl: './order-productlist-edit-container.component.html',
    styleUrls: ['./order-productlist-edit-container.component.scss']
})
export class OrderProductlistEditContainerComponent implements OnInit {

    constructor(private router: Router, private orderform: OrderFormService, private ordersender: OrderSenderService) { }
    @ViewChild(OrderProductListComponent) productlistComp:OrderProductListComponent;
    public deleting: boolean = false;

    ngOnInit() {

    }

    editProduct(cfrow: {row: CFRow}){
        const pid = cfrow.row.get('id').getValue();
        this.router.navigate(['/product/oid/' + this.orderform.order_id + '/update/pid/' + pid]);
    }
    deleteProduct(cfrow: {row: CFRow}){
        this.deleting = true;
        const pid = cfrow.row.get('id').getValue();
        if (confirm('ข้อมูลจะถูกบันทึกลงฐานข้อมูลโดยทันที')) {
          this.ordersender.deleteProduct(pid).subscribe(() => {
            this.orderform.productdata.remove(pid);
            this.deleting = false;
            this.productlistComp.refresh();
          });
        } else {
          this.deleting = false;
        }
    }

    viewProduct(cfrow: {row: CFRow}){
        const pid = cfrow.row.get('id').getValue();
        console.log(pid,"pid")
        this.router.navigate(['/product/view/' + pid]);
    }
}
