import { ProductModel, IProduct } from "app/order/model/product.model";

export class ProductData{
    listdata: Array<ProductModel> = [];
    total_price: number = 0;
    constructor(){

    }

    calculateTotalPrice(){
        this.total_price = 0;
        this.listdata.map((product)=>{
            this.total_price = Number(this.total_price) + Number(product.total_price);
        })
    }

    addProduct(product:IProduct){
        this.listdata.push(new ProductModel(product));
        this.calculateTotalPrice();
    }

    addProducts(products:Array<IProduct>){
        this.listdata = [...this.listdata,...(products.map((product)=>{
            return new ProductModel(product);
        }))];
        this.calculateTotalPrice();
    }

    setProduct(products:Array<IProduct>){
        this.listdata = products.map((product)=>{
            return new ProductModel(product);
        });
        this.calculateTotalPrice();
    }
    list(){
        this.calculateTotalPrice();
        return this.listdata;
    }

    get(id: string):ProductModel{
        return this.listdata.filter((product)=>{
            return product.id == id;
        })[0];
    }

    edit(id: string,editproduct: ProductModel){
        this.listdata.map((product,index)=>{
           if(product.id == id){
               this.listdata[index] = editproduct;
           }
        });
        this.calculateTotalPrice();
    }

    remove(id: string){
        this.listdata.map((product,index)=>{
           if(product.id == id){
               this.listdata.splice(index, 1);
           }
        });
        this.calculateTotalPrice();
    }

    clear(){
        this.listdata = [];
    }
}