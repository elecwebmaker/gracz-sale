import { Component, OnInit } from '@angular/core';
import { OrderGetterService } from "app/order/order-getter.service";
import { OrderFormService } from "app/order/service/order-form.service";
import { Router, ActivatedRoute } from "@angular/router";
import { OrderPrefill } from "app/order/orderprefill";
import { TopbarService } from "app/shared/topbar.service";
import { OrderSenderService } from "app/order/service/order-sender.service";
import { NotifyService } from "app/shared/notify.service";
@Component({
    selector: 'app-order-view',
    templateUrl: './order-view.component.html',
    styleUrls: ['./order-view.component.scss']
})
export class OrderViewComponent implements OnInit {
    so_left: number;
    invoice_left: number;
    loading = true;
    orderprefill: OrderPrefill;
    constructor(
        private ordergetterservice: OrderGetterService,
        private orderformservice: OrderFormService,
        private ordersetterservice: OrderSenderService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private topbarservice: TopbarService,
        private notify: NotifyService
    ) {
        this.orderprefill = new OrderPrefill(this.orderformservice, this.ordergetterservice);

    }

    ngOnInit() {
        this.orderprefill.setOrderFormService(this.activatedRoute.snapshot.params['order_id']).subscribe(() => {
            this.loading = false;
            this.so_left = this.orderformservice.so_left;
            this.invoice_left = this.orderformservice.invoice_left;
            this.topbarservice.setState({
                title: 'ดูรายการสั่งซื้อ #' + this.orderprefill.getPrefillData().preoder_id,
                hasButton: false
            });
        });
    }


    close_product() {
        this.ordersetterservice.closeProduct(this.orderprefill.getPrefillData().order_id).subscribe(() => {
            this.notify.showSuccess();
            this.router.navigate(['order/list']);
        });
    }

}
