import { Component, OnInit, OnDestroy } from '@angular/core';
import { IOrderItemModel, OrderItemModel } from 'app/order/model/order-item.model';
import { Router, ActivatedRoute } from "@angular/router";
import { OrderGetterService } from "app/order/order-getter.service";
import { Subject, Subscription } from 'rxjs';

@Component({
    selector: 'app-order-list',
    templateUrl: './order-list.component.html',
    styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit, OnDestroy {
    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
    public items: Array<IOrderItemModel> = [];
    loading = false;

    totaldata: number;
    resetPage: Subject<any>;
    subscription: Subscription;
    onNavChange({size,offset}){
        this.orderGetterService.setFilter({
            size,
            offset
        });
    }
   constructor(private orderGetterService: OrderGetterService, private router:Router, private activatedRouter: ActivatedRoute) { }

    ngOnInit() {
        this.orderGetterService.searchObservable.subscribe(()=>{
            this.loading = true;
        });
        this.subscription = this.orderGetterService.list().subscribe((res:any)=>{
             this.loading = false;
            this.items = [];
            this.totaldata = res.total;
            res.model.map((item)=>{
                this.items.push(new OrderItemModel({
                    id: item.id,
                    preorder_id: item.preorder_id,
                    list_quantity: item.list_quantity_text,
                    total_price: item.total_price_text,
                    status: item.status_id,
                    status_text: item.status_text,
                    customer_name: item.customer_name,
                    is_edit:item.is_edit
                }));
            })
        });
        this.resetPage = this.orderGetterService.resetPage;
    }
    onItemClick(item: OrderItemModel){
        if(item.is_edit){
            this.router.navigate([`../../edit/${item.id}`],{relativeTo:this.activatedRouter});
        }else{
            this.router.navigate([`../../view/${item.id}`],{relativeTo:this.activatedRouter});
        }
        
    }
}
