import { NgModule } from '@angular/core';
import { RouterModule, Routes, RouteReuseStrategy, CanActivate } from '@angular/router';
import { OutletComponent } from "app/shared/outlet/outlet.component";
import { OrderListPageComponent } from "app/order/order-list-page/order-list-page.component";
import { OrderListComponent } from "app/order/order-list/order-list.component";
import { OrderFormAddContainerComponent } from "app/order/orderformflow/orderhead/order-form-add-container/order-form-add-container.component";
import { OrderAddComponent } from "app/order/order-add/order-add.component";
import { OrderEditComponent } from "app/order/order-edit/order-edit.component";
import { OrderViewComponent } from "app/order/order-view/order-view.component";
import { OrderProductaddContainerComponent } from "app/order/orderformflow/product/order-productadd-container/order-productadd-container.component";
import { OrderProducteditContainerComponent } from "app/order/orderformflow/product/order-productedit-container/order-productedit-container.component";
import { OrderFormPageComponent } from "app/order/order-form-page/order-form-page.component";
import { CanActivateOrderheaderService } from "app/order/can-activate-orderheader.service";
import { OrderProductpushContainerComponent } from "app/order/orderformflow/product/order-productpush-container/order-productpush-container.component";
import { OrderProductviewContainerComponent } from "app/order/orderformflow/product/order-productview-container/order-productview-container.component";
import { OrderProductupdateContainerComponent } from "app/order/orderformflow/product/order-productupdate-container/order-productupdate-container.component";
import { OrderActivateService } from "app/order/order-add-activate.service";
import { CustomerActivateService } from "app/order/customer-activate.service";
import { ProductActivateService } from "app/order/product-activate.service";
import { CfAuthenJWTGuard } from "app/cfusersecure/cf-authen-guard/cf-authen-jwt-guard";
import { AuthorizeOrderGuard } from "app/core/guards/authorize-order.guard";


export const orderRoutes = [{
        path: 'order',
        component: OutletComponent,
        canActivate:[CfAuthenJWTGuard, AuthorizeOrderGuard],
        children: [
            {
                path: 'list',
                component: OrderListPageComponent,
                children:[
                    {
                        path: '',
                        component: OrderListComponent
                    }
                ]
            },
            {
                path: '',
                component: OrderFormPageComponent,
                children:[
                    {
                        path: 'add',
                        component: OrderAddComponent,
                        canDeactivate:[CanActivateOrderheaderService],
                        canActivate:[CustomerActivateService]
                    },
                    {
                        path: 'edit/:order_id',
                        component: OrderEditComponent,
                        canActivate:[OrderActivateService]
                         // add order guard check here
                    },
                    {
                        path: 'view/:order_id',
                        component: OrderViewComponent,
                        canActivate:[OrderActivateService]
                         // add order guard check here
                    }, {
                        path: '**',
                        redirectTo: '/404'
                    }
                ]
            }, {
                path: '**',
                redirectTo: '/404'
            }
        ],

    },
    {
        path: 'product',
        component: OutletComponent,
        canActivate:[CfAuthenJWTGuard],
        children: [
            {
                path:'',
                component: OrderFormPageComponent,
                children:[
                     {
                        path: 'add',
                        component: OrderProductaddContainerComponent,
                        canActivate: [CustomerActivateService]

                    },
                    {
                        path: 'edit/:product_id',
                        component: OrderProducteditContainerComponent,
                        canActivate: [CustomerActivateService, ProductActivateService]
                    },
                    {
                        path: 'oid/:order_id/push',
                        component: OrderProductpushContainerComponent,
                        canActivate:[OrderActivateService]
                    },
                    {
                        path: 'view/:product_id',
                        component: OrderProductviewContainerComponent,
                        canActivate:[ProductActivateService]
                    },
                    {
                        path: 'oid/:order_id/update/pid/:product_id',
                        component: OrderProductupdateContainerComponent,
                        canActivate:[ProductActivateService, OrderActivateService]
                        
                    }
                ]
            }, {
                path: '**',
                redirectTo: '/404'
            }
        ]
    }

];

@NgModule({
  imports: [
    RouterModule.forChild(orderRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers:[]
})


export class OrderRouting { }