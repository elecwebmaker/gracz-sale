import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot } from "@angular/router";
import { OrderFormService } from "app/order/service/order-form.service";
import { OrderGetterService } from "app/order/order-getter.service";

@Injectable()
export class OrderActivateService implements CanActivate{

  constructor(private ordergetter:OrderGetterService) { 

  }

  canActivate(route: ActivatedRouteSnapshot){
    return this.ordergetter.isThereOrder(route.params['order_id']);
  }

}
