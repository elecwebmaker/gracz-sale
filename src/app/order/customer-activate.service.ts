import { Injectable } from '@angular/core';
import { CanActivate, Router } from "@angular/router";
import { OrderFormService } from "app/order/service/order-form.service";

@Injectable()
export class CustomerActivateService implements CanActivate{

  constructor(
    private orderformService: OrderFormService,
    private router: Router) {

  }

  canActivate(){
    const hasHeadOrder = !!(this.orderformService.cus_id && this.orderformService.cus_code);
    if(!hasHeadOrder){
      this.router.navigate(['/order/list']);
    }
    return hasHeadOrder;
  }

}
