import { Component, OnInit } from '@angular/core';
import { TopbarService } from "app/shared/topbar.service";

@Component({
    selector: 'app-order-list-page',
    templateUrl: './order-list-page.component.html',
    styleUrls: ['./order-list-page.component.scss']
})
export class OrderListPageComponent implements OnInit {

    constructor(private topbarservice: TopbarService) { }

    ngOnInit() {
        this.topbarservice.setState({
            title: 'รายการสั่งซื้อ',
            hasButton: false
        })
    }

}
