import { Injectable} from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot } from "@angular/router";
import { OrderFormService } from "app/order/service/order-form.service";

@Injectable()
export class ProductActivateService implements CanActivate{

  constructor(private orderformservice: OrderFormService) { }

  canActivate(route: ActivatedRouteSnapshot){
    return !!this.orderformservice.productdata.get(route.params['product_id']);
  }

}
