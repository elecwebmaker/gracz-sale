import { Component, OnInit, OnDestroy } from '@angular/core';
import { CFTableControl } from "craft-table";
import { InventoryListItem, InventoryGetterService } from "app/inventory/inventory-getter.service";
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-inventory-list',
    templateUrl: './inventory-list.component.html',
    styleUrls: ['./inventory-list.component.scss']
})
export class InventoryListComponent implements OnInit, OnDestroy {
    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
    time_stamp: string;
    subscription: Subscription;
    currentPage:number = 1;
    currentSize:number = 10;
    observableList;

    control: CFTableControl = new CFTableControl({
        header: [{
                id: 'fgcode',
                label: 'รหัส FG'
            },{
                id: 'name',
                label: 'ชื่อ'
            }, {
                id: 'remain_qty',
                label: 'เหลือ'
            }, {
                id: 'order_qty',
                label: 'สั่งซื้อ'
            }, {
                id: 'sale_qty',
                label: 'สั่งขาย'
            }, {
                id: 'unit',
                label: 'หน่วย'
            }],
        navigating: true
    });
    constructor(private inventoryGetterService: InventoryGetterService) { 
        this.observableList = this.inventoryGetterService.list().map((res:any)=>{
            this.control.setDataTotal(res.total);
            return res.model;
        });
    }

    ngOnInit() {
        const mock_data: Array<InventoryListItem> = [];
        const that = this;
        this.inventoryGetterService.getTime().subscribe((res) => {
          this.time_stamp = res;
        });
        this.control.setGetData((offset, size) => {
            console.log("nav update");
            this.inventoryGetterService.filter.setfilter({
                offset,
                size
            })
            return this.observableList;
        });

        this.subscription = this.inventoryGetterService.resetPage.subscribe(()=>{
            this.currentPage = 1;
        });
    }

}
