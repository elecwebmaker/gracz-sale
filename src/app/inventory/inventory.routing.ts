import { NgModule } from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';
import { InventoryListContainerComponent } from "app/inventory/inventory-list-container/inventory-list-container.component";
import { CfAuthenJWTGuard } from "app/cfusersecure/cf-authen-guard/cf-authen-jwt-guard";
import { AuthorizeInventoryGuard } from "app/core/guards/authorize-inventory.guard";
export const inventoryRoutes: Routes = [
    {
        path: 'inventory',
        canActivate:[CfAuthenJWTGuard, AuthorizeInventoryGuard],
        children:[
            {
                path:'list',
                component: InventoryListContainerComponent
            },
            {
                path: '',
                redirectTo: '/inventory/list',
                pathMatch: 'full'
            }, {
                path: '**',
                redirectTo: '/404'
            }
        ]
    }
];


@NgModule({
  imports: [
    RouterModule.forChild(inventoryRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers:[]
})


export class InventoryRouting { }