import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventoryListContainerComponent } from './inventory-list-container.component';

describe('InventoryListContainerComponent', () => {
  let component: InventoryListContainerComponent;
  let fixture: ComponentFixture<InventoryListContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventoryListContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventoryListContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
