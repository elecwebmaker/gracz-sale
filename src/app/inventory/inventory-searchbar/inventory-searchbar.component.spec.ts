import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventorySearchbarComponent } from './inventory-searchbar.component';

describe('InventorySearchbarComponent', () => {
  let component: InventorySearchbarComponent;
  let fixture: ComponentFixture<InventorySearchbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventorySearchbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventorySearchbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
