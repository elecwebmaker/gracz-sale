import { Component, OnInit } from '@angular/core';
import { InventoryGetterService } from 'app/inventory/inventory-getter.service';
import { ListitemdataService } from 'app/shared/listitemdata.service';
import { FilterBar } from 'app/shared/filterbar/filterbar';
import { TopbarService } from 'app/shared/topbar.service';
export interface InventorySearch {
    key: string;
}
@Component({
    selector: 'app-inventory-searchbar',
    templateUrl: './inventory-searchbar.component.html',
    styleUrls: ['./inventory-searchbar.component.scss']
})
export class InventorySearchbarComponent implements OnInit {
    private filter = new FilterBar<InventorySearch>(['key']);
    constructor(private inventorygetter: InventoryGetterService, private topbarservice: TopbarService) { }

    ngOnInit() {
        this.topbarservice.setState({
            title: 'คลังสินค้า',
            hasButton: false
        })
        this.filter.filterChanges().subscribe((res) => {
            this.inventorygetter.setFilter(res);
        });
    }

    searchChange(val) {
        this.filter.setFilter('key', val);
    }

}
