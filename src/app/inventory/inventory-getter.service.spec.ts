import { TestBed, inject } from '@angular/core/testing';

import { InventoryGetterService } from './inventory-getter.service';

describe('InventoryGetterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InventoryGetterService]
    });
  });

  it('should ...', inject([InventoryGetterService], (service: InventoryGetterService) => {
    expect(service).toBeTruthy();
  }));
});
