import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import {
  UrlproviderService,
  LIST_INVENTORY,
  GET_TIME_STAMP
} from "app/shared/urlprovider.service";
import { Observable, Subject } from "rxjs/Rx";
import { Filterservice } from "app/shared/filterservice";

export interface InventoryListItem{
  id: string;
  fgcode: string;
  remain_qty: number;
  sale_qty: number;
  order_qty: number;
  unit: string;
  name: string;
};

export interface InventorySearch{
  key:string;
};

@Injectable()
export class InventoryGetterService {
  filter = new Filterservice<InventorySearch>();
  resetPage = new Subject();
  constructor(private http: Http,private urlprovider: UrlproviderService) {

  }

  list(): Observable<any>{
    this.filter.clearFilter();
    return this.filter.searchObservable.switchMap((filter)=>{
      const url = this.urlprovider.getUrl(LIST_INVENTORY);
      return this.http.post(url,filter).map((res:any)=>{
        const model = res.model;
        const result = (model.map((item)=>{
          return {
            id: item.id,
            fgcode: item.product_code,
            remain_qty: item.remain,
            order_qty: item.order_qty,
            sale_qty: item.sale_qty,
            unit: item.unit,
            name: item.name
          };
        }));
        return {
          model:result,
          total:res.total
        }
      });
    });
  }

  getTime() {
    const url = this.urlprovider.getUrl(GET_TIME_STAMP);
    return this.http.post(url, {}).map((res: any) => {
      return res.last_timestamp;
    });
  }

  setFilter(filter:InventorySearch){
    this.filter.setfilter(filter);
  }
}
