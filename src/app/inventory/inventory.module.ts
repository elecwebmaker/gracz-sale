import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from "app/shared/shared.module";
import { InventoryRouting } from './inventory.routing';
import { InventoryGetterService } from './inventory-getter.service';
import { InventoryListContainerComponent } from './inventory-list-container/inventory-list-container.component';
import { InventoryListComponent } from "app/inventory/inventory-list/inventory-list.component";
import { InventorySearchbarComponent } from './inventory-searchbar/inventory-searchbar.component';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    InventoryRouting
  ],
  declarations: [InventoryListContainerComponent, InventoryListComponent, InventorySearchbarComponent],
  providers: [InventoryGetterService]
})
export class InventoryModule { }
