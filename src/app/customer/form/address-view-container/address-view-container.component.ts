import { Component, OnInit, ViewChild } from '@angular/core';
import { CusDatatype } from 'app/customer/model/cus-datatype';
import { AddressModel, IAddress } from 'app/customer/model/address.model';
import { AddressFormComponent } from "app/customer/form/address-form/address-form.component";
import { BaseEditContainerComponent } from "app/customer/form/base-edit-container/base-edit-container.component";

@Component({
  selector: 'app-address-view-container',
  templateUrl: './address-view-container.component.html',
  styleUrls: ['./address-view-container.component.scss']
})
export class AddressViewContainerComponent extends BaseEditContainerComponent implements OnInit {
  prefillData: AddressModel;
  @ViewChild(AddressFormComponent) addressformcomp: AddressFormComponent;
  ngOnInit() {
    this.setModel(AddressModel);
    window.scrollTo(0, 0);
  }

  fullFillComplete(){
    this.getPreFillData(CusDatatype.address);
  }

  submit(addressModel: AddressModel){
   this.router.navigate(['../payment'], { relativeTo: this.activedroute} );
  }

  onBack() {
    this.router.navigate(['../general'], { relativeTo: this.activedroute} );
  }

}
