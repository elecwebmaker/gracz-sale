import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddressViewContainerComponent } from './address-view-container.component';

describe('AddressViewContainerComponent', () => {
  let component: AddressViewContainerComponent;
  let fixture: ComponentFixture<AddressViewContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddressViewContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddressViewContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
