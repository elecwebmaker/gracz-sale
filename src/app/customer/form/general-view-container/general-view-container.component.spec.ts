import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralViewContainerComponent } from './general-view-container.component';

describe('GeneralViewContainerComponent', () => {
  let component: GeneralViewContainerComponent;
  let fixture: ComponentFixture<GeneralViewContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralViewContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralViewContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
