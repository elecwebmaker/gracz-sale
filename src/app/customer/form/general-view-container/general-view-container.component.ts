import { Component, OnInit, ViewChild } from '@angular/core';
import { GeneralModel, phone } from 'app/customer/model/general.model';
import { CustomerFormStorageService } from 'app/customer/service/customer-form-storage.service';
import { CusDatatype } from 'app/customer/model/cus-datatype'; 
import { GeneralFormComponent } from "app/customer/form/general-form/general-form.component";
import { BaseEditContainerComponent } from "app/customer/form/base-edit-container/base-edit-container.component";

@Component({
  selector: 'app-general-view-container',
  templateUrl: './general-view-container.component.html',
  styleUrls: ['./general-view-container.component.scss']
})
export class GeneralViewContainerComponent extends BaseEditContainerComponent implements OnInit {
  prefillData:GeneralModel;
  ngOnInit() {
    window.scrollTo(0, 0);
  }

  fullFillComplete(){
    this.getPreFillData(CusDatatype.general);
  }

  submit(generalModel:GeneralModel){
    this.router.navigate(['../address'], { relativeTo: this.activedroute} );
  }

}
