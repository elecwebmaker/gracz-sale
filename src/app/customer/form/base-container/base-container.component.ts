import { Component, OnInit } from '@angular/core';
import { CustomerFormStorageService } from 'app/customer/service/customer-form-storage.service';
import { CusDatatype } from 'app/customer/model/cus-datatype';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomerSenderService } from 'app/customer/service/customer-sender.service';
import { MdSnackBar } from '@angular/material';
import { NotifyService } from 'app/shared/notify.service';
import { BaseContainerService } from 'app/customer/service/base-container.service';
@Component({
  
})
export abstract class BaseContainerComponent{
  prefillData: any;
  isDisable: boolean;
  model:any;
  isFullFillComplete: boolean = false;
  abstract submit(evt);
  constructor(
    protected basecontainerservice:BaseContainerService,
    protected router: Router,
    protected activedroute: ActivatedRoute,
  ) {
    this.basecontainerservice.cusStorage.fullfilled$.subscribe((isCompleted) => {
      if(isCompleted){
        this.fullFillComplete();
        this.isFullFillComplete = true;
      }
    });
  }

  protected saveToStorage(id: CusDatatype, data: Object) {
     this.basecontainerservice.cusStorage.set(id, data);
  }
  
  getPreFillData(id: CusDatatype){
    if (this.basecontainerservice.cusStorage.has(id)) {
      this.prefillData = new this.model(this.basecontainerservice.cusStorage.get(id));
    }
  }

  fullFillComplete(){

  }

  setModel(model){
    this.model = model;
  }

  onSubmit($event:any) {
    
    this.submit($event);
  }
}
