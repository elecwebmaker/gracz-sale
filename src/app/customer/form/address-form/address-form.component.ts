import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AddressModel, IAddress, ISubAddress } from 'app/customer/model/address.model';
import { CanSubmit } from 'app/shared/model/can-submit';
import { BaseformComponent } from 'app/shared/baseform/baseform.component';

const parse = (data:IAddress)=> {
    return {
        id: data.id,
        addr: data.address_text,
        dist: data.subaddress.sub_district + '',
        city: data.subaddress.district + '',
        prov: data.subaddress.province + '',
        code: data.zipcode,
        isBill: data.isBill
    }
}

@Component({
  selector: 'app-address-form',
  templateUrl: './address-form.component.html',
  styleUrls: ['./address-form.component.scss']
})
export class AddressFormComponent extends BaseformComponent implements CanSubmit<AddressModel>{
    @Output() onSubmit: EventEmitter<AddressModel> = new EventEmitter<AddressModel>();
    @Output() onBack: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output('onShipAddrDelete') shipAddrDelete: EventEmitter<IAddress> = new EventEmitter<IAddress>();
    prefilldata: AddressModel;
    current_b2s: number;
    isDisable: boolean = false;

    initialForm() {
        this.formg = this._fb.group({
            billAddress: [],
            shipAddress: [[{}]]
        });
    }

    parsePrefill(): Object {
        const prefilldata = this.prefilldata.gen();
        return {
            billAddress: parse(this.prefilldata.bill_address),
            shipAddress: this.prefilldata.ship_address.map((item)=> {
                return parse(item)
            })
        }
    }

    billToShip(event) {
        
        let shipvalue = this.formg.get('shipAddress').value ? this.formg.get('shipAddress').value : [{}];
        let billValue = {};
        if(event.ischecked){
            billValue = this.formg.get('billAddress').value;
        }
        let value = {...shipvalue,...billValue,...{isBill:event.ischecked}};
        
        value.id = shipvalue[event.index].id;
        this.formg.get('shipAddress').value[event.index] = value;
        this.formg.get('shipAddress').setValue(this.formg.get('shipAddress').value);
        ;
    }

    submit() {
        const parse = (data)=>{
            const sub_addr = {
                sub_district: data.dist,
                district: data.city,
                province: data.prov,
            }
            const address = {
                address_text: data.addr,
                subaddress: sub_addr,
                zipcode: data.code,
                isBill:data.isBill,
                id:data.id
            }
            return address;
        }
        if(this.formg.valid) {
            let data = new AddressModel({
                bill_address: parse(this.formg.get('billAddress').value),
                ship_address: this.formg.get('shipAddress').value.map(parse)
            })
            this.onSubmit.emit(data);
       }
    }

    back(){
        this.onBack.emit();
    }

    parseValuetoIAddr(item): IAddress {
        const sub_tmp: ISubAddress = {
            sub_district: item.dist,
            district: item.city,
            province: item.prov
        }
        const tmp: IAddress = {
            id: item.id,
            address_text: item.addr,
            subaddress: sub_tmp,
            zipcode: item.code
        }
        return tmp;
    }

    onDelete(event) {
        this.shipAddrDelete.emit(this.parseValuetoIAddr(event));
    }

    addShipAddress(address: IAddress) {
        this.formg.get('shipAddress').setValue([...this.formg.get('shipAddress').value, parse(address)]);
    }


}
