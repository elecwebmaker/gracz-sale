import { Component, OnInit, ViewChild } from '@angular/core';
import { BaseContainerComponent } from 'app/customer/form/base-container/base-container.component';
import { CusDatatype } from 'app/customer/model/cus-datatype';
import { AddressModel, IAddress } from 'app/customer/model/address.model';
import { AddressFormComponent } from "app/customer/form/address-form/address-form.component";


@Component({
  selector: 'app-address-container',
  templateUrl: './address-container.component.html',
  styleUrls: ['./address-container.component.scss']
})
export class AddressContainerComponent extends BaseContainerComponent implements OnInit{
  prefillData: AddressModel;
  @ViewChild(AddressFormComponent) addressformcomp: AddressFormComponent;
  ngOnInit() {
    this.setModel(AddressModel);
    this.getPreFillData(CusDatatype.address);
    window.scrollTo(0, 0);

  }

  submit(addressModel: AddressModel){
   this.saveToStorage(CusDatatype.address, addressModel);
   this.router.navigate(['../payment'], { relativeTo: this.activedroute} );
  }

  onBack() {
    this.router.navigate(['../general'], { relativeTo: this.activedroute} );
  }

}
