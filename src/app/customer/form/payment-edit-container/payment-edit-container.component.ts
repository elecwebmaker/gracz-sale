import { Component, OnInit, ViewChild } from '@angular/core';
import { CusDatatype } from 'app/customer/model/cus-datatype';
import { PaymentModel, discount_type, pay_method_type, Idatacon } from 'app/customer/model/payment.model';
import { GeneralModel, Igeneral } from "app/customer/model/general.model";
import { AddressModel, IMainAddress } from "app/customer/model/address.model";
import { Observable } from "rxjs/Rx";
import { BaseEditContainerComponent } from "app/customer/form/base-edit-container/base-edit-container.component";
import { Itemupload } from "app/shared/uploader.service";
import { PaymentFormComponent } from "app/customer/form/payment-form/payment-form.component";


@Component({
  selector: 'app-payment-edit-container',
  templateUrl: './payment-edit-container.component.html',
  styleUrls: ['./payment-edit-container.component.scss']
})
export class PaymentEditContainerComponent extends BaseEditContainerComponent implements OnInit {
  loading:boolean = false;
  hasGetData = false;
  @ViewChild(PaymentFormComponent) paymentform: PaymentFormComponent;

  ngOnInit() {
    
    window.scrollTo(0, 0);
  }

  fullFillComplete(){
    this.setModel(PaymentModel);
    this.getPreFillData(CusDatatype.payment);
  }

  submit(paymentModel: PaymentModel) {
    this.saveToStorage(CusDatatype.payment, paymentModel);
    this.addCustomer();
  }
  onBack() {
    this.router.navigate(['../address'], { relativeTo: this.activedroute} );
  }

  deleteFile(file:Itemupload){
    this.basecontainerservice.customersender.deleteFile(file.id).subscribe(()=>{
      this.basecontainerservice.notifyhandler.showSuccessDelete();
    },()=>{
      this.paymentform.addFile(file);
      this.basecontainerservice.notifyhandler.showErrorDelete();
    });
  }

  addCustomer(){
    this.loading = true;
    this.basecontainerservice.customersender.edit({
      address: new AddressModel(<IMainAddress>this.basecontainerservice.cusStorage.get(CusDatatype.address)),
      general: new GeneralModel(<Igeneral>this.basecontainerservice.cusStorage.get(CusDatatype.general)),
      payment: new PaymentModel(<Idatacon>this.basecontainerservice.cusStorage.get(CusDatatype.payment))
    }).map(()=>{
      this.loading = false;
    })
    .subscribe((res) => {
      console.log('test', res);
      this.basecontainerservice.notifyhandler.showSuccess();
      this.router.navigate(['/customer/list'], { relativeTo: this.activedroute} );
    }, () => {
      this.loading = false;
      this.basecontainerservice.notifyhandler.showError();
    });
  }
}
