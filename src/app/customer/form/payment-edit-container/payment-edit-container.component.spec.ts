import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentEditContainerComponent } from './payment-edit-container.component';

describe('PaymentEditContainerComponent', () => {
  let component: PaymentEditContainerComponent;
  let fixture: ComponentFixture<PaymentEditContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentEditContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentEditContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
