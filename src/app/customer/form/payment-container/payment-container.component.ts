import { Component, OnInit } from '@angular/core';
import { BaseContainerComponent } from 'app/customer/form/base-container/base-container.component';
import { CusDatatype } from 'app/customer/model/cus-datatype';
import { PaymentModel, discount_type, pay_method_type, Idatacon } from 'app/customer/model/payment.model';
import { GeneralModel, Igeneral } from "app/customer/model/general.model";
import { AddressModel, IMainAddress } from "app/customer/model/address.model";
import { Observable } from "rxjs/Rx";

@Component({
  selector: 'app-payment-container',
  templateUrl: './payment-container.component.html',
  styleUrls: ['./payment-container.component.scss']
})
export class PaymentContainerComponent extends BaseContainerComponent implements OnInit{
  loading: boolean = false;
  prefillData:PaymentModel;
  ngOnInit() {
     this.setModel(PaymentModel);
    window.scrollTo(0, 0);
    this.getPreFillData(CusDatatype.payment);
    //  this.setModel(PaymentModel);
    // this.saveToStorage(CusDatatype.payment, new PaymentModel({
    //   cond_bill: 'cond_bill',
    //   cond_cheque: 'cond_cheque',
    //   discount: {
    //     type: discount_type.normal,
    //     value: '3'
    //   },
    //   expected_income: '222',
    //   file: '3434',
    //   loan_amount: '545',
    //   remark: '4343',
    //   payment_method: {
    //     type: pay_method_type.bank
    //   }
    // }));
    // this.getPreFillData(CusDatatype.payment);
    // window.scrollTo(0, 0);
  }

  submit(paymentModel: PaymentModel) {
    this.saveToStorage(CusDatatype.payment, paymentModel);
    this.addCustomer();
  }
  onBack() {
    this.router.navigate(['../address'], { relativeTo: this.activedroute} );
  }

  addCustomer(){
    this.loading = true;
    this.basecontainerservice.customersender.add({
      address: new AddressModel(<IMainAddress>this.basecontainerservice.cusStorage.get(CusDatatype.address)),
      general: new GeneralModel(<Igeneral>this.basecontainerservice.cusStorage.get(CusDatatype.general)),
      payment: new PaymentModel(<Idatacon>this.basecontainerservice.cusStorage.get(CusDatatype.payment))
    }).map(()=>{
      this.loading = false;
    })
    .subscribe((res) => {
      this.basecontainerservice.notifyhandler.showSuccess();
      this.router.navigate(['/customer/list'], { relativeTo: this.activedroute} );
    },()=>{
      this.loading = false;
      this.basecontainerservice.notifyhandler.showError();
    });
  }
}
