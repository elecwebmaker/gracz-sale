import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddressEditContainerComponent } from './address-edit-container.component';

describe('AddressEditContainerComponent', () => {
  let component: AddressEditContainerComponent;
  let fixture: ComponentFixture<AddressEditContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddressEditContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddressEditContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
