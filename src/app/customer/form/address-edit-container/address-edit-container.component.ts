import { Component, OnInit, ViewChild } from '@angular/core';
import { CusDatatype } from 'app/customer/model/cus-datatype';
import { AddressModel, IAddress } from 'app/customer/model/address.model';
import { AddressFormComponent } from "app/customer/form/address-form/address-form.component";
import { BaseEditContainerComponent } from "app/customer/form/base-edit-container/base-edit-container.component";

@Component({
  selector: 'app-address-edit-container',
  templateUrl: './address-edit-container.component.html',
  styleUrls: ['./address-edit-container.component.scss']
})
export class AddressEditContainerComponent extends BaseEditContainerComponent implements OnInit {
 prefillData: AddressModel;
 hasGetData = false;
  @ViewChild(AddressFormComponent) addressformcomp: AddressFormComponent;
  ngOnInit() {
    this.setModel(AddressModel);
    window.scrollTo(0, 0);
  }

  fullFillComplete(){
    this.setModel(AddressModel);
    this.getPreFillData(CusDatatype.address);
  }

  submit(addressModel: AddressModel){
   this.saveToStorage(CusDatatype.address, addressModel);
   this.router.navigate(['../payment'], { relativeTo: this.activedroute} );
  }

  onBack() {
    this.router.navigate(['../general'], { relativeTo: this.activedroute} );
  }

  deleteShipAddr(shipaddr: IAddress) {
    if(shipaddr.id){
      this.customershipaddr.remove(shipaddr.id).subscribe(()=>{
        this.basecontainerservice.notifyhandler.showSuccessDelete();
      }, () => {
          this.addressformcomp.addShipAddress(shipaddr);
          this.basecontainerservice.notifyhandler.showErrorDelete();
      });
    }
  }

}
