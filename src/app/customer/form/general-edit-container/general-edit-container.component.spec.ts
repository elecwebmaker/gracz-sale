import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralEditContainerComponent } from './general-edit-container.component';

describe('GeneralEditContainerComponent', () => {
  let component: GeneralEditContainerComponent;
  let fixture: ComponentFixture<GeneralEditContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralEditContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralEditContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
