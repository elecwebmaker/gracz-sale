import { Component, OnInit, ViewChild } from '@angular/core';
import { GeneralModel, phone } from 'app/customer/model/general.model';
import { CustomerFormStorageService } from 'app/customer/service/customer-form-storage.service';
import { CusDatatype } from 'app/customer/model/cus-datatype'; 
import { GeneralFormComponent } from "app/customer/form/general-form/general-form.component";
import { BaseEditContainerComponent } from "app/customer/form/base-edit-container/base-edit-container.component";
@Component({
  selector: 'app-general-edit-container',
  templateUrl: './general-edit-container.component.html',
  styleUrls: ['./general-edit-container.component.scss']
})
export class GeneralEditContainerComponent extends BaseEditContainerComponent implements OnInit {

  prefillData:GeneralModel;
  @ViewChild(GeneralFormComponent) generalformcomp: GeneralFormComponent;
  ngOnInit() {
    
    window.scrollTo(0, 0);
  }

  fullFillComplete(){
    this.setModel(GeneralModel);
    this.getPreFillData(CusDatatype.general);
  }

  submit(generalModel:GeneralModel){
    this.saveToStorage(CusDatatype.general, generalModel);
    this.router.navigate(['../address'], { relativeTo: this.activedroute} );
  }

  deletePhone(phone:phone){
    if(phone.id){
          this.customerphone.remove(phone.id).subscribe(()=>{
          this.basecontainerservice.notifyhandler.showSuccessDelete();
      }, () => {
          this.generalformcomp.addPhone(phone);
          this.basecontainerservice.notifyhandler.showErrorDelete();
      });
    }
  }

}
