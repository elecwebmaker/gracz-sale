import { Component, OnInit, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { CanSubmit } from 'app/shared/model/can-submit';
import { PaymentModel, discount_type, pay_method_type, Idiscount, Ipayment_method } from 'app/customer/model/payment.model';
import { BaseformComponent } from 'app/shared/baseform/baseform.component';
import { Itemupload } from "app/shared/uploader.service";

declare var $: any;

@Component({
    selector: 'app-payment-form',
    templateUrl: './payment-form.component.html',
    styleUrls: ['./payment-form.component.scss']
})
export class PaymentFormComponent extends BaseformComponent implements CanSubmit<PaymentModel> {
    @Output() onBack: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() onSubmit: EventEmitter<PaymentModel> = new EventEmitter<PaymentModel>();
    @Output() onDelteFile = new EventEmitter<Itemupload>();
    prefilldata: PaymentModel;
    isDisable: boolean = true;
    uploadedList:Array<Itemupload> = [];

    public filled_discount: FormControl;
    public filled_credit: FormControl;
    public errorMsg = {
        required: 'กรุณาใส่ข้อมูล',
        upload: 'กรุณาอัพโหลดไฟล์'
    }

    initialForm(): void {
        this.filled_discount = new FormControl('');
        this.filled_credit = new FormControl('');
        this.formg = this._fb.group({
            discount_type: [0],
            discount_value: ['5'],
            pay_method: [1],
            pay_credit: ['30'],
            cond_bill: [''],
            cond_cheque: [''],
            loan_amount: ['', [Validators.required]],
            remark: [''],
            expected_income: [''],
        });
    }
    
    parsePrefill(): Object {
        const prefilldata = this.prefilldata.gen();
        this.checkFill();
        if(this.prefilldata.file){
                this.uploadedList = this.prefilldata.file.map((res) => {
                    return {
                        id: res.id,
                        name: res.name,
                        url: res.ref
                    };
                });
        }
        return {
            discount_type: this.prefilldata.discount.type,
            discount_value: this.prefilldata.discount.value + '',
            pay_method: this.prefilldata.payment_method.type,
            pay_credit: this.prefilldata.payment_method.value + '',
            cond_bill: this.prefilldata.cond_bill,
            cond_cheque: this.prefilldata.cond_cheque,
            loan_amount: this.prefilldata.loan_amount,
            remark: this.prefilldata.remark,
            expected_income: this.prefilldata.expected_income
        }
    }

    fillDiscount(item) {
        switch(item) {
            case 5:     break;
            case 10:    break;
            case 15:    break;
            case 20:    break;
            default:    return true;
        }
    }

    fillCredit(item) {
        switch(item) {
            case 30:    break;
            case 45:    break;
            case 60:    break;
            default:    return true;
        }
    }

    checkFill(){
        if(this.fillDiscount(this.prefilldata.discount.value) && this.prefilldata.discount.value) {
            this.filled_discount.setValue(this.prefilldata.discount.value);
            this.prefilldata.discount.value = 'order';
        }
        if(this.fillCredit(this.prefilldata.payment_method.value) && this.prefilldata.payment_method.value) {
            this.filled_credit.setValue(this.prefilldata.payment_method.value);
            this.prefilldata.payment_method.value = 'order';
        }
    }

    checkRadio() {
        if(this.formg.get('discount_value').value == "order") {
            this.formg.get('discount_value').setValue(this.filled_discount.value);
        }
        if(this.formg.get('pay_credit').value == "order") {
            this.formg.get('pay_credit').setValue(this.filled_credit.value);
        }
    }

    deleteFile(file:Itemupload){
        console.log(file,"file");
        if(file.id){
            this.onDelteFile.next(file);
        }
    }

    addFile(file:Itemupload){
        this.uploadedList.push(file);
    }

    submit() {
        this.checkRadio();
        if(this.formg.valid && this.uploadedList.length > 0) {
            if(this.formg.get('discount_value').value == 'order') {
                this.formg.get('discount_value').setValue(this.filled_discount.value);
            }
            if(this.formg.get('pay_credit').value == 'order') {
                this.formg.get('pay_credit').setValue(this.filled_credit.value);
            }
            const discount: Idiscount = {
                type: this.formg.get('discount_type').value,
                value: (this.formg.get('discount_type').value === 1) ? this.formg.get('discount_value').value : ''
            }
            const method: Ipayment_method = {
                type: this.formg.get('pay_method').value,
                value: (this.formg.get('pay_method').value === 3) ? this.formg.get('pay_credit').value : ''
            }
            const data: PaymentModel = new PaymentModel({
                discount: discount,
                payment_method: method,
                cond_bill: this.formg.get('cond_bill').value,
                cond_cheque: this.formg.get('cond_cheque').value,
                loan_amount: this.formg.get('loan_amount').value,
                expected_income: this.formg.get('expected_income').value,
                remark: this.formg.get('remark').value,
                file:this.uploadedList.map((res)=>{
                    return {
                        name: res.name,
                        ref: res.url,
                        id: res.id
                    };
                })
            });
            this.onSubmit.emit(data);
        }
    }
    back(){
        this.onBack.emit(true);
    }

    onUpload(item: Itemupload){
        this.uploadedList = [...this.uploadedList, item];
    }
    
}
