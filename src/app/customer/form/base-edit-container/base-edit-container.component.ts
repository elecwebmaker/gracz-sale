import { Component, OnInit, ChangeDetectorRef, Renderer2 } from '@angular/core';
import { CustomerFormStorageService } from 'app/customer/service/customer-form-storage.service';
import { CusDatatype } from 'app/customer/model/cus-datatype';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomerSenderService } from 'app/customer/service/customer-sender.service';
import { MdSnackBar } from '@angular/material';
import { NotifyService } from 'app/shared/notify.service';
import { CustomerPhoneService } from 'app/customer/service/customer-phone.service';
import { CustomerShipaddressService } from 'app/customer/service/customer-shipaddress.service';
import { CustomerGetterService } from 'app/customer/service/customer-getter.service';
import { BaseContainerComponent } from 'app/customer/form/base-container/base-container.component';
import { BaseContainerService } from 'app/customer/service/base-container.service';

@Component({})
export abstract class  BaseEditContainerComponent extends BaseContainerComponent{
  constructor(
    basecontainerservice:BaseContainerService,
    router: Router,
    activedroute: ActivatedRoute,
    protected customerphone: CustomerPhoneService,
    protected customershipaddr: CustomerShipaddressService
  ) {
    super(basecontainerservice, router, activedroute);
  }

}
