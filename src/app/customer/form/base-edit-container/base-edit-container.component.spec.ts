import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseEditContainerComponent } from './base-edit-container.component';

describe('BaseEditContainerComponent', () => {
  let component: BaseEditContainerComponent;
  let fixture: ComponentFixture<BaseEditContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseEditContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseEditContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
