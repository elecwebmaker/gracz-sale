import { Component, OnInit, ViewChild } from '@angular/core';
import { GeneralModel, phone } from 'app/customer/model/general.model';
import { CustomerFormStorageService } from 'app/customer/service/customer-form-storage.service';
import { CusDatatype } from 'app/customer/model/cus-datatype'; 
import { BaseContainerComponent } from 'app/customer/form/base-container/base-container.component';
import { GeneralFormComponent } from "app/customer/form/general-form/general-form.component";
@Component({
  selector: 'app-general-container',
  templateUrl: './general-container.component.html',
  styleUrls: ['./general-container.component.scss']
})
export class GeneralContainerComponent extends BaseContainerComponent implements OnInit{
  prefillData:GeneralModel;
  ngOnInit() {
    this.setModel(GeneralModel);
    window.scrollTo(0, 0);
    this.getPreFillData(CusDatatype.general);

  }

  submit(generalModel:GeneralModel){
    this.saveToStorage(CusDatatype.general, generalModel);
    this.router.navigate(['../address'], { relativeTo: this.activedroute} );
  }

}
