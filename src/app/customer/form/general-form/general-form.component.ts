import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from '@angular/forms';
import { CanSubmit } from 'app/shared/model/can-submit';
import { GeneralModel, phone } from 'app/customer/model/general.model';
import { BaseformComponent } from 'app/shared/baseform/baseform.component';
import { ListDataModel } from 'app/shared/listitemdata.service';
import { Observable } from 'rxjs/Rx';

declare var $: any;
@Component({
  selector: 'app-general-form',
  templateUrl: './general-form.component.html',
  styleUrls: ['./general-form.component.scss']
})
export class GeneralFormComponent extends BaseformComponent
  implements CanSubmit<GeneralModel> {
  @Output()
  onSubmit: EventEmitter<GeneralModel> = new EventEmitter<GeneralModel>();
  @Output('onPhoneDelete') phoneDelete: EventEmitter<phone> = new EventEmitter<phone>();
  prefilldata: GeneralModel;
  isDisable: boolean = false;
  sales: Array<ListDataModel>;
  bussinessType: Array<ListDataModel>;
  customerZone: Array<ListDataModel>;
  public errorMsg: { required: string; email: string } = {
    required: 'กรุณากรอกข้อมูล',
    email: 'อีเมลล์ไม่ถูกต้อง'
  };
  OnInit() {
    const sale = this.listitemdata.getSales().map(res => {
      this.sales = res;
      if (this.formg) {
        this.formg.get('sellID').setValue(this.sales[0].id);
      }
    });
    const bussinessType = this.listitemdata.getBussinessType().map(res => {
      this.bussinessType = res;
    });
    const customerZone = this.listitemdata.getCustomerZoneList().map((res: any) => {
      this.customerZone = res;
    });
    this.resolveObservable = [...this.resolveObservable, sale, bussinessType, customerZone];
  }

  parsePrefill() {
    const prefilldata = this.prefilldata.gen();
    return {
      customerName: prefilldata.name_cus,
      contactName: prefilldata.name_contact,
      faxNumber: prefilldata.fax,
      phoneData: prefilldata.tels,
      email: prefilldata.email,
      businessType: prefilldata.type_business,
      taxID: prefilldata.ident_tax,
      sellID: prefilldata.code_sale,
      customerZone: prefilldata.customer_zone
    };
  }

  initialForm() {
    this.formg = this._fb.group({
      customerName: ['', [Validators.required]],
      contactName: ['', Validators.required],
      faxNumber: [''],
      phoneData: ['', [Validators.required]],
      email: ['', Validators.email],
      businessType: ['', Validators.required],
      taxID: ['', Validators.required],
      sellID: ['', Validators.required],
      customerZone: ['', Validators.required]
    });
  }

  submit() {
    if (this.formg.get('email').value === '') {
      this.formg.get('email').setErrors(null);
    }
    if (this.formg.valid) {
      this.onSubmit.emit(
        new GeneralModel({
          name_cus: this.formg.get('customerName').value,
          name_contact: this.formg.get('contactName').value,
          tels: this.formg.get('phoneData').value,
          fax: this.formg.get('faxNumber').value,
          email: this.formg.get('email').value,
          type_business: this.formg.get('businessType').value,
          ident_tax: this.formg.get('taxID').value,
          code_sale: this.formg.get('sellID').value,
          customer_zone: this.formg.get('customerZone').value
        })
      );
    }
  }

  onDelete(event) {
    this.phoneDelete.emit(event);
  }

  addPhone(phone: phone) {
    this.formg.get('phoneData').setValue([...this.formg.get('phoneData').value, phone]);
  }
}