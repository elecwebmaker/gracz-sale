import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentViewContainerComponent } from './payment-view-container.component';

describe('PaymentViewContainerComponent', () => {
  let component: PaymentViewContainerComponent;
  let fixture: ComponentFixture<PaymentViewContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentViewContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentViewContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
