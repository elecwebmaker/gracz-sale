import { Component, OnInit } from '@angular/core';
import { CusDatatype } from 'app/customer/model/cus-datatype';
import { PaymentModel, discount_type, pay_method_type, Idatacon } from 'app/customer/model/payment.model';
import { GeneralModel, Igeneral } from "app/customer/model/general.model";
import { AddressModel, IMainAddress } from "app/customer/model/address.model";
import { Observable } from "rxjs/Rx";
import { BaseEditContainerComponent } from "app/customer/form/base-edit-container/base-edit-container.component";

@Component({
  selector: 'app-payment-view-container',
  templateUrl: './payment-view-container.component.html',
  styleUrls: ['./payment-view-container.component.scss']
})
export class PaymentViewContainerComponent extends BaseEditContainerComponent implements OnInit {
  loading:boolean = false;
  hasGetData = false;
  ngOnInit() {
    this.setModel(PaymentModel);
    window.scrollTo(0, 0);
  }

  fullFillComplete(){
    this.getPreFillData(CusDatatype.payment);
  }

  submit(paymentModel: PaymentModel) {

  }
  onBack() {
    this.router.navigate(['../address'], { relativeTo: this.activedroute} );
  }

}
