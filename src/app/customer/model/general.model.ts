import { IFormModel } from './form.model';
export interface phone{
    code: string;
    number: string;
    id?: string;
}
export interface Igeneral{
    name_cus: string;
    name_contact: string;
    tels: Array<phone>;
    fax: string;
    email: string;
    type_business: string;
    ident_tax: string;
    code_sale: string;
    customer_zone: string;
}
export class GeneralModel implements IFormModel{
    name_cus: string;
    name_contact: string;
    tels: Array<phone>;
    fax: string;
    email: string;
    type_business: string;
    ident_tax: string;
    code_sale: string;
    customer_zone: string;
    constructor(data: Igeneral){
        this.name_contact = data.name_contact;
        this.name_cus = data.name_cus;
        this.tels = data.tels;
        this.fax = data.fax;
        this.email = data.email;
        this.type_business  = data.type_business;
        this.ident_tax = data.ident_tax;
        this.code_sale = data.code_sale;
        this.customer_zone = data.customer_zone;
    }

    gen(): Igeneral{
        return {
            name_cus: this.name_cus,
            name_contact: this.name_contact,
            tels: this.tels,
            fax: this.fax,
            email: this.email,
            type_business: this.type_business,
            ident_tax: this.ident_tax,
            code_sale: this.code_sale,
            customer_zone: this.customer_zone
        }
    }

    addPhone(phone:phone){
        this.tels.push(phone);
    }
}