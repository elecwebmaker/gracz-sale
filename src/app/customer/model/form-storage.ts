import { BehaviorSubject } from 'rxjs/Rx';

export class FormStorage {
    datalist: object = {};
    hasFullFilled: boolean;
    fullfilled$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    protected _set(id: string, data: object): void{
        this.datalist[id] = data;
    }

    remove(id: string): void{
        delete this.datalist[id];
    }

    _get(id: string): object{
        return this.datalist[id];
    }

    protected _has(id: string): boolean{
        return this.datalist[id] ? true : false;
    }

    clear(){
        this.fullfilled$.next(false);
        this.datalist = {};
    }

    complete(){
        this.fullfilled$.next(true);
    }

}