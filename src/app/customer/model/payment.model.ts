import { IFormModel } from "app/customer/model/form.model";

export enum discount_type {
    normal,
    company
}
export enum pay_method_type{
    bank,
    cash,
    credit
}
export interface Ipayment_method{
    type: pay_method_type;
    value?: string;
}
export interface Idiscount{
    type: discount_type;
    value?: string;
}
export interface Ifile{
    id: string;
    ref:string;
    name:string;
}
export interface Idatacon{
    discount: Idiscount;
    payment_method: Ipayment_method;
    cond_bill: string;
    cond_cheque: string;
    loan_amount: string;
    remark: string;
    expected_income: string;
    file: Array<Ifile>;
}
export class PaymentModel implements IFormModel{
    expected_income: any;
    discount: Idiscount;
    payment_method: Ipayment_method;
    cond_bill: string;
    cond_cheque: string;
    loan_amount: string;
    remark: string;
    file: Array<Ifile>;
    constructor(data: Idatacon){
        this.expected_income = data.expected_income;
        this.discount = data.discount;
        this.payment_method = data.payment_method;
        this.cond_bill = data.cond_bill;
        this.cond_cheque = data.cond_cheque;
        this.loan_amount = data.loan_amount;
        this.remark = data.remark;
        this.file = data.file;
    }
    gen(): Idatacon {
        return {
            discount: this.discount,
            payment_method: this.payment_method,
            cond_bill: this.cond_bill,
            cond_cheque: this.cond_cheque,
            loan_amount: this.loan_amount,
            remark: this.remark,
            expected_income: this.expected_income,
            file: this.file
        }
    }
}

