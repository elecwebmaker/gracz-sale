export interface ICusItemModel {
    id: number;
    name: string;
    status: number;
    status_text: string;
    phone_number: string;
    time: string;
    is_edit: boolean;
}

export class CustomerItemModel {
    id: number;
    name: string;
    status: number;
    status_text: string;
    phone_number: string;
    time: string;
    is_edit: boolean;
    constructor(data: ICusItemModel) {
        this.id = data.id;
        this.name = data.name;
        this.status = data.status;
        this.time = data.time;
        this.status_text = data.status_text;
        this.phone_number = data.phone_number;
        this.is_edit = data.is_edit;
    }

    gen(): ICusItemModel {
        return {
            id: this.id,
            name: this.name,
            status: this.status,
            time: this.time,
            status_text: this.status_text,
            phone_number: this.phone_number,
            is_edit: this.is_edit
        }
    }
}