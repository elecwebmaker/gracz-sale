import { IFormModel } from './form.model';
export interface ISubAddress{
    sub_district: string;
    district: string;
    province: string;
}
export interface IAddress{
    id?: number,
    address_text: string;
    subaddress: ISubAddress;
    zipcode: string;
    isBill?: boolean;
}
export interface IMainAddress{
    bill_address: IAddress;
    ship_address: Array<IAddress>;
}
export class AddressModel implements IFormModel{
    bill_address: IAddress;
    ship_address: Array<IAddress>;
    constructor(data: IMainAddress){
        this.bill_address = data.bill_address;
        this.ship_address = data.ship_address;

    }
    gen(): IMainAddress{
        const bill_address = this.bill_address;
        const ship_address = this.ship_address;
        return {
            bill_address,
            ship_address
        };
    }

    addShipAddress(address: IAddress) {
        this.ship_address.push(address);
    }
}

