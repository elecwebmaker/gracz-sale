import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { StepCustomerComponent } from './step-customer/step-customer.component';
import { AddressFormComponent } from './form/address-form/address-form.component';
import { GeneralFormComponent } from './form/general-form/general-form.component';
import { GeneralContainerComponent } from './form/general-container/general-container.component';
import { CustomerAddComponent } from './customer-add/customer-add.component';
import { CustomerRouting } from './customer.routing';
import { CustomerFormStorageService } from './service/customer-form-storage.service';
import { PaymentFormComponent } from './form/payment-form/payment-form.component';
import { AddressContainerComponent } from './form/address-container/address-container.component';
import { BaseContainerComponent } from './form/base-container/base-container.component';
import { PaymentContainerComponent } from './form/payment-container/payment-container.component';
import { CustomerSenderService } from './service/customer-sender.service';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { CustomerItemComponent } from './customer-item/customer-item.component';
import { CustomerPhoneService } from './service/customer-phone.service';
import { CustomerShipaddressService } from './service/customer-shipaddress.service';
import { CustomerDetailComponent } from './customer-detail/customer-detail.component';
import { CustomerListPageComponent } from './../customer-list-page/customer-list-page.component';
import { UploadfileComponent } from './uploadfile/uploadfile.component';
import { UploadlistComponent } from './uploadlist/uploadlist.component';
import { CustomerEditComponent } from './customer-edit/customer-edit.component';
import { GeneralEditContainerComponent } from './form/general-edit-container/general-edit-container.component';
import { AddressEditContainerComponent } from './form/address-edit-container/address-edit-container.component';
import { PaymentEditContainerComponent } from './form/payment-edit-container/payment-edit-container.component';
import { BaseEditContainerComponent } from './form/base-edit-container/base-edit-container.component';
import { CustomerGetterService } from './service/customer-getter.service';
import { CustomerGetPageComponent } from './customer-get-page/customer-get-page.component';
import { CustomerDetailContainerComponent } from './customer-detail-container/customer-detail-container.component';
import { OrdercusTableComponent } from './ordercus-table/ordercus-table.component';
import { BaseContainerService } from './service/base-container.service';
import { GeneralViewContainerComponent } from './form/general-view-container/general-view-container.component';
import { AddressViewContainerComponent } from './form/address-view-container/address-view-container.component';
import { PaymentViewContainerComponent } from './form/payment-view-container/payment-view-container.component';
import { HascustomerguardService } from './hascustomerguard.service';
import { CustomerSearchbarComponent } from 'app/shared/customer-searchbar/customer-searchbar.component';
@NgModule({
  imports:[
    CommonModule,
    SharedModule,
    CustomerRouting
  ],
  exports: [
    GeneralFormComponent,
    StepCustomerComponent,
    AddressFormComponent,
    PaymentFormComponent,
    CustomerListComponent,
    CustomerDetailComponent,
    CustomerSearchbarComponent
  ],
  declarations: [
    GeneralFormComponent,
    StepCustomerComponent,
    AddressFormComponent,
    GeneralContainerComponent,
    CustomerAddComponent,
    AddressContainerComponent,
    PaymentFormComponent,
    PaymentContainerComponent,
    CustomerListComponent,
    CustomerDetailComponent,
    CustomerListPageComponent,
    UploadfileComponent,
    UploadlistComponent,
    CustomerEditComponent,
    GeneralEditContainerComponent,
    AddressEditContainerComponent,
    PaymentEditContainerComponent,
    CustomerGetPageComponent,
    CustomerDetailContainerComponent,
    OrdercusTableComponent,
    GeneralViewContainerComponent,
    AddressViewContainerComponent,
    PaymentViewContainerComponent,
    CustomerSearchbarComponent
  ],
  providers: [
    CustomerFormStorageService,
    CustomerSenderService,
    CustomerPhoneService,
    CustomerShipaddressService,
    CustomerGetterService,
    BaseContainerService,
    HascustomerguardService,
  ],

})
export class CustomerModule { }
