import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FileItem, FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { UploaderService, Itemupload } from 'app/shared/uploader.service';
@Component({
  selector: 'app-uploadfile',
  templateUrl: './uploadfile.component.html',
  styleUrls: ['./uploadfile.component.scss']
})
export class UploadfileComponent implements OnInit {
  uploader: FileUploader;
  @Output() onUpload: EventEmitter<Itemupload> = new EventEmitter<Itemupload>();
  constructor(protected uploaderservice: UploaderService) {
    this.uploader = this.uploaderservice.getInstance();
  }

  ngOnInit() {
    this.uploaderservice.upload$.subscribe((res: Itemupload) => {
      console.log("upload from uplcomp");
      this.onUpload.emit(res);
    });
  }

  upload(item: FileItem){
      this.uploaderservice.upload(item);
  }
}
