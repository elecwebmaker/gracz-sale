import { TestBed, inject } from '@angular/core/testing';

import { HascustomerguardService } from './hascustomerguard.service';

describe('HascustomerguardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HascustomerguardService]
    });
  });

  it('should ...', inject([HascustomerguardService], (service: HascustomerguardService) => {
    expect(service).toBeTruthy();
  }));
});
