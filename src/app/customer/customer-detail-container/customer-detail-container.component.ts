import { Component, OnInit } from '@angular/core';
import { CustomerGetterService } from "app/customer/service/customer-getter.service";
import { Icustomer } from "app/customer/customer-detail/customer-detail.component";
import { OrderFormService } from "app/order/service/order-form.service";
import { Router,ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-customer-detail-container',
  templateUrl: './customer-detail-container.component.html',
  styleUrls: ['./customer-detail-container.component.scss']
})
export class CustomerDetailContainerComponent implements OnInit {
  customerData:Icustomer;
  cus_id: string;
  constructor(private customerGetter: CustomerGetterService, private orderformservice: OrderFormService,private router:Router, private activatedRoute: ActivatedRoute) { }
  
  ngOnInit() {
    this.customerGetter.get().subscribe( (res) => {
      const { general, address, payment } = res;

      this.cus_id = general.cus_id;
      this.customerData = {
        bill_addr: address.bill_addr_text || '-',
        ship_addrs: address.ship_addr_text || '-',
        bill_condition: payment.cond_bill || '-',
        businessType: general.type_business_text || '-',
        cheque_condition: payment.cond_cheque || '-',
        code:general.code_cus || '-',
        credit_expected:payment.expected_income || '-',
        credit_loan: payment.loan_amount || '-',
        files: payment.file,
        name: general.name_cus || '-',
        payment_method:payment.payment_method_text || '-',
        registered_date:general.registered_date || '-',
        remark:payment.remark || '-',
        sale_code:general.text_sale || '-',
        sale_discount: payment.discount_text || '-',
        taxpayer: general.ident_tax || '-',
        customer_zone: general.customer_zone || '-',
        tel:  general.tels.map((phone)=>{
            const {code,number} = phone;
            return `+${code}${number}`;
        }).join(',')
      };
    });
  }


  order(){
    this.orderformservice.setCustomer(this.cus_id, this.customerData.name, this.customerData.code);
    this.orderformservice.clearOnlyHeadForm();
    this.router.navigate(['/order/add']);
  }

  back(){
    this.router.navigate(['/customer/list']);
  }
}
