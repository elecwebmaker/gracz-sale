import { Component, OnInit } from '@angular/core';
import { CustomerGetterService } from "app/customer/service/customer-getter.service";
import { ActivatedRoute } from "@angular/router";
import { OrderGetterService } from "app/order/order-getter.service";
import { TopbarService } from "app/shared/topbar.service";

@Component({
    selector: 'app-customer-get-page',
    templateUrl: './customer-get-page.component.html',
    styleUrls: ['./customer-get-page.component.scss']
})
export class CustomerGetPageComponent implements OnInit {

    constructor(private ordergetter: OrderGetterService,
        private customergetter: CustomerGetterService, private activedRoute: ActivatedRoute
        , private topbarservice: TopbarService) { }

    ngOnInit() {
        this.customergetter.setCusId(this.activedRoute.snapshot.params['cus_id']);
        this.ordergetter.setCusId(this.activedRoute.snapshot.params['cus_id']);
        this.customergetter.get().subscribe((item) => {
            this.topbarservice.setState({
                title: 'ลูกค้า #' + item.general.code_cus,
                hasButton: false
            });
        });
    }

}
