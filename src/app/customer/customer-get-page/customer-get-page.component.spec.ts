import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerGetPageComponent } from './customer-get-page.component';

describe('CustomerGetPageComponent', () => {
  let component: CustomerGetPageComponent;
  let fixture: ComponentFixture<CustomerGetPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerGetPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerGetPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
