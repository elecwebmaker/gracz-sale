import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Itemupload } from "app/shared/uploader.service";

@Component({
  selector: 'app-uploadlist',
  templateUrl: './uploadlist.component.html',
  styleUrls: ['./uploadlist.component.scss']
})
export class UploadlistComponent implements OnInit {
  @Input() items: Array<Itemupload> = [];
  @Output() onDeleteFile = new EventEmitter<Itemupload>();
  constructor() { }

  ngOnInit() {
  }
  open(url){
    window.open(url);
  }

  delete(event,file:Itemupload,index){
    event.stopPropagation();
    this.items.splice(index,1);
    this.onDeleteFile.next(file);
  }

  getFileExt(filename):string{
    return filename.split('.').pop();
  }

  isImage(filename){
    const file_ext = this.getFileExt(filename).toLocaleLowerCase();
    switch(file_ext){
      case "jpg" :
        return true;
      case "png" :
        return true;
      case "gif" :
        return true;
      case "jpeg" :
        return true;
      default : 
        return false;
    }
  }
}
