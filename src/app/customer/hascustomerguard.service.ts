import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { CustomerGetterService } from "app/customer/service/customer-getter.service";

@Injectable()
export class HascustomerguardService implements CanActivate{

  constructor(private customergetter: CustomerGetterService) { 

  }

  canActivate(route: ActivatedRouteSnapshot){
     return this.customergetter.isThereCustomer(route.params['cus_id']);
  }
}
