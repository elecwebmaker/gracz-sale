import { Component, OnInit, OnDestroy } from '@angular/core';
import { ICusItemModel, CustomerItemModel } from 'app/customer/model/customer-item.model';
import { CustomerGetterService } from "app/customer/service/customer-getter.service";
import { Router,ActivatedRoute } from "@angular/router";
import { Subject, Subscription } from 'rxjs';

@Component({
    selector: 'app-customer-list',
    templateUrl: './customer-list.component.html',
    styleUrls: ['./customer-list.component.scss']
})
export class CustomerListComponent implements OnInit, OnDestroy {
    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
    public items: Array<ICusItemModel>;
    loading = false;
    totaldata: number;
    resetPage: Subject<any>;
    subscription: Subscription;
    onNavChange({size,offset}){
        this.customerGetterService.setFilter({
            size,
            offset
        });
    }

    constructor(private customerGetterService: CustomerGetterService, private router:Router, private activatedRouter: ActivatedRoute) { }

    ngOnInit() {
        
        this.customerGetterService.searchObservable.subscribe(()=>{
            this.loading = true;
        });
        this.subscription = this.customerGetterService.list().subscribe((res:any)=>{
            this.loading = false;
            this.items = [];
            this.totaldata = res.total;
            res.model.map((item)=>{
                this.items.push(new CustomerItemModel({
                    id:item.id,
                    name:item.name,
                    status:item.status,
                    phone_number: item.phone_number.map((phone)=>{
                        const {code,number} = phone;
                        return `+${code}${number}`;
                    }).join(','),
                    time: item.time,
                    status_text:item.status_text,
                    is_edit:item.is_edit
                }));
            });
        });
        this.resetPage = this.customerGetterService.resetPage;
    }
    onItemClick(item: CustomerItemModel){
        if(item.is_edit){
            this.router.navigate([`../../edit/${item.id}`],{relativeTo:this.activatedRouter});
        }else{
            this.router.navigate([`../../get/${item.id}`],{relativeTo:this.activatedRouter});
        }
    }
}
