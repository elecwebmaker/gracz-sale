import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomerAddComponent } from 'app/customer/customer-add/customer-add.component';
import { GeneralContainerComponent } from 'app/customer/form/general-container/general-container.component';
import { AddressContainerComponent } from 'app/customer/form/address-container/address-container.component';
import { PaymentContainerComponent } from 'app/customer/form/payment-container/payment-container.component';
import { OutletComponent } from 'app/shared/outlet/outlet.component';
import { CustomerListComponent } from 'app/customer/customer-list/customer-list.component';
import { CustomerListPageComponent } from 'app/customer-list-page/customer-list-page.component';
import { CustomerEditComponent } from 'app/customer/customer-edit/customer-edit.component';
import { GeneralEditContainerComponent } from 'app/customer/form/general-edit-container/general-edit-container.component';
import { AddressEditContainerComponent } from 'app/customer/form/address-edit-container/address-edit-container.component';
import { PaymentEditContainerComponent } from 'app/customer/form/payment-edit-container/payment-edit-container.component';
import { OrderListPageComponent } from 'app/order/order-list-page/order-list-page.component';
import { OrderListComponent } from 'app/order/order-list/order-list.component';
import { CustomerGetPageComponent } from "app/customer/customer-get-page/customer-get-page.component";
import { CustomerDetailComponent } from "app/customer/customer-detail/customer-detail.component";
import { CustomerDetailContainerComponent } from "app/customer/customer-detail-container/customer-detail-container.component";
import { GeneralViewContainerComponent } from "app/customer/form/general-view-container/general-view-container.component";
import { AddressViewContainerComponent } from "app/customer/form/address-view-container/address-view-container.component";
import { PaymentViewContainerComponent } from "app/customer/form/payment-view-container/payment-view-container.component";
import { HascustomerguardService } from "app/customer/hascustomerguard.service";
import { CfAuthenJWTGuard } from "app/cfusersecure/cf-authen-guard/cf-authen-jwt-guard";
import { AuthorizeCustomerGuard } from "app/core/guards/authorize-customer.guard";


export const customerRoutes:Routes = [

    {
        path: 'customer',
        component: OutletComponent,
        canActivate:[CfAuthenJWTGuard, AuthorizeCustomerGuard],
        children: [
            {
                path: 'add',
                component: CustomerAddComponent,
                children: [
                     {
                        path: '',
                        redirectTo: 'general',
                        pathMatch: 'full'
                    },
                    {
                        path: 'general',
                        component: GeneralContainerComponent,
                    },
                     {
                        path: 'address',
                        component: AddressContainerComponent,
                    },
                    {
                        path: 'payment',
                        component: PaymentContainerComponent,
                    }
                ]
            },
            {
                path: 'edit/:cus_id',
                component: CustomerEditComponent,
                canActivate:[HascustomerguardService],
                children: [
                    {
                        path:'',
                        redirectTo:'general',
                        pathMatch: 'full'
                    },
                    {
                        path: 'general',
                        component: GeneralEditContainerComponent,
                    },
                     {
                        path: 'address',
                        component: AddressEditContainerComponent,
                    },
                    {
                        path: 'payment',
                        component: PaymentEditContainerComponent,
                    }
                ]
            },
            {
                path: 'list',
                component: CustomerListPageComponent,
                children:[
                    {
                        path: '',
                        component: CustomerListComponent
                    }
                ]
            },
            {
                path: 'get/:cus_id',
                component: CustomerGetPageComponent,
                canActivate:[HascustomerguardService],
                children:[
                    {
                        path: '',
                        component: CustomerDetailContainerComponent
                    }
                ]
            },
            {
                path: '',
                redirectTo: '/customer/list',
                pathMatch: 'full'
            }, {
                path: '**',
                redirectTo: '/404'
            }
        ]
    },

];

@NgModule({
  imports: [
    RouterModule.forChild(customerRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers:[]
})


export class CustomerRouting { }