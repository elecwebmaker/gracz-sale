import { Component, OnInit } from '@angular/core';
import { CustomerFormStorageService } from 'app/customer/service/customer-form-storage.service';
import { ActivatedRoute } from "@angular/router";
import { TopbarService } from "app/shared/topbar.service";

@Component({
    selector: 'app-customer-add',
    templateUrl: './customer-add.component.html',
    styleUrls: ['./customer-add.component.scss']
})
export class CustomerAddComponent implements OnInit {

    constructor(
        private activedRoute: ActivatedRoute, 
        private topbarservice: TopbarService,
        private customerStorage: CustomerFormStorageService
    ) {

    }

    ngOnInit() {
        this.customerStorage.clear();
        this.topbarservice.setState({
            title: 'เพิ่มลูกค้า',
            hasButton: false
        });
    }


}
