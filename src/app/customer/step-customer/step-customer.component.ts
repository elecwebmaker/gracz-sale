import { Component, OnInit, Input } from '@angular/core';
import { StepComponent } from "app/shared/step/step.component";
import { StepContainerComponent } from "app/shared/step-container/step-container.component";

@Component({
  selector: 'app-step-customer',
  templateUrl: './step-customer.component.html',
  styleUrls: ['./step-customer.component.scss']
})
export class StepCustomerComponent implements OnInit {
    @Input() current: string;
    constructor() { }

    ngOnInit() {
    }

}
