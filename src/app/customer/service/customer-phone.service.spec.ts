import { TestBed, inject } from '@angular/core/testing';

import { CustomerPhoneService } from './customer-phone.service';

describe('CustomerPhoneService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CustomerPhoneService]
    });
  });

  it('should ...', inject([CustomerPhoneService], (service: CustomerPhoneService) => {
    expect(service).toBeTruthy();
  }));
});
