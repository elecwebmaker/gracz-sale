import { Injectable } from '@angular/core';
import { FormStorage } from './../model/form-storage';
import { CusDatatype } from 'app/customer/model/cus-datatype';
@Injectable()
export class CustomerFormStorageService extends FormStorage{
  
  constructor() {
    super();
   }
   set(type: CusDatatype, data: Object): void {
     this._set(type + '', data);
   }

   has(type: CusDatatype): boolean {
     return this._has(type + '');
   }

   get(type: CusDatatype): Object {
    return this._get(type + '');
   }
}
