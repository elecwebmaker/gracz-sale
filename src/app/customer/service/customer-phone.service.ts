import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Craftposter } from "craftposter";
import { UrlproviderService, DELETE_TEL } from "app/shared/urlprovider.service";

@Injectable()
export class CustomerPhoneService {
  craftposter:Craftposter;
  constructor(private http: Http, private urlprovider: UrlproviderService) {
    this.craftposter = new Craftposter(this.http);
    
  }
  remove(id: string){
    this.craftposter.setUrl(this.urlprovider.getUrl(DELETE_TEL)+ `/${id}`);
    return this.craftposter.send();
  }
}
