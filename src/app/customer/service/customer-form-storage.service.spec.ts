import { TestBed, inject } from '@angular/core/testing';

import { CustomerFormStorageService } from './customer-form-storage.service';

describe('CustomerFormStorageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CustomerFormStorageService]
    });
  });

  it('should ...', inject([CustomerFormStorageService], (service: CustomerFormStorageService) => {
    expect(service).toBeTruthy();
  }));
});
