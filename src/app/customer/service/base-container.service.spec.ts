import { TestBed, inject } from '@angular/core/testing';

import { BaseContainerService } from './base-container.service';

describe('BaseContainerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BaseContainerService]
    });
  });

  it('should ...', inject([BaseContainerService], (service: BaseContainerService) => {
    expect(service).toBeTruthy();
  }));
});
