import { TestBed, inject } from '@angular/core/testing';

import { CustomerShipaddressService } from './customer-shipaddress.service';

describe('CustomerShipaddressService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CustomerShipaddressService]
    });
  });

  it('should ...', inject([CustomerShipaddressService], (service: CustomerShipaddressService) => {
    expect(service).toBeTruthy();
  }));
});
