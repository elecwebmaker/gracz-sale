import { TestBed, inject } from '@angular/core/testing';

import { CustomerGetterService } from './customer-getter.service';

describe('CustomerGetterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CustomerGetterService]
    });
  });

  it('should ...', inject([CustomerGetterService], (service: CustomerGetterService) => {
    expect(service).toBeTruthy();
  }));
});
