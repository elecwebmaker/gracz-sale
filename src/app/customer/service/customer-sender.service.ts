
import { Injectable } from '@angular/core';
import { Craftposter } from 'craftposter';
import { Http } from '@angular/http';
import { GeneralModel, Igeneral } from './../model/general.model';
import { AddressModel, IAddress, IMainAddress } from './../model/address.model';
import { PaymentModel, Idatacon, Ifile } from './../model/payment.model';
import { UrlproviderService, ADD_CUSTOMER, EDIT_CUSTOMER, DEL_FILE } from "app/shared/urlprovider.service";
export function generalParse(general: Igeneral) {
   // tslint:disable-next-line:prefer-const
   let result: Igeneral = {...general};
   const tels = {tels:JSON.stringify(result.tels.map((phone) => {
      return {
        code: phone.code.replace('+', ''),
        number: phone.number,
        id: phone.id
      };
    }))
   };
    return {...result,...tels};
}

export function parseFile(files:Array<Ifile>){
  return files.map((file)=>{
    return {
      ...file,
      ...{
        ref: file.ref.split('/').slice(-3).join('/')
      }
    }
  });

}

export function addressParse(address: IMainAddress){
  const parse = (addr: IAddress) => {
    return {
      address: addr.address_text,
      province: addr.subaddress.province,
      district: addr.subaddress.district,
      subdistrict: addr.subaddress.sub_district,
      zipcode: addr.zipcode,
      id:addr.id,
      is_same_bill_address:addr.isBill
    }
  }
  // tslint:disable-next-line:prefer-const
  let result = <any>{...address};
  result.bill_address = JSON.stringify(parse(result.bill_address));
  result.ship_address = JSON.stringify(result.ship_address.map(parse));
  return result;
}

export function paymentParse(payment: Idatacon){
  
  // tslint:disable-next-line:prefer-const
  const paymentcopy = <Idatacon>{...payment};

  const discount_type = paymentcopy.discount.type;
  const discount_value = paymentcopy.discount.type == 1 ? paymentcopy.discount.value : null;
  const payment_method = paymentcopy.payment_method.type;
  const payment_instalment = paymentcopy.payment_method.type == 3 ? paymentcopy.payment_method.value : null;
  const files = JSON.stringify(parseFile(paymentcopy.file));
  const billing_condition = paymentcopy.cond_bill;
  const cheque_condition = paymentcopy.cond_cheque;
  const expect_credit_amount = paymentcopy.loan_amount;
  const remark = paymentcopy.remark;
  const expect_per_month = paymentcopy.expected_income;
  return {
    'discount_type': discount_type,
    'discount_value': discount_value,
    'payment_method': payment_method,
    'payment_instalment':payment_instalment,
    'billing_condition': billing_condition,
    'cheque_condition': cheque_condition,
    'expect_credit_amount': expect_credit_amount,
    'remark': remark,
    'expect_per_month': expect_per_month,
    files
  };
}


const mapBodyRename = {
      name_cus: 'name',
      name_contact: 'contact_name',
      tels: 'phone_number',
      fax: 'fax_number',
      email: 'email',
      type_business: 'business_type',
      ident_tax: 'id_card',
      code_sale: 'sale_id',
      bill_address: 'billing_address',
      ship_address: 'shipment_address',
      discount_type: 'discount_type',
      discount_value: 'discount_value',
      payment_method: 'payment_method',
      payment_instalment: 'payment_instalment',
      billing_condition: 'billing_condition',
      cheque_condition: 'cheque_condition',
      expect_credit_amount: 'expect_credit_amount',
      remark: 'remark',
      expect_per_month: 'expect_per_month'
    };

@Injectable()
export class CustomerSenderService {
  craftposter: Craftposter;
  id:string;
  constructor(private http: Http,private urlprovider: UrlproviderService) { 
    this.craftposter = new Craftposter(http);
    this.setup();
  }
  setup() {
    
  }
  setCusId(id:string){
    this.id = id;
  }
  parse(data: {
    general: GeneralModel,
    address: AddressModel,
    payment: PaymentModel
  }){
    const generaldata = generalParse(data.general.gen());
    const addressdata = addressParse(data.address.gen());
    const paymentdata = paymentParse(data.payment.gen());
    return {
      generaldata,
      addressdata,
      paymentdata
    }
  }

  edit(data: {
    general: GeneralModel,
    address: AddressModel,
    payment: PaymentModel
  }){
        this.craftposter.setUrl(this.urlprovider.getUrl(EDIT_CUSTOMER)+`/${this.id}`);
        const { generaldata,paymentdata,addressdata } = this.parse(data);
        this.craftposter.setBody({...generaldata, ...paymentdata, ...addressdata});
        this.craftposter.renameBodys(mapBodyRename);
        return this.craftposter.send();
  }

  add(data: {
    general: GeneralModel,
    address: AddressModel,
    payment: PaymentModel
  }) {
    this.craftposter.setUrl(this.urlprovider.getUrl(ADD_CUSTOMER));
    const { generaldata,paymentdata,addressdata } = this.parse(data);
    this.craftposter.setBody({...generaldata, ...paymentdata, ...addressdata});
    this.craftposter.renameBodys(mapBodyRename);
    return this.craftposter.send();
  }

  deleteFile(file_id){
    const url = this.urlprovider.getUrl(DEL_FILE) + `/${file_id}`; 
    return this.http.post(url,{});
  }
}
