import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { UrlproviderService, GET_CUSTOMER, LIST_LESS_CUSTOMER, HAS_CUSTOMER } from "app/shared/urlprovider.service";
import { CustomerFormStorageService } from "app/customer/service/customer-form-storage.service";
import { CusDatatype } from 'app/customer/model/cus-datatype';
import { GeneralModel } from "app/customer/model/general.model";
import { AddressModel,IAddress } from "app/customer/model/address.model";
import { PaymentModel } from "app/customer/model/payment.model";
import { BehaviorSubject, Observable, Subject } from "rxjs/Rx";

export interface ISearchList{
  key: string;
  status: string;
};

export interface CustomerListItem{
  id: number;
  name: string;
  phone_number: Array<{code:string,number: string}>;
  status:number;
  status_text:string;
  time: string;
  is_edit: boolean;
}

const parse_addr = (res) => {
    return {
        id: res.id,
        address_text: res.address,
        subaddress: {
            sub_district: res.subdistrict_text,
            district: res.district_text,
            province: res.province_text
        },
        zipcode: res.zipcode,
        isBill: res.is_same_bill_address
    }
};

const parse_addr_text = (res) => {
  return res.text;
};
@Injectable()
export class CustomerGetterService {
  id:string;
  filter;
  resetPage = new Subject();
  searchObservable: BehaviorSubject<ISearchList> = new BehaviorSubject<ISearchList>(undefined); 
  constructor(private http: Http,private urlprovider: UrlproviderService,private custh_storage: CustomerFormStorageService) {

  }
  setCusId(cus_id:string){
    this.id = cus_id;
  }

  get(){
    if(typeof this.id === 'undefined'){
      throw Error('Please provide customer id via setCusId method');
    }
    return this.http.post(this.urlprovider.getUrl(GET_CUSTOMER) + `/${this.id}`,{}).map((res:any)=>{return res.model }).map((res:any)=>{
      const general = {
        code_sale: res.sale_id,
        cus_id: this.id,
        customer_zone: res.customer_zone_text,
        text_sale: res.sale_code,
        email: res.email,
        fax: res.fax_number,
        ident_tax: res.id_card,
        name_contact: res.contact_name,
        name_cus: res.name,
        tels: res.phone_number,
        type_business: res.business_type,
        type_business_text: res.business_type_text,
        code_cus: res.customer_code,
        registered_date:res.register_date
      };
      const address = {
        bill_address:parse_addr(res.billing_address),
        bill_addr_text: res.billing_address.text,
        ship_address: res.shipment_address.map(parse_addr),
        ship_addr_text: res.shipment_address.map(parse_addr_text),
      };
      
      const payment = {
        cond_bill:res.billing_condition,
        cond_cheque:res.cheque_condition,
        discount:{
          type:res.discount_type,
          value:res.discount_value
        },
        expected_income:res.expect_per_month,
        file:res.files.map((res) => {
          return {
             id: res.id,
             ref: "http://" + res.ref,
             name: res.name,

          };
        }),
        loan_amount:res.expect_credit_amount,
        payment_method:{
          type:res.payment_method,
          value:res.payment_instalment  
        },
        remark: res.remark,
        payment_method_text:res.payment_text,
        discount_text:res.discount_text
      };

      return {
        general,
        payment,
        address
      }
    }).share();
  }

  list(): Observable<any> {
    this.filter = {};
    return this.searchObservable.switchMap((filter)=>{
      return this.http.post(this.urlprovider.getUrl(LIST_LESS_CUSTOMER),this.filter).map((res:any)=>{
        let result;
        if(!res.model){
           return [];
        }
         result = res.model.map((item)=>{
            return {
                id:item.id,
                name:item.name,
                phone_number:item.phone_number ? item.phone_number : [],
                status:item.status,
                time: item.timestamp,
                status_text:item.status_text,
                is_edit:item.is_edit
              }
        });
        return {
          model:result,
          total:res.total 
        }
      });
    });
  }

  setFilter(filter:any){
    this.filter = {
      ...this.filter,
      ...filter
    }
    this.searchObservable.next(filter);

  }

  isThereCustomer(cid: string){
    return this.http.post(this.urlprovider.getUrl(HAS_CUSTOMER) + "/" + cid,{}).map((res:any)=>{
      return res.status;
    });
  }
}
