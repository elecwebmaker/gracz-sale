import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Craftposter } from "craftposter";
import { UrlproviderService, DELETE_SHIPADDR } from "app/shared/urlprovider.service";
@Injectable()
export class CustomerShipaddressService {
  craftposter:Craftposter;
  constructor(private http:Http, private urlprovider: UrlproviderService) {
    this.craftposter = new Craftposter(this.http);
    
  }
  remove(id: number){
    this.craftposter.setUrl(this.urlprovider.getUrl(DELETE_SHIPADDR) + `/${id}`);
    this.craftposter.setBody({
      id
    });
    return this.craftposter.send();
  }
}
