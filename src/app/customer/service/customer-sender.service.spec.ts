import { TestBed, inject } from '@angular/core/testing';

import { CustomerSenderService } from './customer-sender.service';

describe('CustomerSenderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CustomerSenderService]
    });
  });

  it('should ...', inject([CustomerSenderService], (service: CustomerSenderService) => {
    expect(service).toBeTruthy();
  }));
});
