import { Injectable } from '@angular/core';
import { NotifyService } from 'app/shared/notify.service';
import { CustomerSenderService } from 'app/customer/service/customer-sender.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerFormStorageService } from 'app/customer/service/customer-form-storage.service';

@Injectable()
export class BaseContainerService {

  constructor(
    public cusStorage: CustomerFormStorageService,
    public customersender: CustomerSenderService,
    public notifyhandler: NotifyService
  ) { }

}
