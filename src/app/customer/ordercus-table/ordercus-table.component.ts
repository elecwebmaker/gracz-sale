import { Component, OnInit, Input } from '@angular/core';
import { CFTableControl, CFColumn, CFRow } from 'craft-table';
import { OrderGetterService } from 'app/order/order-getter.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ordercus-table',
  templateUrl: './ordercus-table.component.html',
  styleUrls: ['./ordercus-table.component.scss']
})
export class OrdercusTableComponent implements OnInit {
  @Input('cusid') cus_id: string;

  currentPage:number = 1;
  currentSize:number = 10;
  observableList;
  dataTotal: number = 0;
  control: CFTableControl = new CFTableControl({
    header:[
      {
        id: 'preorder_id',
        label: 'รหัสการสั่งซื้อ'
      },
      {
        id: 'list_quantity_text',
        label: 'จำนวน'
      },
      {
        id: 'total_price_text',
        label: 'ราคา'
      },
      {
        id: 'status_text',
        label: 'สถานะ'
      }
    ],
    navigating: true
  })
  constructor(private ordergetter: OrderGetterService, private router: Router) { 
    
  }

  ngOnInit() {
    this.observableList = this.ordergetter.list(true).map((res:any)=>{
      this.control.setDataTotal(res.total);
      this.dataTotal = res.total;
      return res.model;
  });
    this.control.setGetData((offset, size) => {
      this.ordergetter.setFilter({
          offset,
          size
      })
      return this.observableList;
  });

  }
  clickColumn(data:{
        column: CFColumn,
        row: CFRow
    }){
    const oid = data.row.get('id').getValue();

    if(data.row.get('is_edit').getValue()){
      this.router.navigate(['/order/edit/'+ oid]);
    }else{
      this.router.navigate(['/order/view/'+ oid]);
    }
  }
}
