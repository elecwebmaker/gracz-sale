import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdercusTableComponent } from './ordercus-table.component';

describe('OrdercusTableComponent', () => {
  let component: OrdercusTableComponent;
  let fixture: ComponentFixture<OrdercusTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdercusTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdercusTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
