import { Component, OnInit, Input, EventEmitter, Output, OnChanges } from '@angular/core';
export interface Icustomer{
    name: string;
    businessType: string;
    code: string;
    tel: string;
    taxpayer: string;
    sale_code: string;
    bill_addr: string;
    ship_addrs: Array<string>;
    sale_discount: string;
    payment_method: string;
    bill_condition: string;
    cheque_condition: string;
    credit_expected: string;
    credit_loan: string;
    remark: string;
    registered_date: string;
    customer_zone: string;
    files: Array<{
      name: string;
      ref: string;
    }>;
  };
@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.scss']
})
export class CustomerDetailComponent implements OnChanges {
  @Input() customer:Icustomer;
  @Output() onOrder: EventEmitter<undefined> = new EventEmitter<undefined>();
  constructor() { }

  ngOnChanges() {
      console.log('mumu', this.customer)
  }

  open(url: string){
    window.open(url);
  }

  order_product(){
    this.onOrder.emit();
  }

}
