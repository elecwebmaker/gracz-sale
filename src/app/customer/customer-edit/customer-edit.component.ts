import { Component, OnInit } from '@angular/core';
import { CustomerGetterService } from "app/customer/service/customer-getter.service";
import { ActivatedRoute } from "@angular/router";
import { CustomerSenderService } from "app/customer/service/customer-sender.service";
import { GeneralModel } from "app/customer/model/general.model";

import { CustomerFormStorageService } from 'app/customer/service/customer-form-storage.service';
import { CusDatatype } from 'app/customer/model/cus-datatype';
import { AddressModel } from "app/customer/model/address.model";
import { PaymentModel } from "app/customer/model/payment.model";
import { TopbarService } from "app/shared/topbar.service";



@Component({
    selector: 'app-customer-edit',
    templateUrl: './customer-edit.component.html',
    styleUrls: ['./customer-edit.component.scss']
})
export class CustomerEditComponent implements OnInit {

    constructor(private customer_getter: CustomerGetterService, private activedRoute: ActivatedRoute,
    private customer_sender: CustomerSenderService, private customerStorage: CustomerFormStorageService,
    private topbarservice: TopbarService) { }

    ngOnInit() {
        this.customer_getter.setCusId(this.activedRoute.snapshot.params['cus_id']);
        this.customer_sender.setCusId(this.activedRoute.snapshot.params['cus_id']);
        this.customerStorage.clear();
        this.customer_getter.get().subscribe((res) => {
            this.topbarservice.setState({
                title: 'แก้ไขข้อมูลลูกค้า #' + res.general.code_cus,
                hasButton: false 
            });
            const { general, address, payment } = res;
            this.customerStorage.set(CusDatatype.general, (new GeneralModel(general)).gen());
            this.customerStorage.set(CusDatatype.address, (new AddressModel(address)).gen());
            this.customerStorage.set(CusDatatype.payment, (new PaymentModel(payment)).gen());
            this.customerStorage.complete();
        })
    }

}
