import { Component, OnInit } from '@angular/core';
import { BaseItemComponent } from "app/shared/base-item/base-item.component";

@Component({
    selector: 'app-customer-item',
    templateUrl: './customer-item.component.html',
    styleUrls: ['./customer-item.component.scss']
})
export class CustomerItemComponent extends BaseItemComponent {
    ngClass;
    ngOnInit() {
        if (this.model) {
            this.ngClass = {
                approve: (this.model.status == 1),
                reject: (this.model.status == 2),
                pending: (this.model.status == 3),
                edit: (this.model.status == 4)
            };
        }
    }
}

