import { CfUserHandlerService } from "app/cfusersecure/cf-user-handler/cf-user-handler.service";
import { CfUserSecureNormal } from "app/cfusersecure/cf-user-secure/cf-user-secure-normal";
import { Injectable } from '@angular/core';

@Injectable()
export class CfUserHandlerNormalService extends CfUserHandlerService {
    cfUserSercure = new CfUserSecureNormal();

    login(accesstoken: string, userdata: Object,authorize?: Array<any>){
        this._login(accesstoken,authorize);
        this.cfUserSercure.saveUser(userdata);
    }

    
}