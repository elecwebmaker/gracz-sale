export class CfUserAccesstoken{
    accesstoken: string;

    constructor(accesstoken?: string){
        this.accesstoken = accesstoken;
    }

    setAccesstoken(accesstoken: string){
        this.accesstoken = accesstoken;
    }
    getAccesstoken(): string {
        return this.accesstoken;
    }

    
}