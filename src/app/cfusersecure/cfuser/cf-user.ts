import { CfUserAccesstoken } from "app/cfusersecure/cfuser/cf-user-accesstoken";
import { CfAuthorize } from "app/cfusersecure/cf-authorize/cf-authorize";

export class CfUser{
    props: Object = {};
    accesstoken: CfUserAccesstoken = new CfUserAccesstoken();
    authorize: CfAuthorize = new CfAuthorize();
    constructor(props?:Object,accesstoken?: string,authorize?:Array<any>){
        if(props){
            this.props = props;
        }
        if(accesstoken){
            this.accesstoken.setAccesstoken(accesstoken);
        }
        this.authorize.setAuthorize(authorize);
    }

    set(key: string, value: any): void{
        if(!this.props){
            this.props = {};
        }
        this.props[key] = value;
    }
    get(key: string): any{
        if(this.props){
             return this.props[key];
        }
       
    }

    clearUserData(){
        this.props = {};
    }
}