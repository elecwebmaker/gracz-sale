import { CfUser } from "app/cfusersecure/cfuser/cf-user";
export const CFACCESSTOKEN = 'CFACCESSTOKEN';
export const CFUSER = 'CFUSER';
export const CFAUTHORIZES = 'CFAUTHORIZES';

export abstract class CfStorage{
    abstract save(data:any):void;
    abstract get(): CfUser;
    abstract isSave(): boolean;
    abstract clear(): void;
}