import { CfStorage, CFACCESSTOKEN, CFUSER, CFAUTHORIZES } from './cf-storage';
import { CfUser } from "app/cfusersecure/cfuser/cf-user";

export class CfLocalUser extends CfStorage{
    save(cfuser: CfUser): void{
        const user = JSON.stringify(cfuser.props);
        const accesstoken  = cfuser.accesstoken.getAccesstoken();
        
        window.localStorage.setItem(CFUSER,user);
        window.localStorage.setItem(CFACCESSTOKEN,accesstoken);

        if(cfuser.authorize.getAuthorize()){
            const authorizes = JSON.stringify(cfuser.authorize.getAuthorize());
            window.localStorage.setItem(CFAUTHORIZES, authorizes);
        }

    }
    get(): CfUser{
        const user = JSON.parse(window.localStorage.getItem(CFUSER));
        const accesstoken = window.localStorage.getItem(CFACCESSTOKEN);
        const authorizes = JSON.parse(window.localStorage.getItem(CFAUTHORIZES));
        
        return new CfUser(user, accesstoken, authorizes);
    };
    isSave(): boolean{
        return !!window.localStorage.getItem(CFACCESSTOKEN);
    };

    clear(){
        window.localStorage.removeItem(CFUSER);
        window.localStorage.removeItem(CFACCESSTOKEN);
        window.localStorage.removeItem(CFAUTHORIZES);
    }
}