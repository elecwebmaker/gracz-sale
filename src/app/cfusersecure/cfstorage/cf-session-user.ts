import { CfStorage, CFACCESSTOKEN, CFUSER, CFAUTHORIZES } from './cf-storage';
import { CfUser } from "app/cfusersecure/cfuser/cf-user";

export class CfSessionUser extends CfStorage{
    save(cfuser: CfUser): void{
        const user = JSON.stringify(cfuser.props);
        const accesstoken  = cfuser.accesstoken.getAccesstoken();
      
        window.sessionStorage.setItem(CFUSER,user);
        window.sessionStorage.setItem(CFACCESSTOKEN,accesstoken);
        
        if(cfuser.authorize.getAuthorize()){
            const authorizes = JSON.stringify(cfuser.authorize.getAuthorize());
             window.sessionStorage.setItem(CFAUTHORIZES, authorizes);
        }
        
    }
    get(): CfUser{
        const user = JSON.parse(window.sessionStorage.getItem(CFUSER));
        const accesstoken = window.sessionStorage.getItem(CFACCESSTOKEN);
        const authorizes = JSON.parse(window.sessionStorage.getItem(CFAUTHORIZES));
        return new CfUser(user, accesstoken,authorizes);
    };
    isSave(): boolean{
        return !!window.sessionStorage.getItem(CFACCESSTOKEN);
    };

    clear(){
        window.sessionStorage.removeItem(CFUSER);
        window.sessionStorage.removeItem(CFACCESSTOKEN);
        window.sessionStorage.removeItem(CFAUTHORIZES);
    }
}