import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, Router } from '@angular/router';
import { CfUserHandlerJWTService } from "app/cfusersecure/cf-user-handler/cf-user-handler-jwt.service";
import { CFAuthenJWTManageService } from "app/cfusersecure/cf-authen-manage/cf-authen-jwt-manage.service";

@Injectable()
export class CfAuthorizeJWTGuard {
    constructor(
        private cfuserhandler: CfUserHandlerJWTService,
        private cfauthenmanage: CFAuthenJWTManageService,
        private router: Router
    ) { }

    isAuthorize(permission, overideRoute?) {
        const isAuthorize = this.cfuserhandler.isAuthorize(permission);
        if (!isAuthorize) {
            if (overideRoute) {
                this.router.navigate([overideRoute]);
            } else {
                this.router.navigate([this.cfauthenmanage.routeWhenUnAuthorize]);
            }
        }
        return isAuthorize;
    }

}