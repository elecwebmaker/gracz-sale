import { NgModule } from '@angular/core';
import { CFAuthenJWTManageService } from "app/cfusersecure/cf-authen-manage/cf-authen-jwt-manage.service";
import { CfUserHandlerNormalService } from "app/cfusersecure/cf-user-handler/cf-user-handler-normal.service";
import { CfAuthenNormalGuard } from "app/cfusersecure/cf-authen-guard/cf-authen-normal-guard";
import { CfAuthenJWTGuard } from "app/cfusersecure/cf-authen-guard/cf-authen-jwt-guard";
import { CfUserHandlerJWTService } from "app/cfusersecure/cf-user-handler/cf-user-handler-jwt.service";
import { CfDisplaySecureDirective } from "app/cfusersecure/cf-display-secure.directive";

@NgModule({
    imports: [],
    exports: [CfDisplaySecureDirective],
    declarations: [CfDisplaySecureDirective],
    providers: [CFAuthenJWTManageService, CfUserHandlerNormalService, CfAuthenNormalGuard,CfAuthenJWTGuard, CfUserHandlerJWTService],
})
export class CfSecureModule { }
