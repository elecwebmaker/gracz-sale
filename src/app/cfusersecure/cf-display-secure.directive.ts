import { Directive, ElementRef, Input, Renderer2, ViewContainerRef, TemplateRef, OnChanges, EmbeddedViewRef } from '@angular/core';
import { CfUserHandlerJWTService } from "app/cfusersecure/cf-user-handler/cf-user-handler-jwt.service";

@Directive({
    selector: '[cfdisplaysecure]'
})
export class CfDisplaySecureDirective implements OnChanges{
    @Input() cfdisplaysecure: any;
    viewRef: EmbeddedViewRef<any>;
    constructor(
        private _tem: TemplateRef<any>,
        private _view: ViewContainerRef,
        private cfJwtGuard: CfUserHandlerJWTService
    ) { }

    ngOnChanges() {
        
        if (!this.cfJwtGuard.isAuthorize(this.cfdisplaysecure)) {
            this.clear();
        }else{
            console.log('test');
            this.create();
        }
    }

    private clear(){
        if(this.viewRef){
            this._view.clear();
        }
    }

    private create(){
        this.clear();
        this.viewRef = this._view.createEmbeddedView(this._tem);
    }
}
