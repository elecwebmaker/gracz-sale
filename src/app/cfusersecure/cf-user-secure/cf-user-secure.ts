import { CfSessionUser } from "app/cfusersecure/cfstorage/cf-session-user";
import { CfLocalUser } from "app/cfusersecure/cfstorage/cf-local-user";
import { CfUser } from "app/cfusersecure/cfuser/cf-user";

export abstract class CfUserSecure {
    protected session: CfSessionUser = new CfSessionUser();
    protected local: CfLocalUser = new CfLocalUser();
    public user: CfUser = new CfUser();
    protected isLoggedIn: boolean = false;
    protected _setUser(props: Object){
        for (const prop of Object.keys(props)){
            this.user.set(prop, props[prop]);
        }
    }
    abstract setUser(props: Object): void;
    abstract saveToLocalStorage(): void;
    abstract setUserFromLocal():void;
    abstract setUserFromSession():void;

    login(accesstoken: string,authorizes?:Array<any>){
        this.user.accesstoken.setAccesstoken(accesstoken);
        if(authorizes){
            this.user.authorize.setAuthorize(authorizes);
        }
        
        this.isLoggedIn = true;
    }
    

    isAuthen(): boolean{
        if(this.isLoggedIn){
            return true;
        }else{
            if(this.session.isSave()){
                this.setUserFromSession();
                return true;
            }
            if(this.local.isSave()){
                this.setUserFromLocal();
                return true;
            }
        }
        return false;
    }

    isAuthorize(authorize:any){
        return this.user.authorize.isAuthorized(authorize);
    }

    logout(): void{
        this.isLoggedIn = false;
        this.user = new CfUser();
        this.session.clear();
        this.local.clear();
    }
};