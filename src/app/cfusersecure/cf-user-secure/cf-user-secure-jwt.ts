import { CfUserSecure } from "./cf-user-secure";
import { JwtHelper  } from 'angular2-jwt';
import { CfUser } from "app/cfusersecure/cfuser/cf-user";
export class CfUserSecureJWT extends CfUserSecure {
    private JwtHelper = new JwtHelper();
    private copyCfuser(){
        return Object.assign(new CfUser(), this.user);
    }
    genUserData(){
        this.setUser(this.getDataFromToken());
    }

    private getDataFromToken(){
        let datafromtoken;
        try{
            datafromtoken = this.JwtHelper.decodeToken(this.user.accesstoken.getAccesstoken());
        }catch (e){
            console.log('token is invalid');
            datafromtoken = {};
            throw new (function(){
                this.name = 'TOKEN_IS_INVALID';
                this.message = 'Token is invalid';
            })();
        }


        if(this.JwtHelper.isTokenExpired(this.user.accesstoken.getAccesstoken())){
            datafromtoken = {};
             console.log('It"s expired, man');
            throw new (function(){
                this.name = 'TOKEN_EXPIRED';
                this.message = 'Token is expired';
            })();

        }else{

        }
        return datafromtoken;

    }

    setUser(props: Object){
        this._setUser(props);
        const cfuser = this.copyCfuser();
        cfuser.clearUserData();
        this.session.save(cfuser);
    }

    saveToLocalStorage(){
         const cfuser = this.copyCfuser();
        cfuser.clearUserData();
        this.local.save(cfuser);
    }

    setUserFromLocal(){
        const cfuser = this.local.get();
        this.user = cfuser;
        this.setUser(this.getDataFromToken())
    }

    setUserFromSession(){
        const cfuser = this.session.get();
        this.user = cfuser;
        this.setUser(this.getDataFromToken())
    }
}