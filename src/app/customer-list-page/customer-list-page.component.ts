import { Component, OnInit } from '@angular/core';
import { TopbarService } from 'app/shared/topbar.service';

@Component({
  selector: 'app-customer-list-page',
  templateUrl: './customer-list-page.component.html',
  styleUrls: ['./customer-list-page.component.scss']
})
export class CustomerListPageComponent implements OnInit {

  constructor(private topbarservice:TopbarService) { }

  ngOnInit() {
    this.topbarservice.setState({
      title: 'รายชื่อลูกค้า',
      hasButton: true,
      route: '/customer/add'
    });
  }

}
