import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { NgSemanticModule } from 'ng-semantic/ng-semantic';
import { InvalidmessageDirective } from './directive/invalidmessage.directive';
import { MultiPhoneComponent } from './multi-phone/multi-phone.component';
import { PrefixCountryCodeDirective } from './directives/prefix-country-code.directive';
import { StepComponent } from './step/step.component';
import { StepContainerComponent } from './step-container/step-container.component';
import { MultiAddressComponent } from './multi-address/multi-address.component';
import { SingleAddressComponent } from './single-address/single-address.component';
import { InvalidTypeDirective } from './directive/invalid-type.directive';
import { TopbarComponent } from './topbar/topbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { WrapperFormComponent } from './wrapper-form/wrapper-form.component';
import { HttpModule, Http, XHRBackend, RequestOptions } from '@angular/http';
import { CustomHttp,CraftutilityModule } from 'craftutility';
import { WrapperListComponent } from './wrapper-list/wrapper-list.component';
import { BaseItemComponent } from './base-item/base-item.component';
import { CustomerItemComponent } from "app/customer/customer-item/customer-item.component";
import { NotifyService } from './notify.service';
import { MainappComponent } from './mainapp/mainapp.component';
import { MenuComponent } from './menu/menu.component';
import { OutletComponent } from './outlet/outlet.component';
import { ListitemdataService } from './listitemdata.service';
import { UploaderService } from './uploader.service';
import { FileSelectDirective, FileDropDirective } from 'ng2-file-upload/ng2-file-upload';
import { UrlproviderService } from './urlprovider.service';
import { DatepickerModule } from 'angular2-material-datepicker'; 
import { CFTableModule,CFComponentModule } from 'craft-table';
import { SearchComponent } from './search/search.component';
import { StatusComponent } from './status/status.component';
import { SubTopbarComponent } from './sub-topbar/sub-topbar.component';
import { CfSelectComponent } from './cf-select/cf-select.component';
import { CfChoiceComponent } from './cf-choice/cf-choice.component';
import { EditTableButtonComponent } from './edit-table-button/edit-table-button.component';
import { DeleteTableButtonComponent } from './delete-table-button/delete-table-button.component';
import { ViewTableButtonComponent } from './view-table-button/view-table-button.component';
import { BaseSearchbarComponent } from './base-searchbar/base-searchbar.component';
import { MdSidenavModule } from '@angular/material';
import { MyDatePickerModule } from 'mydatepicker';
import { TopbarService } from "app/shared/topbar.service";
import { CfSecureModule } from "app/cfusersecure/cf-secure.module";
import { AccestokenProviderService } from "app/core/accestoken-provider.service";
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CfUploadImageService } from "app/shared/cf-upload/cf-upload-image.service";
import { CfUploadImageComponent } from "app/shared/cf-upload/cf-upload-image/cf-upload-image.component";
import { CfPreviewImageComponent } from "app/shared/cf-upload/cf-preview-image/cf-preview-image.component";
import { CoreModule } from "app/core/core.module";
import { CoreRouting } from "app/core/core.routing";
import { NavigationEveComponent } from './navigation-eve/navigation-eve.component';
import { PaginationEveComponent } from './pagination-eve/pagination-eve.component';
import { SizinationEveComponent } from './sizination-eve/sizination-eve.component';
import { CraftHttpNotifyService } from 'craftutility';
import { NoLetterDirective } from './directives/no-letter.directive';
import { LetterAndCommaDirective } from './directives/letter-and-comma.directive';
import { OnlyNumberDirective } from './directives/only-number.directive';
import { SidebarService } from './sidebar.service';
import { OnlyNumberAndPlusDirective } from './directives/only-number-and-plus.directive';
import { Ng2CompleterModule } from "ng2-completer";
const API_URL = '';
export function CustomhttpProvider(backend, defaultOptions:RequestOptions,accestokenProvider:AccestokenProviderService,crafthttpnotify:CraftHttpNotifyService){
  return new CustomHttp(backend, defaultOptions, API_URL,crafthttpnotify,accestokenProvider, 'token');
}

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgSemanticModule,
    DatepickerModule,
    CFComponentModule,
    CraftutilityModule,
    MdSidenavModule,
    MyDatePickerModule,
    CfSecureModule,
    Ng2CompleterModule
  ],
  exports:[
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    NgSemanticModule,
    InvalidmessageDirective,
    MultiPhoneComponent,
    InvalidTypeDirective,
    StepComponent,
    StepContainerComponent,
    MultiAddressComponent,
    SingleAddressComponent,
    PrefixCountryCodeDirective,
    TopbarComponent,
    SidebarComponent,
    WrapperFormComponent,
    HttpModule,
    WrapperListComponent,
    MainappComponent,
    MenuComponent,
    OutletComponent,
    FileSelectDirective,
    FileDropDirective,
    DatepickerModule,
    CFTableModule,
    SearchComponent,
    StatusComponent,
    SubTopbarComponent,
    CfSelectComponent,
    CfChoiceComponent,
    EditTableButtonComponent,
    DeleteTableButtonComponent,
    ViewTableButtonComponent,
    CraftutilityModule,
    MdSidenavModule,
    MyDatePickerModule,
    PageNotFoundComponent,
    CfUploadImageComponent,
    CfPreviewImageComponent,
    NavigationEveComponent,
    PaginationEveComponent,
    SizinationEveComponent,
    NoLetterDirective,
    LetterAndCommaDirective,
    OnlyNumberDirective,
    OnlyNumberAndPlusDirective,
    Ng2CompleterModule
  ],
  declarations: [
    MultiPhoneComponent,
    PrefixCountryCodeDirective,
    InvalidmessageDirective,
    InvalidTypeDirective,
    StepComponent,
    StepContainerComponent,
    MultiAddressComponent,
    SingleAddressComponent,
    TopbarComponent,
    SidebarComponent,
    WrapperFormComponent,
    WrapperListComponent,
    CustomerItemComponent,
    MainappComponent,
    MenuComponent,
    OutletComponent,
    FileSelectDirective,
    FileDropDirective,
    SearchComponent,
    StatusComponent,
    SubTopbarComponent,
    CfSelectComponent,
    CfChoiceComponent,
    EditTableButtonComponent,
    DeleteTableButtonComponent,
    ViewTableButtonComponent,
    BaseSearchbarComponent,
    PageNotFoundComponent,
    CfUploadImageComponent,
    CfPreviewImageComponent,
    NavigationEveComponent,
    PaginationEveComponent,
    SizinationEveComponent,
    NoLetterDirective,
    LetterAndCommaDirective,
    OnlyNumberDirective,
    OnlyNumberAndPlusDirective
  ],
  providers:[
    NotifyService,
    ListitemdataService,
    UploaderService,
    UrlproviderService,
    TopbarService,
    AccestokenProviderService,
    CfUploadImageService,
    SidebarService,
    { provide: Http,
        useFactory: CustomhttpProvider,
        deps: [XHRBackend, RequestOptions,AccestokenProviderService,CraftHttpNotifyService],
      },
  ]
})
export class SharedModule { }