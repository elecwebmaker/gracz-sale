import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationEveComponent } from './navigation-eve.component';

describe('NavigationEveComponent', () => {
  let component: NavigationEveComponent;
  let fixture: ComponentFixture<NavigationEveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigationEveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationEveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
