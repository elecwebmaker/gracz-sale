import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'edit-table-btn',
  templateUrl: './edit-table-button.component.html',
  styleUrls: ['./edit-table-button.component.scss']
})
export class EditTableButtonComponent implements OnInit {
  @Output('onClick') onClick: EventEmitter<undefined> = new EventEmitter<undefined>();
  constructor() { }

  ngOnInit() {
  }

}
