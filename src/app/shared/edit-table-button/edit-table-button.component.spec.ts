import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTableButtonComponent } from './edit-table-button.component';

describe('EditTableButtonComponent', () => {
  let component: EditTableButtonComponent;
  let fixture: ComponentFixture<EditTableButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditTableButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTableButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
