import { Component, OnInit } from '@angular/core';
import { CustomerGetterService } from 'app/customer/service/customer-getter.service';
import { ListitemdataService } from 'app/shared/listitemdata.service';
import { FilterBar,IFilterKey } from 'app/shared/filterbar/filterbar';

@Component({
    selector: 'app-customer-searchbar',
    templateUrl: './customer-searchbar.component.html',
    styleUrls: ['./customer-searchbar.component.scss']
})
export class CustomerSearchbarComponent implements OnInit {
    private filter = new FilterBar<IFilterKey>(["status","key"]);
    constructor(private customergetter: CustomerGetterService, private listdataservice: ListitemdataService) { }

    ngOnInit() {
        this.filter.setStatusList(this.listdataservice.getCustomerStatus());
        this.filter.filterChanges().subscribe((res) => {
            this.customergetter.setFilter(res);
            this.customergetter.resetPage.next();
        });
    }

    searchChange(val) {
        this.filter.setFilter('key', val);
    }

    statusChange(val) {
        this.filter.setFilter('status', val);
    }
}
