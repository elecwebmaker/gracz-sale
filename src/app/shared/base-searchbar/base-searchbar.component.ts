import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-base-searchbar',
  templateUrl: './base-searchbar.component.html',
  styleUrls: ['./base-searchbar.component.scss']
})
export class BaseSearchbarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
