import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseSearchbarComponent } from './base-searchbar.component';

describe('BaseSearchbarComponent', () => {
  let component: BaseSearchbarComponent;
  let fixture: ComponentFixture<BaseSearchbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseSearchbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseSearchbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
