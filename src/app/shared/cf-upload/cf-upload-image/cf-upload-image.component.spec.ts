import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CfUploadImageComponent } from './cf-upload-image.component';

describe('CfUploadImageComponent', () => {
  let component: CfUploadImageComponent;
  let fixture: ComponentFixture<CfUploadImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CfUploadImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CfUploadImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
