import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CfPreviewImageComponent } from './cf-preview-image.component';

describe('CfPreviewImageComponent', () => {
  let component: CfPreviewImageComponent;
  let fixture: ComponentFixture<CfPreviewImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CfPreviewImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CfPreviewImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
