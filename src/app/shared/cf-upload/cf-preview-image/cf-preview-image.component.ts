import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'cf-preview-image',
  templateUrl: './cf-preview-image.component.html',
  styleUrls: ['./cf-preview-image.component.scss']
})
export class CfPreviewImageComponent implements OnChanges {
  @Input() src: string;
  @Input() hasUploaded: any;
  @Input() initialImage: string;
  background_src: any;
  constructor() { }

  ngOnChanges() {
    if(this.hasUploaded){
      this.background_src = {
        ["background-image"]:`url(${this.src})`
      };
    }else{
      if(this.initialImage){
        this.background_src = {
          ["background-image"]:`url(${this.initialImage})`
        }
      }else{
        this.background_src = undefined;
      }
      
    }

  }
  
}
