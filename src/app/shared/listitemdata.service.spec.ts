import { TestBed, inject } from '@angular/core/testing';

import { ListitemdataService } from './listitemdata.service';

describe('ListitemdataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListitemdataService]
    });
  });

  it('should ...', inject([ListitemdataService], (service: ListitemdataService) => {
    expect(service).toBeTruthy();
  }));
});
