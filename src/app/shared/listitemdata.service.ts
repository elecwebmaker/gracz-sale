import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Craftposter } from "craftposter";
import {
  UrlproviderService,
  GET_SALES,
  GET_BUSINESS_TYPE,
  GET_PROVINCE,
  GET_DISTRICT,
  GET_SUBDISTRICT,
  LIST_STATUS,
  LIST_SHIPMENT,
  LIST_CATEGORY,
  LIST_MODEL,
  LIST_UNIT,
  GET_CUSTOMER_STATUS,
  GET_ORDER_STATUS,
  GET_CUSTOMER_ZONE
} from "app/shared/urlprovider.service";

export interface ListDataModel {
  id: string;
  text: string;
}
@Injectable()
export class ListitemdataService {
  constructor(private http: Http, private urlprovider: UrlproviderService) {}

  getSales() {
    const url = this.urlprovider.getUrl(GET_SALES);
    return this.http.post(url, {}).map((res: any) => {
      return res.model;
    });
  }

  getBussinessType() {
    const url = this.urlprovider.getUrl(GET_BUSINESS_TYPE);
    return this.http.post(url, {}).map((res: any) => {
      return res.model;
    });
  }

  getProvince(province) {
    const url = this.urlprovider.getUrl(GET_PROVINCE);
    return this.http.post(url, {province}).map((res: any) => {
      return res.model;
    });
  }

  getDistrict(province: string,district: string) {
    const url = this.urlprovider.getUrl(GET_DISTRICT);
    return this.http.post(url, {province,district}).map((res: any) => {
      return res.model;
    });
  }

  getSubDistrict(province: string,district: string,subdistrict: string) {
    const url = this.urlprovider.getUrl(GET_SUBDISTRICT);
    return this.http.post(url, {province,district,subdistrict}).map((res: any) => {
      return res.model;
    });
  }

  getStatus() {
    const url = this.urlprovider.getUrl(LIST_STATUS);
    return this.http.post(url, {}).map((res: any) => {
      return res.model;
    });
  }

  getShipmentList(cid: string) {
    const url = this.urlprovider.getUrl(LIST_SHIPMENT) + `/${cid}`;
    return this.http.post(url, {}).map((res: any) => {
      return res.model;
    });
  }

  getProductList() {
    const url = this.urlprovider.getUrl(LIST_CATEGORY);
    return this.http.post(url, {}).map((res: any) => {
      console.log(res);
      return res.model;
    });
  }

  getModelList(category_id: string) {
    const url = this.urlprovider.getUrl(LIST_MODEL) + `/${category_id}`;
    return this.http.post(url, {}).map((res: any) => {
      console.log(res.model);
      return res.model;
    });
  }

  getUnitList(model_id: string, cus_code: string) {
    const url = this.urlprovider.getUrl(LIST_UNIT) + `/${model_id}`;
    return this.http.post(url, { customer_code: cus_code }).map((res: any) => {
      return res.model;
    });
  }

  getCustomerStatus() {
    const url = this.urlprovider.getUrl(GET_CUSTOMER_STATUS);
    return this.http.post(url, {}).map((res: any) => {
      return res.model;
    });
  }

  getOrderStatus() {
    const url = this.urlprovider.getUrl(GET_ORDER_STATUS);
    return this.http.post(url, {}).map((res: any) => {
      return res.model;
    });
  }

  getCustomerZoneList() {
    const url = this.urlprovider.getUrl(GET_CUSTOMER_ZONE);
    return this.http.post(url, {}).map((res: any) => {
      return res.model;
    });
  }
}
