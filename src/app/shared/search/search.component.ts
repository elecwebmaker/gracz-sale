import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from "@angular/forms";
import { BehaviorSubject } from 'rxjs/Rx';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  seachinput = new FormControl();
  @Output() onChange: EventEmitter<any> = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
    this.seachinput.valueChanges.debounceTime(500).distinctUntilChanged().subscribe((val)=>{
      console.log('search called!');
      this.onChange.emit(val);
    });
  }

}
