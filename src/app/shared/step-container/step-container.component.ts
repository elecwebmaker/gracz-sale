import { Component, OnInit, QueryList, ContentChildren, ViewChild, Input } from '@angular/core';
import { StepComponent, state } from "app/shared/step/step.component";

@Component({
  selector: 'app-step-container',
  templateUrl: './step-container.component.html',
  styleUrls: ['./step-container.component.scss']
})
export class StepContainerComponent implements OnInit {
    private isFindCurrent:boolean = false;
    @Input() current:string;
    @Input() classname:string;
    @ContentChildren(StepComponent) step: QueryList<StepComponent>;
    // @ViewChild(StepComponent) step: QueryList<StepComponent>;

    constructor() { }

    ngOnInit() {
    }

    ngAfterViewInit() {
        window.setTimeout(()=>{
            this.setState();
        },0)
        
    }

    setState():void {
        this.step.forEach((item)=>{
            if(!this.isFindCurrent) {
                if(item.address != this.current) {
                    item.setStage(state.completed);
                } else {
                    this.isFindCurrent = true;
                    item.setStage(state.active);
                }
            } else {
                item.setStage(state.disabled);
            }
        });
    }

}