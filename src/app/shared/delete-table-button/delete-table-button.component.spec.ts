import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteTableButtonComponent } from './delete-table-button.component';

describe('DeleteTableButtonComponent', () => {
  let component: DeleteTableButtonComponent;
  let fixture: ComponentFixture<DeleteTableButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteTableButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteTableButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
