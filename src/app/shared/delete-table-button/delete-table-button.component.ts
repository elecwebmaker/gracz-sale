import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'del-table-btn',
  templateUrl: './delete-table-button.component.html',
  styleUrls: ['./delete-table-button.component.scss']
})
export class DeleteTableButtonComponent implements OnInit {
  @Output('onClick') onClick: EventEmitter<undefined> = new EventEmitter<undefined>();
  @Input() loading: boolean;
  constructor() { }

  ngOnInit() {
  }

}
