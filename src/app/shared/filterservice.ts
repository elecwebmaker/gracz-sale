import { BehaviorSubject } from "rxjs/Rx";

export class Filterservice<T> {
    searchObservable: BehaviorSubject<T>;
    filterVal = {};
    clearFilter(){
        this.filterVal = {};
    }
    setfilter(filter:any){
        this.filterVal = {
            ...this.filterVal,
            ...filter
        }
        this.searchObservable.next(<T>this.filterVal);
    }
    constructor(filter?:T){
        this.searchObservable = new BehaviorSubject<T>(filter);
    }
}