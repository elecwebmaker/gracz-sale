import { Component, OnInit, Input} from '@angular/core';

export enum state {
    disabled,
    active,
    completed
}

@Component({
  selector: 'app-step',
  templateUrl: './step.component.html',
  styleUrls: ['./step.component.scss']
})

export class StepComponent implements OnInit {

    private stage:state;
    @Input() address: string;

    constructor() { }

    ngOnInit() {
    }

    setStage(val:state):void {
        this.stage = val;
    }

    get getStage():state {
        return this.stage;
    }

}
