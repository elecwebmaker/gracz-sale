import { BehaviorSubject, Observable } from 'rxjs/Rx';

export interface IFilterKey {
    status: string;
    key: string;
}

export class FilterBar<T> {
    private filter:T;
    props:Array<any>;
    private filterChanges$: BehaviorSubject<T> = new BehaviorSubject(null);
    public statusList: Array<{value: string, text: string}> = [];

    constructor(props) { 
        this.filter = <T>{};
        this.props = props;
        this.props.map((prop)=>{
            this.filter[prop] = undefined;
        })
    }

    setStatusList(statusList: Observable<Array<{id: any, text: any}>>) {
        statusList.subscribe((res) => {
            this.statusList = res.map((item) => {
                return {
                    value: item.id,
                    text: item.text
                };
            });
        });
    }
    hasProp(key){
        
        return this.props.indexOf(key) > -1;
    }
    setFilter(key: string, value: string) {
        if (this.hasProp(key)){
            this.filter[key] = value;
            this.filterChanges$.next(this.filter);
        }else {
            throw Error('Can\'t find key value mumu');
        }
    }

    filterChanges(): Observable<T> {
        return this.filterChanges$;
    }
}