import { Component, OnInit, HostListener } from "@angular/core";
import { UserAuthenService } from "app/core/user-authen.service";
import { Router } from "@angular/router";
import { SidebarService } from "app/shared/sidebar.service";
declare var $: any;

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.scss"]
})
export class SidebarComponent implements OnInit {
  isOpen = false;
  name: string;
  position: string;
  picture_src: string;
  logout_loading = false;
  @HostListener("window:resize", ["$event"])
  onResize(event) {
    this.checkWindowSize(event.target.innerWidth);
  }
  constructor(
    private userAuthenservice: UserAuthenService,
    private router: Router,
    private sidebarService: SidebarService
  ) {}

  ngOnInit() {
    this.checkWindowSize(window.innerWidth);
    const {
      name,
      picture_src,
      position
    } = this.userAuthenservice.getUserData();
    this.name = name;
    this.position = position;
    this.picture_src = picture_src;
    this.sidebarService.getState().subscribe((state) => {
      this.isOpen = state;
    });
  }

  closeSidebar() {
    this.sidebarService.setState(false);
  }

  checkWindowSize(width) {
    if (width > 1200) {
      this.sidebarService.setState(true);
    } else {
      this.sidebarService.setState(false);
    }
  }

  logout() {
    this.logout_loading = true;
    this.userAuthenservice.logout().subscribe(() => {
      this.logout_loading = false;
      this.router.navigate(["/login"]);
    });
  }
}
