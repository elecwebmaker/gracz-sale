import { Directive, Input, ElementRef, AfterViewInit, OnInit, TemplateRef, ViewContainerRef, OnDestroy, Renderer2,  } from '@angular/core';
import { FormGroup, ControlContainer, FormGroupDirective, FormControl, AbstractControl, NgForm, FormArrayName } from '@angular/forms';
import { Observable, Subscription } from 'rxjs/Rx';
@Directive({
  selector: '[invalidmessage],[controlinvalidmessage]'
})
export class InvalidmessageDirective implements OnInit, OnDestroy{
  @Input() invalidmessage: string;
  @Input() controlinvalidmessage: FormControl;
  control: AbstractControl;
  hasView = false;
  controlValue$: Observable<any>;
  controlSubscription: Subscription;
  hasSubmitted: boolean;
  constructor(
    private _fg: ControlContainer,
    private _el: ElementRef,
    private render: Renderer2
  ) { }

  ngOnInit() {
    let formDirective: FormGroupDirective = (<FormGroupDirective>this._fg);
    if (this.invalidmessage) {
      this.control = this.form.get(this.invalidmessage);
    }else if (this.controlinvalidmessage){
      this.control = this.controlinvalidmessage;
    }else{
      throw new Error('Please provide formcontrol');
    }
    if(this._fg instanceof FormArrayName){
      formDirective = this._fg.formDirective;
    }

    const formSubmit$ = formDirective.ngSubmit.map(()=>{
        this.hasSubmitted = true;
    });
    this.controlValue$ =  Observable.merge(this.control.valueChanges, Observable.of(''), formSubmit$ );
    this.controlSubscription = this.controlValue$.subscribe(() => {
      this.setVisible();
    });
  }

  private setVisible() {
    if (this.control.invalid && (this.control.dirty || this.hasSubmitted)) {
      this.render.removeStyle(this._el.nativeElement, 'display');
    }else {
      this.render.setStyle(this._el.nativeElement, 'display', 'none');
    }
  }

  match(error: string){
    if (this.control && this.control.errors){
      if (Object.keys(this.control.errors).indexOf(error) > -1){
        return true;
      }
    }
    return false;
  }

  get form(){ return this._fg.formDirective ? (this._fg.formDirective as FormGroupDirective).form : null; }

  ngOnDestroy(){
    this.controlSubscription.unsubscribe();
  }
}
