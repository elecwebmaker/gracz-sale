import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs/Rx";

export interface TopbarState{
  title:string;
  hasButton:boolean;
  route?:string;
}


@Injectable()
export class TopbarService {
  state:TopbarState;
  state$:BehaviorSubject<TopbarState> = new BehaviorSubject<TopbarState>({title:'',hasButton:false});
  constructor() { 

  }

  setState(state:TopbarState){
    this.state = state;
    this.state$.next(this.state);
  }

  getState(){
    return this.state$;
  }
}
