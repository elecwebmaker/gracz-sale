import { CfLoadManageContainer } from "app/shared/cf-comp-dispatcher/cf-load-manage-container";
import { BehaviorSubject } from "rxjs/Rx";

export class CfDataDispatcher<T>{
    loadingmanage: CfLoadManageContainer = new CfLoadManageContainer();
    dataDispatch:T;

    constructor(){
        this.loadingmanage.setLoading(true);
    }

    setData(data:T){
        this.dataDispatch = data;
        this.loadingmanage.setLoading(false);
    }

    getLoading():BehaviorSubject<boolean>{
        return this.loadingmanage.loading$;
    }

    clearData(){
        this.dataDispatch = undefined;
        this.loadingmanage.setLoading(true);
    }
}