import { CfDataDispatcher } from "app/shared/cf-comp-dispatcher/cf-data-dispatcher";

export interface CfIContainerComp<T> {
    data_dispatcher:CfDataDispatcher<T>;
}