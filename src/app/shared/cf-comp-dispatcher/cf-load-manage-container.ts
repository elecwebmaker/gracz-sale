import { BehaviorSubject } from 'rxjs/Rx';
export class CfLoadManageContainer{
    loading$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    setLoading(loading:boolean){
        this.loading$.next(loading);
    }
}