import { Directive } from '@angular/core';
import { NoLetterDirective } from './no-letter.directive';
@Directive({
  selector: '[formControl][appOnlyNumberAndPlus]'
})
export class OnlyNumberAndPlusDirective extends NoLetterDirective{
  setMask(val){
    var curr_val =  val;
    let alphabhet = /[^0-9\+]/g;
    curr_val = curr_val.replace(alphabhet, '');
    return curr_val;
}

}
