import { Directive, ElementRef, Renderer2, Input, HostListener } from '@angular/core';
import { FormControl } from '@angular/forms';

@Directive({
  selector: '[formControl][appNumberAndDash]'
})
export class NoLetterDirective {

  private _onChange:Function;
  private _onTouched:Function;
  @Input('formControl') formcontrol: FormControl;
  constructor(
      private el: ElementRef,
      private render: Renderer2,
  ) { }
  ngOnInit(){
      this.changeValuedown();
  }
  setMask(val){
      var curr_val =  val;
      let alphabhet = /[^0-9\-]/g;
      curr_val = curr_val.replace(alphabhet, '');
      return curr_val;
  }
  @HostListener('input') changeValuedown() {
    console.log('appNoLetter');
    let curr = this.setMask(this.el.nativeElement.value);
    this.formcontrol.setValue(curr);
  }

}
