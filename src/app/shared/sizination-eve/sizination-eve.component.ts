import { Component, OnInit, Input, forwardRef, EventEmitter, Output } from '@angular/core';
import { NG_VALUE_ACCESSOR ,ControlValueAccessor} from '@angular/forms';

export const CUSTOM_SIZE_CONTROL_VALUE_ACCESSOR:any = {
  provide:NG_VALUE_ACCESSOR,
  useExisting:forwardRef(()=>SizinationEveComponent),
  multi:true
}

const noop = () => {
};
@Component({
  selector: 'app-sizination-eve',
  templateUrl: './sizination-eve.component.html',
  styleUrls: ['./sizination-eve.component.scss'],
  providers:[CUSTOM_SIZE_CONTROL_VALUE_ACCESSOR]
})
export class SizinationEveComponent implements OnInit {
  @Input('class') classStyle: string;
  @Input('sizeAvailable') sizeAvailable: Array<number>;
  @Output('onChange') onChange = new EventEmitter<number>();
  private current_in_size:number;
  get current_size():number{
      return this.current_in_size;
  }
  set current_size(value:number){
      this.current_in_size = value;
      this.onChangeCallback(value);
      this.onChange.emit(this.current_size);
  }
  constructor() { }

  
  ngOnInit() { 
      
  }

  private onTouchedCallback:()=>void = noop;
  private onChangeCallback:(_any)=>void = noop;

  setSize(value:any):void{
      if(value !== this.current_size){
           this.current_size = value;
           
      }
     
  }

  writeValue(value:any){
      this.setSize(value);
  }

  registerOnChange(fn:any){
      this.onChangeCallback = fn;
  }

  registerOnTouched(fn:any){
      
  }

}
