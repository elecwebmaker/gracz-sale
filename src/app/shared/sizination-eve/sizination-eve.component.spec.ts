import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SizinationEveComponent } from './sizination-eve.component';

describe('SizinationEveComponent', () => {
  let component: SizinationEveComponent;
  let fixture: ComponentFixture<SizinationEveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SizinationEveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SizinationEveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
