import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTableButtonComponent } from './view-table-button.component';

describe('ViewTableButtonComponent', () => {
  let component: ViewTableButtonComponent;
  let fixture: ComponentFixture<ViewTableButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTableButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTableButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
