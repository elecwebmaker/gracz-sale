import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'view-table-btn',
  templateUrl: './view-table-button.component.html',
  styleUrls: ['./view-table-button.component.scss']
})
export class ViewTableButtonComponent{
  @Output('onClick') onClick: EventEmitter<undefined> = new EventEmitter<undefined>();
  constructor() { }

  

}
