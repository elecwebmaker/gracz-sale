import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CfSelectComponent } from './cf-select.component';

describe('CfSelectComponent', () => {
  let component: CfSelectComponent;
  let fixture: ComponentFixture<CfSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CfSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CfSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
