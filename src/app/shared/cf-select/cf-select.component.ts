import { Component, forwardRef, ContentChildren, Input, OnChanges, SimpleChanges, AfterContentInit, QueryList } from '@angular/core';
import { NG_VALUE_ACCESSOR, FormControl, ControlValueAccessor } from '@angular/forms';
import { CfChoiceComponent } from 'app/shared/cf-choice/cf-choice.component';
import { Subscription } from "rxjs/Subscription";

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => CfSelectComponent),
    multi: true
};

@Component({
    selector: 'cf-select',
    templateUrl: './cf-select.component.html',
    styleUrls: ['./cf-select.component.scss'],
    providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})
export class CfSelectComponent implements ControlValueAccessor, OnChanges,AfterContentInit {
    private isDisable: boolean = false;
    public chosen_choice: FormControl;
    public optionSubscription: Array<Subscription> = [];
    private _onChange: (_: any) => {};
    private _onTouched: () => {};

    @ContentChildren(CfChoiceComponent) choice: QueryList<CfChoiceComponent>;

    get logValue() {
        return this.chosen_choice.value;
    }

    set logValue(item: any) {
        this.chosen_choice.setValue(item);
    }

    writeValue(value: any) {
        console.log("change ja ",this.choice);
        if (!value) {
            return ;
        }
        if (value !== this.chosen_choice.value) {
            this.chosen_choice.setValue(value);
        }
    }

    registerOnChange(fn: any) {
        this._onChange = fn;
    }

    registerOnTouched(fn: any) {
        this._onTouched = fn;
    }

    constructor() { }

    ngOnInit() {
        this.chosen_choice = new FormControl();
    }



    ngAfterContentInit() {
        this.choice.changes.subscribe(()=>{
             this.subscribeOption();
             this.checkSelect(this.logValue);
        })

    }

    subscribeOption(){
        this.clearSubscribeOptions();
         this.choice.forEach(element => {
            const subscription = element.onClick.subscribe( (event) => {
                if (!this.isDisable) {
                    this.checkSelect(event);
                    this.chosen_choice.setValue(event);
                    this.onChange();
                }
            });
            this.optionSubscription.push(subscription);
        });
    }

    clearSubscribeOptions(){
        this.optionSubscription.map((sub)=>{
            sub.unsubscribe();
        })
        this.optionSubscription = [];
    }

    ngOnChanges(change:SimpleChanges){
        console.log("change",change);
    }

    setDisabledState(isDisabled: boolean) {
        if (isDisabled) {
            this.isDisable = true;
        } else {
            this.isDisable = false;
        }
    }

    checkSelect(value) {
        this.choice.forEach(element => {
            if (element.value === value) {
                element.setActive();
            } else {
                element.setDeactive();
            }
        });
    }

    onChange() {
        this._onChange(this.logValue);
    }
}
