import { Component, OnInit, ViewContainerRef, ViewChild, ComponentRef, ComponentFactoryResolver, ComponentFactory, NgModule, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { CustomerItemComponent } from 'app/customer/customer-item/customer-item.component';
import { CustomerItemModel } from "app/customer/model/customer-item.model";
import { OrderItemModel } from "app/order/model/order-item.model";
import { OrderItemComponent } from "app/order/order-item/order-item.component";

@Component({
    selector: 'app-wrapper-list',
    templateUrl: './wrapper-list.component.html',
    styleUrls: ['./wrapper-list.component.scss'],
    entryComponents: [CustomerItemComponent, OrderItemComponent]
})
export class WrapperListComponent implements OnChanges {
    public isEmpty: boolean;
    @Input('item') items: Array<any>;
    @Output('itemClick') itemClick = new EventEmitter();
    @ViewChild('item', { read: ViewContainerRef }) container:ViewContainerRef;
    componentRef: ComponentRef<any>;

    constructor(private resolver: ComponentFactoryResolver) { }

    ngOnChanges(change) {
        if(change && this.items){
            this.container.clear();
            if (this.items.length === 0) {
                this.isEmpty = true;
            } else {
                this.isEmpty = false;
                this.items.forEach((item, index) => {
                    this.listItem(item, index);
                });
            }
        }

    }

    listItem(item, index) {
        let factory: ComponentFactory<any>;
        if (item instanceof CustomerItemModel) {
           
            factory  = this.resolver.resolveComponentFactory(CustomerItemComponent);
        } else if (item instanceof OrderItemModel) {
           
            factory  = this.resolver.resolveComponentFactory(OrderItemComponent);
        } else {
           
        }
        
        this.componentRef = this.container.createComponent(factory);
        if (index % 2 == 0) {
            this.componentRef.instance.isStrip = false;
        } else {
            this.componentRef.instance.isStrip = true;
        }
        this.componentRef.instance.model = item;
        
        this.componentRef.instance.output.subscribe(item => {
            this.itemClick.emit(item);
        });
    }

    ngOnDestroy() {
        if (this.componentRef) {
            this.componentRef.destroy();
        }
    }

}