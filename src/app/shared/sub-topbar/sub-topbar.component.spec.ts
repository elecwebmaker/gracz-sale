import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubTopbarComponent } from './sub-topbar.component';

describe('SubTopbarComponent', () => {
  let component: SubTopbarComponent;
  let fixture: ComponentFixture<SubTopbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubTopbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubTopbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
