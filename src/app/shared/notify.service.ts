import { Injectable } from '@angular/core';
import { MdSnackBar } from '@angular/material';
const defaultErrorMessage = 'มีข้อผิดพลาดเกิดขึ้น กรุณาลองใหม่อีกครั้ง';
const defaultSuccessMessage = 'บันทึกข้อมูลสำเร็จ';
@Injectable()
export class NotifyService {

   constructor(
    protected snackbar: MdSnackBar
  ) { }


  showError(message?:string){
    if(!message){
      message = defaultErrorMessage;
    }
    this.snackbar.open(message, 'ตกลง', {
      duration: 2000
    });
  }

  showSuccess(message?:string){
    if (!message){
      message = defaultSuccessMessage;
    }
    this.snackbar.open(message, 'ตกลง', {
      duration: 2000
    });
  }

  showSuccessDelete(){
    this.showSuccess('ลบข้อมูลสำเร็จ');
  }

  showErrorDelete(){
    this.showSuccess('ลบข้อมูลไม่สำเร็จ');
  }

  showUploadSuccess(){
    this.showSuccess('อัพโหลดไฟล์สำเร็จ');
  }

  showUploadError(){
     this.showSuccess('อัพโหลดไฟล์ผิดพลาด');
  }
}
