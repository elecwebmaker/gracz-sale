import { Component, OnInit, Input, AfterViewInit, forwardRef, Output, EventEmitter, DoCheck, ElementRef, ViewChild } from '@angular/core';
import { FormControl, ControlValueAccessor, NG_VALUE_ACCESSOR, FormGroup, FormBuilder, Validators, Validator, NG_VALIDATORS } from '@angular/forms';
import { Observable } from "rxjs/Rx";
import { ListitemdataService } from "app/shared/listitemdata.service";
import { SemanticSelectComponent } from "ng-semantic/ng-semantic";
import { CompleterService } from 'ng2-completer';
declare var $: any;

export interface Iaddress {isBill?,id:string, addr:string, prov:string, city:string, dist:string, code:string}
export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => SingleAddressComponent),
    multi: true
};
export const Custom_validator: any = {
    provide: NG_VALIDATORS,
    useExisting: forwardRef(() => SingleAddressComponent),
    multi: true
}
@Component({
    selector: 'app-single-address',
    templateUrl: './single-address.component.html',
    styleUrls: ['./single-address.component.scss'],
    providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR, Custom_validator]
})

export class SingleAddressComponent implements OnInit, AfterViewInit, Validator{
    @Input() hasCheckbox: boolean = false;
    @Output() fillAddress = new EventEmitter();
    @Input('disabled') singleDisabled: boolean = false;
    @ViewChild('selectprovince') province_select: SemanticSelectComponent;
    @ViewChild('selectdistrict') district_select: SemanticSelectComponent;
    @ViewChild('selectsubdistrict') subdistrict_select: SemanticSelectComponent;

    public cbStatus: boolean = false;

    public errorMsg: {required: string} = {
        required: 'กรุณากรอกข้อมูล'
    }

    public address: FormGroup;

    private _onChange: (_: any) => {};
    private _onTouched: () => {};

    public province: Array<any> = [];
    public district: Array<any> = [];
    public sub_district: Array<any> = [];

    constructor(private _fb: FormBuilder,private listitemdata: ListitemdataService,private completerService: CompleterService) { }

    ngOnInit() {
        this.address = this._fb.group({
            id: [''],
            address: ['', [Validators.required]],
            province: ['', [Validators.required]],
            city: ['', [Validators.required]],
            district: ['', [Validators.required]],
            postcode: ['', [Validators.required]],
            isBill:[false]
        });
        this.address.get('province').valueChanges.subscribe((res) => {
            this.getProvince(this.address.get('province').value);
        });
        this.address.get('city').valueChanges.subscribe((res) => {
            this.getDistrict(this.address.get('province').value, this.address.get('city').value);
        });
        this.address.get('district').valueChanges.subscribe((res) => {
            this.getSubDistrict(this.address.get('province').value, this.address.get('city').value, this.address.get('district').value);
        });
        // this.address.get('city').valueChanges.subscribe((res) => {
        //     this.getSubDistrict(this.address.get('city').value);
        // });
        // this.getProvince();
    }

    ngAfterViewInit() {
        $('.ui.checkbox').checkbox();
        this.address.valueChanges.subscribe(() => {
            this.onChange();
        });
    }

    get logValue(): Iaddress {
        return {
            id: this.address.get('id').value,
            addr: this.address.get('address').value,
            prov: this.address.get('province').value,
            city: this.address.get('city').value,
            dist: this.address.get('district').value,
            code: this.address.get('postcode').value,
            isBill:this.address.get('isBill').value
        };
    }

    set logValue(val: Iaddress) {
        this.address.patchValue({
            id: val.id,
            address: val.addr,
            province: val.prov,
            city: val.city,
            district: val.dist,
            postcode: val.code,
            isBill:val.isBill ? val.isBill : false
        });
    }

    writeValue(val: any) {
        if (!val) {
            return ;
        }
        if (this.logValue !== val) {
            this.logValue = val;
        }
    }

    registerOnChange(fn:any) {
        this._onChange = fn;
    }

    registerOnTouched(fn:any) {
        this._onTouched = fn;
    }

    onChange() {
        this._onChange(this.logValue);
    }

    singleEmit() {
        this.cbStatus = !this.cbStatus;
        if (this.cbStatus) {
            this.singleDisabled = true;
        }
        this.fillAddress.emit(this.cbStatus);
    }

    validate(c: FormControl){
        return this.address.valid ? null : {
            addresscomplete: true
        };
    }

    getProvince(province) {
        this.listitemdata.getProvince(province).subscribe((res)=>{
             this.province = res.map((province)=>{
                return province.text;
             });
            //  window.setTimeout( () => {
            //      this.province_select.reselect();
            //  }, 0);
        });
    }

    getDistrict(province,district) {
         this.listitemdata.getDistrict(province,district).subscribe((res)=>{
             this.district = res.map((district)=>{
                return district.text;
             });
            //   window.setTimeout(() => {
            //     this.district_select.reselect();
            //   }, 0);
        });
    }

    resetDistrict(isAll) {
        this.address.get('district').setValue('');
        if (isAll) {
            this.address.get('city').setValue('');
        }
    }

    getSubDistrict(province,district,subdistrict) {
        this.listitemdata.getSubDistrict(province,district,subdistrict).subscribe((res)=>{
             this.sub_district = res.map((sub_district)=>{
                return sub_district.text;
             });
            //  window.setTimeout(() => {
            //      this.subdistrict_select.reselect();
            //  }, 0);
        });
    }
}
