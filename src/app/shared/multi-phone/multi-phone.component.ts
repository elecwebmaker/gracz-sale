import { Component, OnInit, Input, forwardRef, EventEmitter, Output } from '@angular/core';
import { FormControl, ControlValueAccessor, NG_VALUE_ACCESSOR, NG_VALIDATORS, Validator, Validators } from "@angular/forms";

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => MultiPhoneComponent),
    multi: true
};
export const CUSTOM_VALIDATOR: any = {
    provide: NG_VALIDATORS,
    useExisting: forwardRef(() => MultiPhoneComponent),
    multi: true
};

interface IphoneControl{ 
    country_code: FormControl; 
    mobile_number: FormControl;
    id: FormControl;
};
interface Iphone{
    id?: string;
    code?: string;
    number?: string;
}
@Component({
    selector: 'app-multi-phone',
    templateUrl: './multi-phone.component.html',
    styleUrls: ['./multi-phone.component.scss'],
    providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR, CUSTOM_VALIDATOR]
})

export class MultiPhoneComponent implements OnInit, ControlValueAccessor, Validator  {
    @Input() classname: string;
    @Output() onDelete: EventEmitter<Iphone> = new EventEmitter<Iphone>();
    public phones: Array<IphoneControl> = [];
    private _onChange: (_: any) => {};
    private _onTouched: () => {};
    private isCompleted(){
        return this.phones.every((control_obj) => {
            return control_obj.country_code.valid && control_obj.mobile_number.valid;
        });

    }
    constructor() { }

    ngOnInit() {
        this.add_number(true);
    }

    add_number(notUpdate: boolean) {
        this.phones.push({
            country_code: new FormControl('', Validators.required),
            mobile_number: new FormControl('', Validators.required),
            id: new FormControl('')
        });
        if (!notUpdate){
             this.onChange();
        }
    }

    del_number(i) {
        this.onDelete.emit(this.parseFormtoValue(this.phones)[i]);
        this.phones.splice(i, 1);
        this.onChange();
    }

    parseFormtoValue(form: any) {
        return form.map((item) => {
            const tmp: Iphone = {};
            tmp.code = item.country_code.value;
            tmp.number = item.mobile_number.value;
            tmp.id = item.id.value;
            return tmp;
        });
    }

    parseValuetoForm(val: any) {
        return val. map((item) => {
            const tmp: IphoneControl = {
                country_code: new FormControl('', Validators.required),
                mobile_number: new FormControl('', Validators.required),
                id: new FormControl('')
            };
            tmp.country_code.setValue(item.code);
            tmp.mobile_number.setValue(item.number);
            tmp.id.setValue(item.id);
            return tmp;
        });
    }

    get logvalue(){
        return this.parseFormtoValue(this.phones);
    }

    set logvalue(val: Array<Iphone>) {
        if (!val){
            this.phones = [];
            return ;
        }
        this.phones = this.parseValuetoForm(val);
    }

    writeValue(val: any) {
        if (!val) {
            return 
        }
        if (this.logvalue != val) {
            this.logvalue = val;
        }
    }

    registerOnChange(fn: any) {
        this._onChange = fn;
    }

    registerOnTouched(fn: any) {
        this._onTouched = fn;
    }

    onChange() {
        this._onChange(this.logvalue);
    }

    validate(c: FormControl) {
        return this.isCompleted() ? null : { phonecomplete: true };
    }
}