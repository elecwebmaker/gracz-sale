import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiPhoneComponent } from './multi-phone.component';

describe('MultiPhoneComponent', () => {
  let component: MultiPhoneComponent;
  let fixture: ComponentFixture<MultiPhoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiPhoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiPhoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
