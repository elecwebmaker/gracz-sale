import { TestBed, inject } from '@angular/core/testing';

import { UrlproviderService } from './urlprovider.service';

describe('UrlproviderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UrlproviderService]
    });
  });

  it('should ...', inject([UrlproviderService], (service: UrlproviderService) => {
    expect(service).toBeTruthy();
  }));
});
