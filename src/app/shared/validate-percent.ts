import { FormControl } from '@angular/forms';


export function validateDiscountPercent(inputPct: FormControl){
        if(inputPct.value >= 0 && inputPct.value <= 100 ){
            return null
        }else{
            return {
                validatePercent: true
            }
        }
}


export function validateDiscountValue(inputPct: FormControl){
    return (c: FormControl) => {
        if(inputPct.value >= 0 && inputPct.value <= 100 ){
            if(inputPct.errors){
                delete inputPct.errors["validatePercent"];
            }
        }else{
            console.log('mu validate error')
            inputPct.setErrors({
                validatePercent: true
            })
        }
        if(inputPct.errors && !Object.keys(inputPct.errors).length){
             console.log('mu validate clear')
                inputPct.setErrors(null);
        }
    }
}