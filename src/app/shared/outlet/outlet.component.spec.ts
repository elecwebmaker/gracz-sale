import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerOutletComponent } from './customer-outlet.component';

describe('CustomerOutletComponent', () => {
  let component: CustomerOutletComponent;
  let fixture: ComponentFixture<CustomerOutletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerOutletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerOutletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
