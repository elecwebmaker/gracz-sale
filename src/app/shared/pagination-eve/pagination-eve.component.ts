import { Component, OnInit, Input, forwardRef, EventEmitter, Output } from '@angular/core';
import { NG_VALUE_ACCESSOR,ControlValueAccessor } from '@angular/forms';
import { CFPage } from 'craft-table';
export const CUSTOM_PAGE_CONTROL_VALUE_ACCESSOR: any = {
  provide:NG_VALUE_ACCESSOR,
  useExisting:forwardRef(()=>PaginationEveComponent),
  multi:true
}
const noop = () => {
};
@Component({
  selector: 'app-pagination-eve',
  templateUrl: './pagination-eve.component.html',
  styleUrls: ['./pagination-eve.component.scss'],
  providers:[CUSTOM_PAGE_CONTROL_VALUE_ACCESSOR]
})
export class PaginationEveComponent implements OnInit {
  @Input('class') class:String;
  @Output('onChange') onChange = new EventEmitter<number>();
  _total:number = 50;
  @Input('total')
  set total(val){
    this._total = val;

    this.genPageAvailable();
  }
  get total(){
      return this._total;
  }

  _size:number = 10;
  @Input('size')
  set size(val){
    this._size = val;
    console.log('size has setted');

    this.genPageAvailable();
    this.setPage(1,true);
  }
  get size(){
      return this._size;
  }
  
  total_page:number;
  page_available:Array<CFPage>;
  private current_page:number;

  private onTouchedCallback:()=>void = noop;
  private onChangeCallback:(_any)=>void = noop;
  writeValue(value:any){
      
      this.setPage(value,false);
      
  }

  registerOnChange(fn:any){
      this.onChangeCallback = fn;
  }

  registerOnTouched(fn:any){
      
  }

  setPage(value:any,update:boolean){
      console.log('setPAge!');
      this.current_page = value;
      
      if(update){
          this.onChangeCallback(value);
          
      }
      this.genPageAvailable();
      this.onChange.emit(value);
      
  }

  constructor() { 

  }

  ngOnInit() { 
     
  }

  genPageAvailable(){
      console.log('genPageAvailable',this.size);
      console.log('genPageAvailable',this.total);
    let max_pages = 7;
    var res_arr = [];
    this.total_page = Math.ceil(this.total / this.size);
    this.current_page = Number(this.current_page);
    if(this.total_page <= max_pages){
        this.total_page >= 2 ?   this.genCFPageAvailable(new Array(this.total_page).fill(1).map((x,i)=>i+1)) :  this.genCFPageAvailable([1]);
        return false;
    }
 
    if(this.current_page >= 5){
        let prev = this.current_page - 1;
        
        res_arr = [1,"...",prev,this.current_page];
        if(this.current_page + 1 <= this.total){
            let next = this.current_page + 1;
            res_arr.push(next);
        }

    }else{
        res_arr = [1,2,3,4,5];
    }

    if(this.current_page + 2 < this.total_page){
        res_arr.push("...");
        res_arr.push(this.total_page);
    }
   
    if(this.current_page + 3 >= this.total_page){
        
        res_arr = [1,"...",this.total_page-4,this.total_page-3,this.total_page-2,this.total_page-1,this.total_page]
    }
    this.genCFPageAvailable(res_arr);
    
}

private genCFPageAvailable(page_available:Array<any>){
    this.page_available = page_available.map((page)=>{
        if(page != "..."){
            return new CFPage(page+"",page+"");
        }else{
            return new CFPage(page+"","none");
        }
     }).map((pageitem)=>{
        if(pageitem.value == <any>this.current_page){
            pageitem.setCurrent(true);  
        }else{
            pageitem.setCurrent(false);
        }

        if(pageitem.value == "1"){
            pageitem.setFirst(true);
        }else{
            pageitem.setFirst(false);
        }

        if(pageitem.value == this.total_page+""){
            pageitem.setLast(true);
        }else{
             pageitem.setLast(false);
        }
        return pageitem;
     });

}
}
