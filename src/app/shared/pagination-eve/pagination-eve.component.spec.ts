import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginationEveComponent } from './pagination-eve.component';

describe('PaginationEveComponent', () => {
  let component: PaginationEveComponent;
  let fixture: ComponentFixture<PaginationEveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaginationEveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginationEveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
