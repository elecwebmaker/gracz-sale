import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiAddressComponent } from './multi-address.component';

describe('MultiAddressComponent', () => {
  let component: MultiAddressComponent;
  let fixture: ComponentFixture<MultiAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
