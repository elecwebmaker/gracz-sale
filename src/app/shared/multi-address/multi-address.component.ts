import { Component, OnInit, Input, AfterViewInit, forwardRef, Output, EventEmitter } from '@angular/core';
import { FormControl, ControlValueAccessor, NG_VALUE_ACCESSOR, FormGroup, FormArray, NG_VALIDATORS, Validator, Validators } from "@angular/forms";
import { Iaddress } from "app/shared/single-address/single-address.component";
declare var $: any;
export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => MultiAddressComponent),
    multi: true
};
export const CUSTOM_VALIDATOR: any = {
    provide: NG_VALIDATORS,
    useExisting: forwardRef(() => MultiAddressComponent),
    multi: true
}
@Component({
    selector: 'app-multi-address',
    templateUrl: './multi-address.component.html',
    styleUrls: ['./multi-address.component.scss'],
    providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR, CUSTOM_VALIDATOR]
})
export class MultiAddressComponent implements OnInit, AfterViewInit, Validator{
    @Output() billToShip: EventEmitter<any> = new EventEmitter();
    @Output() onDelete: EventEmitter<Iaddress> = new EventEmitter<Iaddress>();
    @Input() disabled: boolean;
    public groupAddress: FormGroup;
    private _onChange: (_:any)=>{};
    private _onTouched: ()=>{};
    public ischecked: boolean = false;
    public b2sIndex: number;
    constructor() { }

    ngOnInit() {
        this.groupAddress = new FormGroup({
            addresses: new FormArray([])
        });
        this.add_address(true);
        
    }

    ngAfterViewInit() {
        $('.ui.checkbox').checkbox();
        this.groupAddress.valueChanges.subscribe(() => {
            this.onChange();
        });
        this.validate(null);
    }

    fillAddress(i) {
        console.log('checkbox',i);
        this.ischecked = !this.ischecked;
        this.b2sIndex = i;
        this.billToShip.emit({index:i,ischecked:this.ischecked});
    }

    add_address(notupdate: boolean) {
        (<FormArray>this.groupAddress.get('addresses')).push(new FormControl('',[Validators.required]));
        if(!notupdate){
            this.onChange();
        }
    }

    del_address(i) {
        this.onDelete.emit(this.parseFormtoValue((<FormArray>this.groupAddress.get('addresses')))[i]);
        (<FormArray>this.groupAddress.get('addresses')).removeAt(i);
    }

    parseFormtoValue(form: any): Array<Iaddress> {
        let tmp: Array<Iaddress> = [];
        for (let i = 0; i < form.length; i++) {
            tmp[i] = form.at(i).value;
        }
        return tmp;
    }

    parseValuetoForm(val: Array<Iaddress>, form: any) {
        for (let i = 0; i < val.length; i++) {
            form.at(i).setValue(val[i]);
        }
    }

    get logValue(): Array<Iaddress> {
        return this.parseFormtoValue(this.groupAddress.get('addresses'));
    }

    set logValue(val: Array<Iaddress>) {
        for (let i = 0; i < val.length; i++) {
            if (!(<FormArray>this.groupAddress.get('addresses')).at(i)) {
                this.add_address(true);
            }
        }
        this.parseValuetoForm(val, this.groupAddress.get('addresses'));
       
    }

    writeValue(val:any) {
        if(!val) {
            return ;
        }
        if(this.logValue !== val) {
            this.logValue = val;
            this.addrCtrls.map((control,index)=>{
                console.log('control',control);
                if(control && control.value.isBill == true){
                    this.ischecked = true;
                    this.b2sIndex = index;
                }
            });
        }
    }

    registerOnChange(fn: any) {
        this._onChange = fn;
    }

    registerOnTouched(fn: any) {
        this._onTouched = fn;
    }

    onChange() {
        console.log('onChange called');
       this._onChange(this.logValue);
    }

    get addrCtrls():Array<any>{
        if(this.groupAddress) {
            return (<FormArray>this.groupAddress.get('addresses')).controls;
        } else {
            return [];
        }
    }

    validate(c: FormControl) {
        var isValid = true;
        // window.setTimeout(()=>{
        console.log('start!');
        this.addrCtrls.map((control)=>{
            console.log('control.valid',control.valid);
            if(!control.valid){
                isValid = false;
            }
        });
        // });
        console.log(isValid,'isValid');
        return isValid ? null : { multiaddrcomplete: true };
    }
}
