export class AddressModel {
    public address: string = "";
    public province: string = "";
    public city: string = "";
    public district: string = "";
    public post_code: string = "";

    constructor() {}
}