import { EventEmitter } from "@angular/core";

export interface CanSubmit<T> {
    onSubmit: EventEmitter<T>;
}