import { Injectable } from '@angular/core';
import { FileSelectDirective, FileDropDirective, FileUploader, FileItem } from 'ng2-file-upload/ng2-file-upload';
import { Observable,Subject } from "rxjs/Rx";
import { UrlproviderService, UPLOAD_FILE } from "app/shared/urlprovider.service";
import { NotifyService } from "app/shared/notify.service";
export interface Itemupload {
  name: string;
  url: string;
  id?: string;
};
@Injectable()
export class UploaderService {
  upload$:Subject<Itemupload> = new Subject<Itemupload>();
  private uploader: FileUploader = new FileUploader({
    autoUpload:true,
    allowedFileType:['doc','pdf','xls','image'],
    
  });
  constructor(private urlprovider: UrlproviderService,private notifyservice: NotifyService) {
    this.uploader.setOptions({url:this.urlprovider.getUrl(UPLOAD_FILE)});
    this.uploader.onSuccessItem = (item,res) =>{
      
      const resjson = JSON.parse(res);
      console.log(resjson,"upload -success");
      const { absolute, original_name } = resjson;
      this.notifyservice.showUploadSuccess();
      this.upload$.next({
        name: original_name,
        url: "http://" + absolute
      });
    };

    this.uploader.onErrorItem = () => {
      this.notifyservice.showUploadError();
    }
  };

  upload(item: FileItem){
    item.method = 'POST';
    item.upload();
  };

  get queue(){
    return this.uploader.queue;
  }

  getInstance() {
    return this.uploader;
  }
}
