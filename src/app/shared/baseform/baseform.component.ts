import { Component, OnInit, Input,OnChanges } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ListitemdataService } from 'app/shared/listitemdata.service';
import { Observable } from 'rxjs/Rx';
import { UploaderService } from 'app/shared/uploader.service';

@Component({
  selector: 'app-baseform',
  templateUrl: './baseform.component.html',
  styleUrls: ['./baseform.component.scss']
})
export abstract class BaseformComponent implements OnInit {
  formg: FormGroup;
  @Input() abstract prefilldata;
  @Input() loading = false;
  // tslint:disable-next-line:no-input-rename
  @Input('disable-btn') disablebtn: boolean = false;
  resolveObservable: Array<Observable<any>> = [Observable.of('')];
  constructor(protected _fb: FormBuilder, protected listitemdata: ListitemdataService) {

  }

  abstract initialForm(): void;
  abstract submit(): void;
  abstract parsePrefill(): Object;
  OnInit(){

  }
  ngOnInit() {
   
    this.initialForm();
     this.OnInit();
     this.prefill();
  }

  prefill() {
    
      Observable.combineLatest(this.resolveObservable).subscribe(()=>{
        console.log("sub");
        if (this.prefilldata) {
          this.formg.patchValue(this.parsePrefill());
        }
      });
    
  }
}
