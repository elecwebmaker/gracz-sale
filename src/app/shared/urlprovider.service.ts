import { Injectable } from '@angular/core';
import { isMockApi } from 'app/config';
import { API_URL } from 'app/api';
export const GET_BUSINESS_TYPE = 'GET_BUSINESS_TYPE'
export const GET_SALES = 'GET_SALES';
export const GET_PROVINCE = 'GET_PROVINCE';
export const GET_DISTRICT = 'GET_DISTRICT';
export const GET_SUBDISTRICT = 'GET_SUBDISTRICT';
export const ADD_CUSTOMER = 'ADD_CUSTOMER';
export const EDIT_CUSTOMER = 'EDIT_CUSTOMER';
export const DELETE_SHIPADDR = 'DELETE_SHIPADDR';
export const DELETE_TEL = 'DELETE_TEL';
export const UPLOAD_FILE = 'UPLOAD_FILE';
export const GET_CUSTOMER = 'GET_CUSTOMER';
export const LIST_LESS_CUSTOMER = 'LIST_LESS_CUSTOMER';
export const LIST_STATUS = 'LIST_STATUS';
export const LIST_LESS_ORDER = 'LIST_LESS_ORDER';
export const LIST_SHIPMENT = 'LIST_SHIPMENT';
export const GET_ORDER = 'GET_ORDER';
export const LIST_PRODUCT = 'LIST_PRODUCT';
export const GEN_CODE_PRODUCT = 'GEN_CODE_PRODUCT';
export const ADD_ORDER = 'ADD_ORDER';
export const PUSH_PRODUCT = 'PUSH_PRODUCT';
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT';
export const UPDATE_ORDER = 'UPDATE_ORDER'; 
export const DEL_PRODUCT = 'DEL_PRODUCT';
export const LIST_CATEGORY = 'LIST_CATEGORY';
export const LIST_MODEL = 'LIST_MODEL';
export const LIST_UNIT = 'LIST_UNIT';
export const HAS_ORDER = 'HAS_ORDER';
export const HAS_CUSTOMER = 'HAS_CUSTOMER';
export const GET_CUSTOMER_STATUS  = 'GET_CUSTOMER_STATUS';
export const GET_ORDER_STATUS  = 'GET_ORDER_STATUS';
export const LIST_INVENTORY = 'LIST_INVENTORY';
export const DEL_FILE = 'DEL_FILE';
export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const ACCOUNT_GET = 'ACCOUNT_GET';
export const PASSWORD_RESET = 'PASSWORD_RESET';
export const ACCOUNT_EDIT = 'ACCOUNT_EDIT';
export const CLOSE_ORDER = 'CLOSE_ORDER';
export const GET_CUSTOMER_ZONE = 'GET_CUSTOMER_ZONE';
export const GET_TIME_STAMP = 'GET_TIME_STAMP';
export const CHK_SCREEN = 'CHK_SCREEN';
export const GET_PRICE = 'GET_PRICE';
export const generateUrl = function(url: string, mockurl: string, isOverideMock: boolean){
  return isMockApi || isOverideMock ? mockurl : API_URL + url;
}



@Injectable()
export class UrlproviderService {

    constructor() {

    }

    getUrl(type: string, isOverideMock = false){
        const generateUrlWithMock = function(url: string, mockurl: string){
            return generateUrl(url,mockurl,isOverideMock);
        };
        switch (type) {
        case GET_BUSINESS_TYPE:
            return generateUrlWithMock('/customer/businessType', 'http://demo0905840.mockable.io/businessType');
        case GET_SALES:
            return generateUrlWithMock('/sale/list', 'http://demo0905840.mockable.io/getSales');
        case GET_PROVINCE:
            return generateUrlWithMock('/addressInfo/provinceList', 'http://demo0905840.mockable.io/getProvince');
        case GET_DISTRICT:
            return generateUrlWithMock('/addressInfo/districtList/province_id', 'http://demo0905840.mockable.io/getDistrict');
        case GET_SUBDISTRICT:
            return generateUrlWithMock('/addressInfo/subdistrictList/district_id', 'http://demo0905840.mockable.io/getSubdistrict');
        case ADD_CUSTOMER:
            return generateUrlWithMock('/customer/add', 'http://demo0905840.mockable.io/customer/add');
        case DELETE_SHIPADDR:
            return generateUrlWithMock('/customer/deleteShipmentAddress/id', 'http://demo0905840.mockable.io/customer/add');
        case DELETE_TEL:
            return generateUrlWithMock('/customer/deletePhoneNumber/id', 'http://demo0905840.mockable.io/customer/add');
        case UPLOAD_FILE:
            return generateUrlWithMock('/customer/uploadFile', 'http://162.243.199.82/mju-co-op/api/member/uploadFile');
        case GET_CUSTOMER:
            return generateUrlWithMock('/customer/get/id', 'http://demo0905840.mockable.io/customer/get');
        case EDIT_CUSTOMER:
            return generateUrlWithMock('/customer/edit/id', '');
        case LIST_LESS_CUSTOMER:
            return generateUrlWithMock('/customer/list', '');
        case LIST_STATUS:
            return generateUrlWithMock('/customer/statusMapping', '');
        case LIST_LESS_ORDER:
            return generateUrlWithMock('/order/list','');
        case LIST_SHIPMENT:
            return generateUrlWithMock('/customer/getShipmentList/cid', 'http://demo0905840.mockable.io/shipmentAddr/list');
        case GET_ORDER:
            return generateUrlWithMock('/order/get/oid', '');
        case LIST_PRODUCT:
            return generateUrlWithMock('/order/productList/oid', '');
        case GEN_CODE_PRODUCT:
            return generateUrlWithMock('/product/generateProductCode','http://demo0905840.mockable.io/generateproduct');
        case ADD_ORDER:
            return generateUrlWithMock('/order/add','');
        case PUSH_PRODUCT:
            return generateUrlWithMock('/order/addProduct', '');
        case UPDATE_PRODUCT:
            return generateUrlWithMock('/order/editProduct/pid/', '');
        case UPDATE_ORDER:
            return generateUrlWithMock('/order/edit/oid', '');
        case DEL_PRODUCT:
            return generateUrlWithMock('/order/deleteProduct/id', '');
        case LIST_CATEGORY:
            return generateUrlWithMock('/product/categoryList', 'http://demo6523336.mockable.io/categoryList');
        case LIST_MODEL:
            return generateUrlWithMock('/product/modelList/cid', 'http://demo6523336.mockable.io/modelList/cid');
        case LIST_UNIT:
            return generateUrlWithMock('/product/unitList/mid', 'http://demo6523336.mockable.io/unitList/mid');
        case HAS_ORDER:
            return generateUrlWithMock('/order/isThereOrder/oid', '');
        case HAS_CUSTOMER:
            return generateUrlWithMock('/customer/isThereCustomer/cid', '');
        case GET_CUSTOMER_STATUS:
            return generateUrlWithMock('/customer/statusMapping', '');
        case GET_ORDER_STATUS:
            return generateUrlWithMock('/order/statusMapping', '');
        case LIST_INVENTORY:
            return generateUrlWithMock('/inventory/list', 'http://demo6523336.mockable.io/inventory/list');
        case DEL_FILE:
            return generateUrlWithMock('/customer/removeFile/id', '');
        case LOGIN:
            return generateUrlWithMock('/user/login', '');
        case LOGOUT:
            return generateUrlWithMock('/user/logout', '');
        case ACCOUNT_GET:
            return generateUrlWithMock('/user/getTokenDetail', '');
        case PASSWORD_RESET:
            return generateUrlWithMock('/user/changePassword', '');
        case ACCOUNT_EDIT:
            return generateUrlWithMock('/user/selfEdit', '');
        case CLOSE_ORDER:
            return generateUrlWithMock('/order/closeStatus', '');
        case GET_CUSTOMER_ZONE:
            return generateUrlWithMock('/customer/customerZone', '');
        case GET_TIME_STAMP:
            return generateUrlWithMock('/inventory/getLastTimestamp', '');
        case CHK_SCREEN:
            return generateUrlWithMock('/product/checkCanScreen', '');
        case GET_PRICE:
            return generateUrlWithMock('/product/getPrice', '');
        }
    }
}
