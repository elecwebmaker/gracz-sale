import { Component, OnInit, Input, Output, HostListener, EventEmitter } from '@angular/core';

@Component({
    selector: 'cf-choice',
    templateUrl: './cf-choice.component.html',
    styleUrls: ['./cf-choice.component.scss']
})
export class CfChoiceComponent implements OnInit {
    @Input() value: string;
    @Output() onClick: EventEmitter<any> = new EventEmitter();
    public isSelected: boolean = false;
    constructor() { }

    ngOnInit() {
    }

    @HostListener('click') click() {
        this.onClick.emit(this.value);
    };

    setActive() {
        this.isSelected = true;
    }

    setDeactive() {
        this.isSelected = false;
    }

}
