import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CfChoiceComponent } from './cf-choice.component';

describe('CfChoiceComponent', () => {
  let component: CfChoiceComponent;
  let fixture: ComponentFixture<CfChoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CfChoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CfChoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
