import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs/Rx";

@Injectable()
export class SidebarService {

  private sidebarState: boolean;
  private sidebarState$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor() { }

  setState(state: boolean): void {
    this.sidebarState = state;
    this.sidebarState$.next(this.sidebarState);
  }

  getState() {
    return this.sidebarState$;
  }

  toggleState(): void {
    this.sidebarState = !this.sidebarState;
    this.sidebarState$.next(this.sidebarState);
  }

}
