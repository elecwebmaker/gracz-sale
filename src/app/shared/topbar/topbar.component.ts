import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  HostListener,
  ViewChild
} from "@angular/core";
import { TopbarService, TopbarState } from "app/shared/topbar.service";
import { Router } from "@angular/router";
import { SidebarService } from "app/shared/sidebar.service";

@Component({
  selector: "app-topbar",
  templateUrl: "./topbar.component.html",
  styleUrls: ["./topbar.component.scss"]
})
export class TopbarComponent implements OnInit {
  sidebarState: boolean = false;
  state: TopbarState = {
    hasButton: false,
    title: ""
  };
  @ViewChild('sbToggle') sbToggle;
  @Output() toggleMenu: EventEmitter<any> = new EventEmitter();
  constructor(
    private topbarservice: TopbarService,
    private router: Router,
    private sidebarService: SidebarService
  ) {}

  @HostListener('window:click', ['$event']) onClick(event) {
    if (this.sbToggle.nativeElement !== event.target && window.innerWidth < 1200) {
      this.sidebarService.setState(false);
    }
  }

  ngOnInit() {
    this.topbarservice.getState().subscribe(state => {
      this.state = state;
    });
    this.sidebarService.getState().subscribe(state => {
      this.sidebarState = state;
    });
  }

  menuClick() {
    this.toggleMenu.emit();
    this.sidebarService.toggleState();
  }

  addClick() {
    if (this.state.route) {
      this.router.navigate([this.state.route]);
    }
  }
}
