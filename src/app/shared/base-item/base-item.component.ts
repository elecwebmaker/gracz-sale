import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CustomerItemModel } from 'app/customer/model/customer-item.model';

@Component({
    selector: 'app-base-item',
    templateUrl: './base-item.component.html',
    styleUrls: ['./base-item.component.scss']
})

export abstract class BaseItemComponent implements OnInit {
    @Input() model: any;
    @Input() isStrip: boolean;
    @Output('onClick') output = new EventEmitter();

    constructor() { }

    ngOnInit() {
    }

}
