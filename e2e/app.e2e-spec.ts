import { SalePage } from './app.po';

describe('sale App', () => {
  let page: SalePage;

  beforeEach(() => {
    page = new SalePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
